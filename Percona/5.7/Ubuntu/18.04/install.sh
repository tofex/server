#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Server host name, default: localhost
  -p  Server port, default: 3306
  -s  User password

Example: ${scriptName} -s secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootPassword=

while getopts hn:p:s:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root port specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

export DEBIAN_FRONTEND=noninteractive

echo "Downloading libraries"
install-package gnupg2

install-package-from-deb percona-release 1.0-15.generic https://repo.percona.com/apt/percona-release_latest.bionic_all.deb
update-apt
install-package percona-server-server-5.7 5.7.29

service mysql stop 2>&1

cat <<EOF > /etc/mysql/conf.d/mysqld.cnf
[mysqld]
port = ${databaseRootPort}
pid-file = /var/run/mysqld/mysqld.pid
socket = /var/run/mysqld/mysqld.sock
datadir = /var/lib/mysql
log-error = /var/log/mysql/error.log
bind-address = 0.0.0.0
symbolic-links = 0
skip-external-locking
skip-name-resolve
max_allowed_packet = 2G
thread_stack = 192K
thread_cache_size = 8
myisam-recover-options = BACKUP
max_connections = 100
innodb_log_file_size = 320M
innodb_log_buffer_size = 320M
innodb_buffer_pool_size = 2G
innodb_file_per_table = OFF
join_buffer_size = 8M
key_buffer_size = 32M
max_heap_table_size = 1024M
query_cache_size = 64M
query_cache_limit = 16M
read_buffer_size = 1M
read_rnd_buffer_size = 8M
sort_buffer_size = 1M
tmp_table_size = 1024M
query_cache_type = 1
expire_logs_days = 10
max_binlog_size = 100M
EOF

cat <<EOF > /etc/mysql/conf.d/mysqld_safe.cnf
[mysqld_safe]
socket = /var/run/mysqld/mysqld.sock
nice = 0
EOF

cat <<EOF > /etc/mysql/conf.d/mysqldump.cnf
[mysqldump]
quick
quote-names
max_allowed_packet = 2G
EOF

cat <<EOF > /etc/mysql/conf.d/isamchk.cnf
[isamchk]
key_buffer_size = 16M
EOF

rm /var/lib/mysql/ib_logfile*

service mysql start 2>&1

echo "Adding user: root"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e 'use mysql;' >/dev/null 2>&1 && mysqladmin -h"${databaseRootHost}" -P"${databaseRootPort}" -u root password "${databaseRootPassword}" >/dev/null 2>&1

export MYSQL_PWD="${databaseRootPassword}"

echo "Granting super rights to user: 'root'@'%'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT USAGE ON *.* TO 'root'@'%' IDENTIFIED BY '${databaseRootPassword}';";

echo "Granting all privileges to user: 'root'@'%'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';"

echo "Allowing login with password to user: 'root'@'%'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '${databaseRootPassword}';"

echo "Granting super rights to user: 'root'@'127.0.0.1'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT USAGE ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY '${databaseRootPassword}';";

echo "Granting all privileges to user: 'root'@'127.0.0.1'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1';"

echo "Allowing login with password to user: 'root'@'127.0.0.1'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "ALTER USER 'root'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY '${databaseRootPassword}';"

echo "Granting super rights to user: 'root'@'localhost'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT USAGE ON *.* TO 'root'@'localhost' IDENTIFIED BY '${databaseRootPassword}';";

echo "Granting all privileges to user: 'root'@'localhost'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost';"

echo "Allowing login with password to user: 'root'@'127.0.0.1'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '${databaseRootPassword}';"

echo "Flushing privileges"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "FLUSH PRIVILEGES;"

mkdir -p /opt/install/
crudini --set /opt/install/env.properties mysql type "percona"
crudini --set /opt/install/env.properties mysql version "5.7"
crudini --set /opt/install/env.properties mysql port "${databaseRootPort}"
crudini --set /opt/install/env.properties mysql rootUser "root"
crudini --set /opt/install/env.properties mysql rootPassword "${databaseRootPassword}"
