#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Ban time, default: 26h
  -f  Find time, default: 32m
  -r  Max retry number, default: 4

Example: ${scriptName} -b 26h -f 32m -r 4
EOF
}

trim()
{
  echo -n "$1" | xargs
}

banTime="26h"
findTime="32m"
maxRetry="4"

while getopts hb:f:r:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) banTime=$(trim "$OPTARG");;
    f) findTime=$(trim "$OPTARG");;
    r) maxRetry=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${banTime}" ]]; then
  echo "No ban time specified!"
  exit 1
fi

if [[ -z "${banTime}" ]]; then
  echo "No ban time specified!"
  exit 1
fi

if [[ -z "${findTime}" ]]; then
  echo "No find time specified!"
  exit 1
fi

if [[ -z "${maxRetry}" ]]; then
  echo "No max retry number specified!"
  exit 1
fi

echo "Installing Fail2Ban 0.10"
install-package fail2ban 0.10

echo "Copy default settings"
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

echo "Setting ban time"
replace-file-content /etc/fail2ban/jail.local "bantime  = ${banTime}" "bantime  = 10m"

echo "Setting find time"
replace-file-content /etc/fail2ban/jail.local "findtime  = ${findTime}" "findtime  = 10m"

echo "Setting max retry number"
replace-file-content /etc/fail2ban/jail.local "maxretry = ${maxRetry}" "maxretry = 5"

echo "Restarting Fail2Ban"
systemctl restart fail2ban.service
