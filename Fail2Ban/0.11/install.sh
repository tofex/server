#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Ban time, default: 26h
  -f  Find time, default: 32m
  -r  Max retry number, default: 4

Example: ${scriptName} -b 26h -f 32m -r 4
EOF
}

trim()
{
  echo -n "$1" | xargs
}

banTime="26h"
findTime="32m"
maxRetry="4"

while getopts hb:f:r:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) banTime=$(trim "$OPTARG");;
    f) findTime=$(trim "$OPTARG");;
    r) maxRetry=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${banTime}" ]]; then
  echo "No ban time specified!"
  exit 1
fi

if [[ -z "${banTime}" ]]; then
  echo "No ban time specified!"
  exit 1
fi

if [[ -z "${findTime}" ]]; then
  echo "No find time specified!"
  exit 1
fi

if [[ -z "${maxRetry}" ]]; then
  echo "No max retry number specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -b "${banTime}" -f "${findTime}" -r "${maxRetry}"
