#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  Port, default: 8086

Example: ${scriptName} -p 8086
EOF
}

trim()
{
  echo -n "$1" | xargs
}

port="8086"

while getopts hp:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) port=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${port}" ]]; then
  echo "No port specified!"
  exit 1
fi

echo "Installing InfluxDB"
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.4.2_amd64.deb
dpkg -i influxdb_1.4.2_amd64.deb

replace-file-content /etc/influxdb/influxdb.conf "bind-address = \":${port}\"" "# bind-address = \":8086\""

echo "Restarting InfluxDB"
service influxdb restart

mkdir -p /opt/install/
crudini --set /opt/install/env.properties influxdb version "1.4"
crudini --set /opt/install/env.properties influxdb port "${port}"
