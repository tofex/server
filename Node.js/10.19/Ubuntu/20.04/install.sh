#!/bin/bash -e

install-package build-essential
install-package python2-minimal

mkdir -p /tmp/node.js
cd /tmp/node.js
wget -q https://nodejs.org/dist/v10.19.0/node-v10.19.0.tar.gz
tar xfz node-v10.19.0.tar.gz
cd node-v10.19.0
./configure
make
make install
cd /
rm -rf /tmp/node.js
