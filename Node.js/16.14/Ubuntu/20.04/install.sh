#!/bin/bash -e

install-package build-essential
install-package python2-minimal

mkdir -p /tmp/node.js
cd /tmp/node.js
wget -q https://nodejs.org/dist/v16.14.2/node-v16.14.2.tar.gz
tar xfz node-v16.14.2.tar.gz
cd node-v16.14.2
./configure
make
make install
cd /
rm -rf /tmp/node.js
