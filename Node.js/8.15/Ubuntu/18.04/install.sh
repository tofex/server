#!/bin/bash -e

install-package build-essential
install-package python-minimal

mkdir -p /tmp/node.js
cd /tmp/node.js
wget -q https://nodejs.org/dist/v8.15.1/node-v8.15.1.tar.gz
tar xfz node-v8.15.1.tar.gz
cd node-v8.15.1
./configure
make
make install
cd /
rm -rf /tmp/node.js
