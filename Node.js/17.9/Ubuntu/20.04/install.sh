#!/bin/bash -e

install-package build-essential
install-package python2-minimal

mkdir -p /tmp/node.js
cd /tmp/node.js
wget -q https://nodejs.org/dist/v17.9.1/node-v17.9.1.tar.gz
tar xfz node-v17.9.1.tar.gz
cd node-v17.9.1
./configure
make
make install
cd /
rm -rf /tmp/node.js
