#!/bin/bash -e

echo "Installing Telegraf"
wget https://dl.influxdata.com/telegraf/releases/telegraf_1.5.0-1_amd64.deb
dpkg -i telegraf_1.5.0-1_amd64.deb

mkdir -p /opt/install/
crudini --set /opt/install/env.properties telegraf version "1.5"
