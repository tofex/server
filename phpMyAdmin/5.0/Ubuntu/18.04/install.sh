#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -t  Database Type, default: MySQL
  -v  Database Version, default: 5.6
  -n  Root host, default: localhost
  -p  Root port, default: 3306
  -u  Root user, default: root
  -s  Root password
  -o  Project host, default: localhost
  -r  Project port, default: 3306
  -e  Project user, default: pma
  -w  Project password, default: pma
  -y  Web server type, default: Apache
  -c  Web server version, default: 2.4
  -g  HTTP port, default: 80
  -l  SSL port, default: 443
  -m  Server name
  -a  SSL terminated (yes/no), default: no
  -f  Force SSL (yes/no), default: yes
  -b  Basic auth user name (optional)
  -d  Basic auth password (optional)

Example: ${scriptName} -m dev -a -u
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseType="MySQL"
databaseVersion="5.6"
databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootUser="root"
databaseRootPassword=
databaseProjectHost="localhost"
databaseProjectPort="3306"
databaseProjectUser="pma"
databaseProjectPassword="pma"
webServerType="Apache"
webServerVersion="2.4"
webServerHttpPort=80
webServerSslPort=443
serverName=
sslTerminated="no"
forceSsl="yes"
basicAuthUserName=
basicAuthPassword=

while getopts ht:v:n:p:u:s:o:r:e:w:y:c:g:l:m:a:f:b:d:? option; do
  case ${option} in
    h) usage; exit 1;;
    t) databaseType=$(trim "$OPTARG");;
    v) databaseVersion=$(trim "$OPTARG");;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    u) databaseRootUser=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    o) databaseProjectHost=$(trim "$OPTARG");;
    r) databaseProjectPort=$(trim "$OPTARG");;
    e) databaseProjectUser=$(trim "$OPTARG");;
    w) databaseProjectPassword=$(trim "$OPTARG");;
    y) webServerType=$(trim "$OPTARG");;
    c) webServerVersion=$(trim "$OPTARG");;
    g) webServerHttpPort=$(trim "$OPTARG");;
    l) webServerSslPort=$(trim "$OPTARG");;
    m) serverName=$(trim "$OPTARG");;
    a) sslTerminated=$(trim "$OPTARG");;
    f) forceSsl=$(trim "$OPTARG");;
    b) basicAuthUserName=$(trim "$OPTARG");;
    d) basicAuthPassword=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseType}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseVersion}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootUser}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${databaseProjectHost}" ]]; then
  echo "No database project host specified!"
  exit 1
fi

if [[ -z "${databaseProjectPort}" ]]; then
  echo "No database project port specified!"
  exit 1
fi

if [[ -z "${databaseProjectUser}" ]]; then
  echo "No database project user specified!"
  exit 1
fi

if [[ -z "${databaseProjectPassword}" ]]; then
  echo "No database project password specified!"
  exit 1
fi

if [[ -z "${webServerType}" ]]; then
  echo "No web server type specified!"
  exit 1
fi

if [[ -z "${webServerVersion}" ]]; then
  echo "No web server version specified!"
  exit 1
fi

if [[ -z "${webServerHttpPort}" ]]; then
  echo "No web server HTTP port specified!"
  exit 1
fi

if [[ -z "${webServerSslPort}" ]]; then
  echo "No web server SSL port specified!"
  exit 1
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${sslTerminated}" ]]; then
  echo "No SSL terminated specified!"
  exit 1
fi

if [[ -z "${forceSsl}" ]]; then
  echo "No force SSL specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

databaseInitScript="${currentPath}/../../../../${databaseType}/${databaseVersion}/database_init.sh"

if [[ ! -f "${databaseInitScript}" ]]; then
  echo "Missing database init script at: ${databaseInitScript}"
  exit 1
fi

webServerInitScript="${currentPath}/../../../../${webServerType}/${webServerVersion}/vhost.sh"

if [[ ! -f "${webServerInitScript}" ]]; then
  echo "Missing web server virtual host script at: ${webServerInitScript}"
  exit 1
fi

install-package unzip

"${databaseInitScript}" -n "${databaseRootHost}" -p "${databaseRootPort}" -u "${databaseRootUser}" -s "${databaseRootPassword}" -o "${databaseProjectHost}" -r "${databaseProjectPort}" -e "${databaseProjectUser}" -w "${databaseProjectPassword}" -b phpmyadmin -g

version="5.0.2"

cd /var/www/
wget -nv "https://files.phpmyadmin.net/phpMyAdmin/${version}/phpMyAdmin-${version}-all-languages.zip"
unzip -o -q "phpMyAdmin-${version}-all-languages.zip"
rm -rf "phpMyAdmin-${version}-all-languages.zip"
chown -hR www-data:www-data "phpMyAdmin-${version}-all-languages"

export MYSQL_PWD="${databaseProjectPassword}"
mysql "-h${databaseProjectHost}" "-P${databaseProjectPort}" "-u${databaseProjectUser}" --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;" phpmyadmin < "phpMyAdmin-${version}-all-languages/sql/create_tables.sql"

"${webServerInitScript}" -r "${webServerHttpPort}" -s "${webServerSslPort}" -v "${serverName}" -w "/var/www/phpMyAdmin-${version}-all-languages" -t "${sslTerminated}" -f "${forceSsl}" -o -u "${basicAuthUserName}" -p "${basicAuthPassword}"

cat <<EOF > "phpMyAdmin-${version}-all-languages/config.inc.php"
<?php
\$cfg['blowfish_secret'] = 'FdbzR32ZYo8XXm5EB5kOQsWjaUBH1UP1';
\$i=0;
\$i++;
\$cfg['Servers'][\$i]['auth_type'] = 'cookie';
\$cfg['Servers'][\$i]['controlhost'] = '${databaseProjectHost}';
\$cfg['Servers'][\$i]['controlport'] = '${databaseProjectPort}';
\$cfg['Servers'][\$i]['controluser'] = '${databaseProjectUser}';
\$cfg['Servers'][\$i]['controlpass'] = '${databaseProjectPassword}';
\$cfg['Servers'][\$i]['pmadb'] = 'phpmyadmin';
\$cfg['Servers'][\$i]['bookmarktable'] = 'pma__bookmark';
\$cfg['Servers'][\$i]['relation'] = 'pma__relation';
\$cfg['Servers'][\$i]['table_info'] = 'pma__table_info';
\$cfg['Servers'][\$i]['table_coords'] = 'pma__table_coords';
\$cfg['Servers'][\$i]['pdf_pages'] = 'pma__pdf_pages';
\$cfg['Servers'][\$i]['column_info'] = 'pma__column_info';
\$cfg['Servers'][\$i]['history'] = 'pma__history';
\$cfg['Servers'][\$i]['table_uiprefs'] = 'pma__table_uiprefs';
\$cfg['Servers'][\$i]['tracking'] = 'pma__tracking';
\$cfg['Servers'][\$i]['designer_coords'] = 'pma__designer_coords';
\$cfg['Servers'][\$i]['userconfig'] = 'pma__userconfig';
\$cfg['Servers'][\$i]['recent'] = 'pma__recent';
\$cfg['Servers'][\$i]['favorite'] = 'pma__favorite';
\$cfg['Servers'][\$i]['users'] = 'pma__users';
\$cfg['Servers'][\$i]['usergroups'] = 'pma__usergroups';
\$cfg['Servers'][\$i]['navigationhiding'] = 'pma__navigationhiding';
\$cfg['Servers'][\$i]['savedsearches'] = 'pma__savedsearches';
\$cfg['Servers'][\$i]['central_columns'] = 'pma__central_columns';
\$cfg['Servers'][\$i]['designer_settings'] = 'pma__designer_settings';
\$cfg['Servers'][\$i]['export_templates'] = 'pma__export_templates';
EOF

chown www-data:www-data "phpMyAdmin-${version}-all-languages/config.inc.php"
