#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -t  Database Type, default: MySQL
  -v  Database Version, default: 5.6
  -n  Root host, default: localhost
  -p  Root port, default: 3306
  -u  Root user, default: root
  -s  Root password
  -o  Project host, default: localhost
  -r  Project port, default: 3306
  -e  Project user, default: pma
  -w  Project password, default: pma
  -y  Web server type, default: Apache
  -c  Web server version, default: 2.4
  -g  HTTP port, default: 80
  -l  SSL port, default: 443
  -m  Server name
  -a  SSL terminated (yes/no), default: no
  -f  Force SSL (yes/no), default: yes
  -b  Basic auth user name (optional)
  -d  Basic auth password (optional)

Example: ${scriptName} -m dev -a -u
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseType="MySQL"
databaseVersion="5.6"
databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootUser="root"
databaseRootPassword=
databaseProjectHost="localhost"
databaseProjectPort="3306"
databaseProjectUser="pma"
databaseProjectPassword="pma"
webServerType="Apache"
webServerVersion="2.4"
webServerHttpPort=80
webServerSslPort=443
serverName=
sslTerminated="no"
forceSsl="yes"
basicAuthUserName=
basicAuthPassword=

while getopts ht:v:n:p:u:s:o:r:e:w:y:c:g:l:m:a:f:b:d:? option; do
  case ${option} in
    h) usage; exit 1;;
    t) databaseType=$(trim "$OPTARG");;
    v) databaseVersion=$(trim "$OPTARG");;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    u) databaseRootUser=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    o) databaseProjectHost=$(trim "$OPTARG");;
    r) databaseProjectPort=$(trim "$OPTARG");;
    e) databaseProjectUser=$(trim "$OPTARG");;
    w) databaseProjectPassword=$(trim "$OPTARG");;
    y) webServerType=$(trim "$OPTARG");;
    c) webServerVersion=$(trim "$OPTARG");;
    g) webServerHttpPort=$(trim "$OPTARG");;
    l) webServerSslPort=$(trim "$OPTARG");;
    m) serverName=$(trim "$OPTARG");;
    a) sslTerminated=$(trim "$OPTARG");;
    f) forceSsl=$(trim "$OPTARG");;
    b) basicAuthUserName=$(trim "$OPTARG");;
    d) basicAuthPassword=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseType}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseVersion}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootUser}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root password specified!"
  exit 1
fi

if [[ -z "${databaseProjectHost}" ]]; then
  echo "No database project host specified!"
  exit 1
fi

if [[ -z "${databaseProjectPort}" ]]; then
  echo "No database project port specified!"
  exit 1
fi

if [[ -z "${databaseProjectUser}" ]]; then
  echo "No database project user specified!"
  exit 1
fi

if [[ -z "${databaseProjectPassword}" ]]; then
  echo "No database project password specified!"
  exit 1
fi

if [[ -z "${webServerType}" ]]; then
  echo "No web server type specified!"
  exit 1
fi

if [[ -z "${webServerVersion}" ]]; then
  echo "No web server version specified!"
  exit 1
fi

if [[ -z "${webServerHttpPort}" ]]; then
  echo "No web server HTTP port specified!"
  exit 1
fi

if [[ -z "${webServerSslPort}" ]]; then
  echo "No web server SSL port specified!"
  exit 1
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${sslTerminated}" ]]; then
  echo "No SSL terminated specified!"
  exit 1
fi

if [[ -z "${forceSsl}" ]]; then
  echo "No force SSL specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" \
 -t "${databaseType}" \
 -v "${databaseVersion}" \
 -n "${databaseRootHost}" \
 -p "${databaseRootPort}" \
 -u "${databaseRootUser}" \
 -s "${databaseRootPassword}" \
 -o "${databaseProjectHost}" \
 -r "${databaseProjectPort}" \
 -e "${databaseProjectUser}" \
 -w "${databaseProjectPassword}" \
 -y "${webServerType}" \
 -c "${webServerVersion}" \
 -g "${webServerHttpPort}" \
 -l "${webServerSslPort}" \
 -m "${serverName}" \
 -a "${sslTerminated}" \
 -f "${forceSsl}" \
 -b "${basicAuthUserName}" \
 -d "${basicAuthPassword}"
