#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Server host name, default: localhost
  -p  Server port, default: 3306
  -s  Root password
  -b  Bind address, default: 127.0.0.1 or 0.0.0.0 if docker environment

Example: ${scriptName} -s secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost=
databaseRootPort=
databaseRootPassword=
bindAddress=

while getopts hn:p:s:b:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    b) bindAddress=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  databaseRootHost="localhost"
fi

if [[ -z "${databaseRootPort}" ]]; then
  databaseRootPort="3306"
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root password specified!"
  exit 1
fi

if [[ -z "${bindAddress}" ]]; then
  if [[ -f /.dockerenv ]]; then
    bindAddress="0.0.0.0"
  else
    bindAddress="127.0.0.1"
  fi
fi

echo "Setting up installation"
curl -LsS https://r.mariadb.com/downloads/mariadb_repo_setup | bash -s -- --mariadb-server-version="mariadb-10.6" --os-type="ubuntu" --os-version="jammy"

export DEBIAN_FRONTEND=noninteractive

echo "Downloading libraries"
install-package mariadb-server 1:10.6

echo "Fix start script"
# shellcheck disable=SC2016
replace-file-content /etc/init.d/mariadb '[ -z "$datadir" ]' '[ -z "$datadir"]' 0

echo "Setting port to: ${databaseRootPort}"
add-file-content-after /etc/mysql/mariadb.conf.d/50-server.cnf "port = ${databaseRootPort}" "[mysqld]" 1

echo "Starting MariaDB"
/etc/init.d/mariadb start

echo "Setting root password"
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED VIA unix_socket OR mysql_native_password USING PASSWORD('${databaseRootPassword}'); FLUSH PRIVILEGES;"

export MYSQL_PWD="${databaseRootPassword}"

rootUserNames=( "'root'@'localhost'" "'root'@'127.0.0.1'" "'root'@'%'" )

for rootUserName in "${rootUserNames[@]}"; do
  if [[ "${rootUserName}" != "'root'@'localhost'" ]]; then
    echo "Create user: ${rootUserName}"
    mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "CREATE USER ${rootUserName} IDENTIFIED BY '${databaseRootPassword}';";
  fi

  echo "Granting super rights to user: ${rootUserName}"
  mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT USAGE ON *.* TO ${rootUserName};";

  echo "Granting all privileges to user: ${rootUserName}"
  mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT ALL PRIVILEGES ON *.* TO ${rootUserName} WITH GRANT OPTION;"
done

echo "Flushing privileges"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "FLUSH PRIVILEGES;"

echo "Increasing file limits"
add-file-content-before /etc/security/limits.conf "mysql  soft  nofile  65535" "# End of file" 1
add-file-content-before /etc/security/limits.conf "mysql  hard  nofile  65535" "# End of file" 1
sysctl -p

echo "Allowing binding from: ${bindAddress}"
replace-file-content /etc/mysql/mariadb.conf.d/50-server.cnf "bind-address            = ${bindAddress}" "bind-address            = 127.0.0.1"

echo "Adding skipping of DNS lookup"
replace-file-content /etc/mysql/mariadb.conf.d/50-server.cnf "skip-name-resolve" "#skip-name-resolve" 0

echo "Stopping MariaDB"
sleep 3
/etc/init.d/mariadb stop

if [[ -f /.dockerenv ]]; then
  echo "Creating start script at: /usr/local/bin/mariadb.sh"
  cat <<EOF > /usr/local/bin/mariadb.sh
#!/usr/bin/env bash
trap stop SIGTERM SIGINT SIGQUIT SIGHUP ERR
stop() {
  echo "Stopping MariaDB"
  mariadb-admin shutdown
  exit
}
for command in "\$@"; do
  echo "Run: \${command}"
  /bin/bash "\${command}"
done
/usr/bin/install \
  -m 755 \
  -o mysql \
  -g root \
  -d /var/run/mysqld
echo "Starting MariaDB"
/usr/sbin/mysqld \
  --basedir=/usr \
  --datadir=/var/lib/mysql \
  --plugin-dir=/usr/lib/mysql/plugin \
  --user=mysql \
  --skip-log-error \
  --pid-file=/var/run/mysqld/mysqld.pid \
  --socket=/var/run/mysqld/mysqld.sock &
tail -f /dev/null & wait \$!
EOF
  chmod +x /usr/local/bin/mariadb.sh
else
  echo "Starting MariaDB"
  /etc/init.d/mariadb start
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties mysql type "mariadb"
crudini --set /opt/install/env.properties mysql version "10.6"
crudini --set /opt/install/env.properties mysql port "${databaseRootPort}"
crudini --set /opt/install/env.properties mysql rootUser "root"
crudini --set /opt/install/env.properties mysql rootPassword "${databaseRootPassword}"
