#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -y  System name, default: project
  -u  Root user, default: root
  -s  Root password
  -g  Grant user super rights
  -c  Create initial database

Example: ${scriptName} -u root -s secret -e user -w password -b dbname -g -c
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseHost="localhost"
databasePort="3306"
databaseRootPassword=
databaseUser=
databasePassword=
databaseName=
grantSuperRights="no"
createDatabase="no"

while getopts ho:p:s:e:w:b:gc? option; do
  case ${option} in
    h) usage; exit 1;;
    o) databaseHost=$(trim "$OPTARG");;
    p) databasePort=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    e) databaseUser=$(trim "$OPTARG");;
    w) databasePassword=$(trim "$OPTARG");;
    b) databaseName=$(trim "$OPTARG");;
    g) grantSuperRights="yes";;
    c) createDatabase="yes";;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseHost}" ]]; then
  echo "No database host specified!"
  exit 1
fi

if [[ -z "${databasePort}" ]]; then
  echo "No database port specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root password specified!"
  exit 1
fi

if [[ -z "${databaseUser}" ]]; then
  echo "No database user specified!"
  exit 1
fi

if [[ -z "${databasePassword}" ]]; then
  echo "No database password specified!"
  exit 1
fi

if [[ -z "${databaseName}" ]]; then
  echo "No database name specified!"
  exit 1
fi

if [[ -z "${grantSuperRights}" ]]; then
  echo "No grant super rights specified!"
  exit 1
fi

if [[ -z "${createDatabase}" ]]; then
  echo "No create database specified!"
  exit 1
fi

export MYSQL_PWD="${databaseRootPassword}"

echo "Adding user: '${databaseUser}'@'localhost'"
mysql -P"${databasePort}" -u root -e "GRANT ALL ON ${databaseName}.* TO '${databaseUser}'@'localhost' identified by '${databasePassword}' WITH GRANT OPTION;"

if [[ "${grantSuperRights}" == "yes" ]]; then
    echo "Granting super rights to user: '${databaseUser}'@'localhost'"
    mysql -P"${databasePort}" -u root -e "GRANT SUPER ON *.* TO '${databaseUser}'@'localhost';"
fi

echo "Adding user: '${databaseUser}'@'%'"
mysql -P"${databasePort}" -u root -e "GRANT ALL ON ${databaseName}.* TO '${databaseUser}'@'%' identified by '${databasePassword}' WITH GRANT OPTION;"

if [[ "${grantSuperRights}" == "yes" ]]; then
    echo "Granting super rights to user: '${databaseUser}'@'%'"
    mysql -P"${databasePort}" -u root -e "GRANT SUPER ON *.* TO '${databaseUser}'@'%';"
fi

echo "Flushing privileges"
mysql -P"${databasePort}" -u root -e "FLUSH PRIVILEGES;"

if [[ "${createDatabase}" == "yes" ]]; then
  export MYSQL_PWD="${databasePassword}"

  echo "Dropping database: ${databaseName}"
  mysql -P"${databasePort}" -u"${databaseUser}" -e "DROP DATABASE IF EXISTS ${databaseName};"

  echo "Creating database: ${databaseName}"
  mysql -P"${databasePort}" -u"${databaseUser}" -e "CREATE DATABASE ${databaseName} CHARACTER SET utf8 COLLATE utf8_general_ci;";
fi
