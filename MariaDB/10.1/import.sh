#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -o  Database host, default: localhost
  -p  Database port, default: 3306
  -u  Database user
  -s  Database password
  -n  Database name
  -i  Import file
  -t  Path to temp directory
  -r  Remove import file after import

Example: ${scriptName} -i import.sql.gz
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseHost="localhost"
databasePort="3306"
importFile=
removeFile=0
tempDir=

while getopts ho:p:u:s:n:i:t:r? option; do
  case ${option} in
    h) usage; exit 1;;
    o) databaseHost=$(trim "$OPTARG");;
    p) databasePort=$(trim "$OPTARG");;
    u) databaseUser=$(trim "$OPTARG");;
    s) databasePassword=$(trim "$OPTARG");;
    n) databaseName=$(trim "$OPTARG");;
    i) importFile=$(trim "$OPTARG");;
    t) tempDir=$(trim "$OPTARG");;
    r) removeFile=1;;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${importFile}" ]]; then
  usage
  exit 1
fi

if [[ -z "${tempDir}" ]]; then
  tempDir="/tmp"
fi

if [[ ! -d "${tempDir}" ]]; then
  echo "Temp directory does not exists at: ${tempDir}"
  exit 1
fi

if [[ -z "${databaseHost}" ]]; then
  echo "No database host specified!"
  exit 1
fi

if [[ -z "${databasePort}" ]]; then
  echo "No database port specified!"
  exit 1
fi

if [[ -z "${databaseUser}" ]]; then
  echo "No database user specified!"
  exit 1
fi

if [[ -z "${databasePassword}" ]]; then
  echo "No database password specified!"
  exit 1
fi

if [[ -z "${databaseName}" ]]; then
  echo "No database name specified!"
  exit 1
fi

echo "Preparing import file"
if [[ "${importFile: -7}" == ".tar.gz" ]]; then
  tar -xOzf "${importFile}" | sed -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' | sed -e '/^ALTER\sDATABASE/d' | sed -e 's/ROW_FORMAT=FIXED//g' > "${tempDir}/import.sql"
elif [[ "${importFile: -7}" == ".sql.gz" ]]; then
  cat "${importFile}" | gzip -d -q | sed -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' | sed -e '/^ALTER\sDATABASE/d' | sed -e 's/ROW_FORMAT=FIXED//g' > "${tempDir}/import.sql"
elif [[ "${importFile: -4}" == ".zip" ]]; then
  unzip -p "${importFile}" | sed -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' | sed -e '/^ALTER\sDATABASE/d' | sed -e 's/ROW_FORMAT=FIXED//g' > "${tempDir}/import.sql"
elif [[ "${importFile: -4}" == ".sql" ]]; then
  cat "${importFile}" | sed -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' | sed -e '/^ALTER\sDATABASE/d' | sed -e 's/ROW_FORMAT=FIXED//g' > "${tempDir}/import.sql"
else
  echo "Unsupported file format"
  exit 1
fi

export MYSQL_PWD="${databasePassword}"

echo "Importing dump..."
mysql "-h${databaseHost}" "-P${databasePort:-3306}" "-u${databaseUser}" --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;" "${databaseName}" < "${tempDir}/import.sql"

echo "Removing prepared import file"
rm -rf "${tempDir}/import.sql"

if [[ "${removeFile}" == 1 ]]; then
  echo "Removing import file: ${importFile}"
  rm -rf "${importFile}"
fi
