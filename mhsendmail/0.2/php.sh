#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  Remote host
  -p  Remote port
  -s  Sender mail address, default webmaster@localhost.local

Example: ${scriptName} -r 192.168.0.1 -p 1026 -s webmaster@localhost.local
EOF
}

trim()
{
  echo -n "$1" | xargs
}

remoteHost=
remotePort=
sender=

while getopts hr:p:s:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) remoteHost=$(trim "$OPTARG");;
    p) remotePort=$(trim "$OPTARG");;
    s) sender=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${remoteHost}" ]]; then
  echo "No remote host specified!"
  exit 1
fi

if [[ -z "${remotePort}" ]]; then
  echo "No remote port specified!"
  exit 1
fi

if [[ -z "${sender}" ]]; then
  sender="webmaster@localhost.local"
fi

if [[ -d "/usr/local/php/etc/ini" ]]; then
  echo "Creating configuration at: /usr/local/php/etc/ini/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /usr/local/php/etc/ini/mhsendmail.ini
fi
if [[ -d "/etc/php5/conf.d" ]]; then
  echo "Creating configuration at: /etc/php5/conf.d/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php5/conf.d/mhsendmail.ini
fi
if [[ -d "/etc/php5/mods-available" ]]; then
  echo "Creating configuration at: /etc/php5/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php5/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/5.5/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/5.5/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/5.5/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/5.6/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/5.6/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/5.6/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/7.0/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/7.0/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/7.0/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/7.1/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/7.1/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/7.1/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/7.2/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/7.2/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/7.2/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/7.3/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/7.3/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/7.3/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/7.4/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/7.4/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/7.4/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/8.1/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/8.1/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/8.1/mods-available/mhsendmail.ini
fi
if [[ -d "/etc/php/8.2/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/8.2/mods-available/mhsendmail.ini"
  echo "sendmail_path = /usr/bin/mhsendmail --smtp-addr ${remoteHost}:${remotePort} --from ${sender}" > /etc/php/8.2/mods-available/mhsendmail.ini
fi

if [[ -n $(which phpenmod) ]]; then
  echo "Enabling module mhsendmail"
  phpenmod mhsendmail
fi

if [[ -n $(which php5enmod) ]]; then
  echo "Enabling module mhsendmail"
  php5enmod mhsendmail
fi

if [[ ! -f /.dockerenv ]]; then
  if [[ $(which php5-fpm | wc -l) -gt 0 ]]; then
    echo "Restarting PHP5 FPM"
    service php5-fpm restart
  fi

  if [[ $(which apache2 | wc -l) -gt 0 ]]; then
    echo "Restarting Apache"
    service apache2 restart
  fi

  if [[ $(which nginx | wc -l) -gt 0 ]]; then
    echo "Restarting Nginx"
    service nginx restart
  fi
fi
