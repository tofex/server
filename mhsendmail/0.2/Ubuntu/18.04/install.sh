#!/bin/bash -e

install-package ca-certificates
install-package curl
install-package git

echo "Downloading Go"
curl -Lsf 'https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz' | tar -C '/usr/local' -xvzf -

PATH="/usr/local/go/bin:${PATH}"

echo "Building mhsendmail"
go get github.com/mailhog/mhsendmail

cp /root/go/bin/mhsendmail /usr/bin/mhsendmail
