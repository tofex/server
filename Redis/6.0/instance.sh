#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Bind address, default: 127.0.0.1
  -p  Port, default: 6379
  -t  type, i.e. cache, session, fullPageCache

Example: ${scriptName} -b 0.0.0.0 -p 6380 -t session
EOF
}

trim()
{
  echo -n "$1" | xargs
}

bindAddress=
port=
type=

while getopts hb:p:t:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) bindAddress=$(trim "$OPTARG");;
    p) port=$(trim "$OPTARG");;
    t) type=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${bindAddress}" ]]; then
  bindAddress="127.0.0.1"
fi

if [[ -z "${port}" ]]; then
  port=6379
fi

if [[ ! -f /.dockerenv ]] && [[ -z "${type}" ]]; then
  echo "No type specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

if [[ -n "${type}" ]]; then
  "${distributionReleaseScript}" -b "${bindAddress}" -p "${port}" -t "${type}"
else
  "${distributionReleaseScript}" -b "${bindAddress}" -p "${port}"
fi
