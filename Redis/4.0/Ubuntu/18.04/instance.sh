#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Bind address, default: 127.0.0.1
  -p  Port, default: 6379
  -t  type, i.e. cache, session, fullPageCache

Example: ${scriptName} -b 0.0.0.0 -t session -p 6380
EOF
}

trim()
{
  echo -n "$1" | xargs
}

bindAddress=
port=
type=

while getopts hb:p:t:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) bindAddress=$(trim "$OPTARG");;
    p) port=$(trim "$OPTARG");;
    t) type=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${bindAddress}" ]]; then
  bindAddress="127.0.0.1"
fi

if [[ -z "${port}" ]]; then
  port=6379
fi

if [[ ! -f /.dockerenv ]] && [[ -z "${type}" ]]; then
  echo "No type specified!"
  exit 1
fi

install-package build-essential
install-package tcl8.5

if [[ ! -d /usr/local/source/redis/redis-4.0.12 ]]; then
  mkdir -p /usr/local/source/redis
  cd /usr/local/source/redis
  wget -nv http://download.redis.io/releases/redis-4.0.12.tar.gz
  tar xzf redis-4.0.12.tar.gz
  cd redis-4.0.12
  make
  make install
else
  cd /usr/local/source/redis/redis-4.0.12
fi

cd utils
mkdir -p /etc/redis/
mkdir -p /var/log/redis
mkdir -p /var/lib/redis
REDIS_PORT=${port} \
REDIS_CONFIG_FILE=/etc/redis/redis_${port}.conf \
REDIS_LOG_FILE=/var/log/redis/${port}.log \
REDIS_DATA_DIR=/var/lib/redis/${port} \
REDIS_EXECUTABLE=$(command -v redis-server) \
./install_server.sh

add-file-content-before /etc/security/limits.conf "root  soft  nofile  10240" "# End of file" 1
add-file-content-before /etc/security/limits.conf "root  hard  nofile  1048576" "# End of file" 1
sysctl -p

echo "Setting bind address to: ${bindAddress}"
replace-file-content "/etc/redis/redis_${port}.conf" "bind ${bindAddress}" "bind 127.0.0.1" 0

if [[ -f /.dockerenv ]]; then
  echo "Stopping Redis"
  "/etc/init.d/redis_${port}" stop

  echo "Creating start script at: /usr/local/bin/redis.sh"
  cat <<EOF > /usr/local/bin/redis.sh
#!/bin/bash -e
/usr/local/bin/redis-server /etc/redis/redis_${port}.conf --daemonize no
EOF
  chmod +x /usr/local/bin/redis.sh
else
  echo "Restarting Redis"
  service "redis_${port}" restart
fi

mkdir -p /opt/install/
if [[ -f /.dockerenv ]]; then
  crudini --set /opt/install/env.properties redis "version" "4.0"
  crudini --set /opt/install/env.properties redis "port" "${port}"
else
  crudini --set /opt/install/env.properties redis "${type}Version" "4.0"
  crudini --set /opt/install/env.properties redis "${type}Port" "${port}"
fi
