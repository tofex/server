#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  HTTP port, default: 80
  -s  SSL port, default: 443
  -e  Web user, default: www-data
  -g  Web group, default: www-data
  -c  Target system protocol, default: http
  -x  Target system host name, default: localhost
  -y  Target system port
  -l  Log path, default: /var/log/nginx
  -v  Server name
  -t  SSL terminated (yes/no), default: no
  -f  Force SSL (yes/no), default: yes
  -u  Basic auth user name (optional)
  -p  Basic auth password (optional)
  -i  Basic auth user file path (optional), default: /var/www
  -o  Overwrite existing files (yes/no), default: no

Example: ${scriptName} -w /var/www/project01/htdocs -v project01.net -t yes -f no
EOF
}

trim()
{
  echo -n "$1" | xargs
}

nginxHttpPort=80
nginxSslPort=443
webUser="www-data"
webGroup="www-data"
proxyProtocol="http"
proxyHost="localhost"
proxyPort=
logPath="/var/log/nginx"
serverName=
sslTerminated="no"
forceSsl="yes"
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath="/var/www"
overwrite="no"

while getopts hr:s:e:g:c:x:y::l:v:t:f:u:p:i:o:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) nginxHttpPort=$(trim "$OPTARG");;
    s) nginxSslPort=$(trim "$OPTARG");;
    e) webUser=$(trim "$OPTARG");;
    g) webGroup=$(trim "$OPTARG");;
    c) proxyProtocol=$(trim "$OPTARG");;
    x) proxyHost=$(trim "$OPTARG");;
    y) proxyPort=$(trim "$OPTARG");;
    l) logPath=$(trim "$OPTARG");;
    v) serverName=$(trim "$OPTARG");;
    t) sslTerminated=$(trim "$OPTARG");;
    f) forceSsl=$(trim "$OPTARG");;
    u) basicAuthUserName=$(trim "$OPTARG");;
    p) basicAuthPassword=$(trim "$OPTARG");;
    i) basicAuthUserFilePath=$(trim "$OPTARG");;
    o) overwrite=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${nginxHttpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${nginxSslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${proxyProtocol}" ]]; then
  echo "No proxy protocol specified!"
  exit 1
fi

if [[ -z "${proxyHost}" ]]; then
  echo "No proxy host specified!"
  exit 1
fi

if [[ -z "${proxyPort}" ]]; then
  echo "No proxy port specified!"
  exit 1
fi

if [[ -z "${logPath}" ]]; then
  echo "No log path port specified!"
  exit 1
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${sslTerminated}" ]]; then
  echo "No SSL terminated specified!"
  exit 1
fi

if [[ -z "${forceSsl}" ]]; then
  echo "No force SSL specified!"
  exit 1
fi

if [[ ! -d "${logPath}" ]]; then
  sudo mkdir -p "${logPath}"
  sudo chown "${webUser}":"${webGroup}" "${logPath}"
fi

if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]] && [[ ! -d "${basicAuthUserFilePath}" ]]; then
  sudo mkdir -p "${basicAuthUserFilePath}"
  sudo chown "${webUser}":"${webGroup}" "${basicAuthUserFilePath}"
fi

if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
  echo "Adding basic user in file at: ${basicAuthUserFilePath}/${serverName}.htpasswd"
  if [[ -f "${basicAuthUserFilePath}/${serverName}.htpasswd" ]]; then
    $(htpasswd -vb "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}" >/dev/null 2>&1)
    result=$?
    if [[ "${result}" -ne 0 ]]; then
      htpasswd -b "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}"
    else
      echo "User already added"
    fi
  else
    htpasswd -b -c "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}"
  fi
  sudo chown "${webUser}":"${webGroup}" "${basicAuthUserFilePath}/${serverName}.htpasswd"
fi

if [[ "${overwrite}" == "no" ]]; then
  if [[ -f "/etc/nginx/conf.d/${serverName}.conf" ]]; then
    echo "Configuration \"/etc/nginx/conf.d/${serverName}.conf\" already exists"
    exit 1
  fi
fi

echo "Creating configuration at: /etc/nginx/conf.d/${serverName}.conf"

if [[ ${forceSsl} == "yes" ]] && [[ ${sslTerminated} == "no" ]]; then
  cat <<EOF | sudo tee "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
server {
  listen 0.0.0.0:${nginxHttpPort};
  server_name ${serverName};
  return 301 https://\$host\$request_uri;
  error_log ${logPath}/${serverName}-nginx-http-error.log error;
  access_log ${logPath}/${serverName}-nginx-http-access.log custom;
}
EOF
else
  cat <<EOF | sudo tee "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
server {
  listen 0.0.0.0:${nginxHttpPort};
  server_name ${serverName};
EOF

  if [[ ${sslTerminated} == "yes" ]]; then
    cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  if (\$http_x_forwarded_proto = "http") {
    return 301 https://\$host\$request_uri;
  }
EOF
  fi

  if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
    cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  auth_basic "${serverName}";
  auth_basic_user_file ${basicAuthUserFilePath}/${serverName}.htpasswd;
EOF
  fi

  cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  location / {
    proxy_pass                          ${proxyProtocol}://${proxyHost}:${proxyPort}/;
    proxy_set_header Host               \$http_host;
    proxy_set_header X-Forwarded-Host   \$http_host;
    proxy_set_header X-Forwarded-For    \$http_x_forwarded_for;
    proxy_set_header X-Forwarded-Port   \$server_port;
    proxy_set_header Https-Forwarded    "1";
    proxy_set_header X-Forwarded-Proto  "https";
    proxy_set_header X-WEBAUTH-USER     \$remote_user;
    proxy_set_header Authorization      "";
  }
EOF

  cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  error_log ${logPath}/${serverName}-nginx-http-error.log warn;
  access_log ${logPath}/${serverName}-nginx-http-access.log custom;
}
EOF
fi

if [[ ${sslTerminated} == "no" ]]; then
  cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
server {
  listen 0.0.0.0:${nginxSslPort};
  server_name ${serverName};
  ssl on;
  ssl_certificate /etc/ssl/certs/default.pem;
  ssl_certificate_key /etc/ssl/private/default.key;
  ssl_session_timeout 10m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
  ssl_prefer_server_ciphers on;
EOF

  if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
    cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  auth_basic "${serverName}";
  auth_basic_user_file ${basicAuthUserFilePath}/${serverName}.htpasswd;
EOF
  fi

  cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  location / {
    proxy_pass                          ${proxyProtocol}://${proxyHost}:${proxyPort}/;
    proxy_set_header Host               \$http_host;
    proxy_set_header X-Forwarded-Host   \$http_host;
    proxy_set_header X-Forwarded-For    \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Port   \$server_port;
    proxy_set_header Https-Forwarded    "1";
    proxy_set_header X-Forwarded-Proto  "https";
    proxy_set_header X-WEBAUTH-USER     \$remote_user;
    proxy_set_header Authorization      "";
  }
EOF

  cat <<EOF | sudo tee -a "/etc/nginx/conf.d/${serverName}.conf" > /dev/null
  error_log ${logPath}/${serverName}-nginx-ssl-error.log warn;
  access_log ${logPath}/${serverName}-nginx-ssl-access.log custom;
}
EOF
fi

echo "Restarting Nginx"
service nginx restart
