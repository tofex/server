#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  HTTP port, default: 80
  -s  SSL Port, default: 443
  -c  SSL certificate file, default: /etc/ssl/certs/ssl-cert-snakeoil.pem
  -k  SSL key file, default: /etc/ssl/private/ssl-cert-snakeoil.key
  -v  PHP version, default: auto determined

Example: ${scriptName} -p 80 -s 443
EOF
}

trim()
{
  echo -n "$1" | xargs
}

httpPort="80"
sslPort="443"
sslCertFile="/etc/ssl/certs/ssl-cert-snakeoil.pem"
sslKeyFile="/etc/ssl/private/ssl-cert-snakeoil.key"
phpVersion=

while getopts hp:s:c:k:v:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) httpPort=$(trim "$OPTARG");;
    s) sslPort=$(trim "$OPTARG");;
    c) sslCertFile=$(trim "$OPTARG");;
    k) sslKeyFile=$(trim "$OPTARG");;
    v) phpVersion=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${httpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${sslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${sslCertFile}" ]]; then
  echo "No SSL certificate file specified!"
  exit 1
fi

if [[ ! -f "${sslCertFile}" ]]; then
  echo "Invalid SSL certificate file specified!"
  exit 1
fi

if [[ -z "${sslKeyFile}" ]]; then
  echo "No SSL key file specified!"
  exit 1
fi

if [[ ! -f "${sslKeyFile}" ]]; then
  echo "Invalid SSL key file specified!"
  exit 1
fi

if [[ -z "${phpVersion}" ]]; then
  phpVersion=$(php -v | grep --only-matching --perl-regexp "(PHP )\d+\.\\d+\.\\d+" | cut -c 5-7)
fi

if [[ -z "${phpVersion}" ]]; then
  echo "No PHP version specified!"
  exit 1
fi

echo "Creating configuration at: /etc/nginx/conf.d/default.conf"
cat <<EOF >/etc/nginx/conf.d/default.conf
server {
  listen ${httpPort};
  server_name localhost;
  root /usr/share/nginx/html;
  index index.html index.htm index.php;
  error_page 500 502 503 504  /50x.html;
  location ~ .php\$ {
EOF
if [[ ${phpVersion} == "5.6" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php5-fpm.sock;
EOF
elif [[ ${phpVersion} == "7.0" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.1" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.2" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.3" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.4" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
fi
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_index index.php;
    include fastcgi_params;
  }
  location / {
    try_files \$uri \$uri/ /index.html;
  }
}
server {
  listen ${sslPort};
  server_name localhost;
  root /usr/share/nginx/html;
  index index.html index.htm index.php;
  error_page 500 502 503 504  /50x.html;
  ssl on;
  ssl_certificate ${sslCertFile};
  ssl_certificate_key ${sslKeyFile};
  ssl_session_timeout 5m;
  ssl_protocols SSLv3 TLSv1;
  ssl_ciphers ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv3:+EXP;
  ssl_prefer_server_ciphers on;
  location ~ .php\$ {
EOF
if [[ ${phpVersion} == "5.6" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php5-fpm.sock;
EOF
elif [[ ${phpVersion} == "7.0" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.1" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.2" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.3" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.4" ]]; then
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
fi
cat <<EOF >>/etc/nginx/conf.d/default.conf
    fastcgi_index index.php;
    include fastcgi_params;
  }
  location / {
    try_files \$uri \$uri/ /index.html;
  }
}
EOF

echo "Restarting Nginx"
service nginx restart
