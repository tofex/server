#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  HTTP port, default: 80
  -s  SSL port, default: 443
  -e  Web user, default: www-data
  -g  Web group, default: www-data
  -w  Web path
  -l  Log path, default: /var/log/apache2
  -v  Server name
  -t  SSL terminated (yes/no), default: no
  -f  Force SSL (yes/no), default: yes
  -u  Basic auth user name (optional)
  -p  Basic auth password (optional)
  -i  Basic auth user file path (optional), default: /var/www
  -o  Overwrite existing files (yes/no), default: no

Example: ${scriptName} -w /var/www/project01/htdocs -v project01.net -t yes -f no
EOF
}

trim()
{
  echo -n "$1" | xargs
}

nginxHttpPort=80
nginxSslPort=443
webUser="www-data"
webGroup="www-data"
webPath=
logPath="/var/log/apache2"
serverName=
sslTerminated="no"
forceSsl="yes"
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath="/var/www"
overwrite="no"

while getopts hr:s:e:g:w:l:v:t:f:u:p:i:o:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) nginxHttpPort=$(trim "$OPTARG");;
    s) nginxSslPort=$(trim "$OPTARG");;
    e) webUser=$(trim "$OPTARG");;
    g) webGroup=$(trim "$OPTARG");;
    w) webPath=$(trim "$OPTARG");;
    l) logPath=$(trim "$OPTARG");;
    v) serverName=$(trim "$OPTARG");;
    t) sslTerminated=$(trim "$OPTARG");;
    f) forceSsl=$(trim "$OPTARG");;
    u) basicAuthUserName=$(trim "$OPTARG");;
    p) basicAuthPassword=$(trim "$OPTARG");;
    i) basicAuthUserFilePath=$(trim "$OPTARG");;
    o) overwrite=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${nginxHttpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${nginxSslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${webPath}" ]]; then
  echo "No web path port specified!"
  exit 1
fi

if [[ -z "${logPath}" ]]; then
  echo "No log path port specified!"
  exit 1
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${sslTerminated}" ]]; then
  echo "No SSL terminated specified!"
  exit 1
fi

if [[ -z "${forceSsl}" ]]; then
  echo "No force SSL specified!"
  exit 1
fi

if [[ ! -d "${webPath}" ]]; then
  sudo mkdir -p "${webPath}"
  sudo chown "${webUser}":"${webGroup}" "${webPath}"
fi

if [[ ! -d "${logPath}" ]]; then
  sudo mkdir -p "${logPath}"
  sudo chown "${webUser}":"${webGroup}" "${logPath}"
fi

if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]] && [[ ! -d "${basicAuthUserFilePath}" ]]; then
  sudo mkdir -p "${basicAuthUserFilePath}"
  sudo chown "${webUser}":"${webGroup}" "${basicAuthUserFilePath}"
fi

if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
  echo "Adding basic user in file at: ${basicAuthUserFilePath}/${serverName}.htpasswd"
  if [[ -f "${basicAuthUserFilePath}/${serverName}.htpasswd" ]]; then
    $(htpasswd -vb "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}" >/dev/null 2>&1)
    result=$?
    if [[ "${result}" -ne 0 ]]; then
      htpasswd -b "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}"
    else
      echo "User already added"
    fi
  else
    htpasswd -b -c "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}"
  fi
  sudo chown "${webUser}":"${webGroup}" "${basicAuthUserFilePath}/${serverName}.htpasswd"
fi

if [[ "${overwrite}" == "no" ]]; then
  if [[ -f "/etc/nginx/conf.d/${serverName}.conf" ]]; then
    echo "Configuration \"/etc/nginx/conf.d/${serverName}.conf\" already exists"
    exit 1
  fi
fi

phpVersion=$(php -v | head -n 1 | cut -d " " -f 2 | cut -f1-2 -d".")

echo "Creating configuration at: /etc/apache2/sites-available/${serverName}.conf"

cat <<EOF > "/etc/nginx/conf.d/${serverName}.conf"
server {
  listen ${nginxHttpPort};
  server_name localhost;
  root /usr/share/nginx/html;
  index index.html index.htm index.php;
  location ~ .php\$ {
EOF
if [[ ${phpVersion} == "5.6" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php5-fpm.sock;
EOF
elif [[ ${phpVersion} == "7.0" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.1" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.2" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.3" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
fi
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_index index.php;
    include fastcgi_params;
  }
  location / {
    try_files \$uri \$uri/ /index.html;
  }
}
server {
  listen ${nginxSslPort};
  server_name localhost;
  root /usr/share/nginx/html;
  index index.html index.htm index.php;
  ssl on;
  ssl_certificate /etc/ssl/certs/${sslCertFile};
  ssl_certificate_key /etc/ssl/private/${sslKeyFile};
  ssl_session_timeout 5m;
  ssl_protocols SSLv3 TLSv1;
  ssl_ciphers ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv3:+EXP;
  ssl_prefer_server_ciphers on;
  location ~ .php\$ {
EOF
if [[ ${phpVersion} == "5.6" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php5-fpm.sock;
EOF
elif [[ ${phpVersion} == "7.0" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.1" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.2" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
elif [[ ${phpVersion} == "7.3" ]]; then
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
EOF
fi
cat <<EOF >> "/etc/nginx/conf.d/${serverName}.conf"
    fastcgi_index index.php;
    include fastcgi_params;
  }
  location / {
    try_files \$uri \$uri/ /index.html;
  }
}
EOF

echo "Restarting Nginx"
service nginx restart
