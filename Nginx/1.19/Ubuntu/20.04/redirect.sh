#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  HTTP port, default: 80
  -s  SSL Port, default: 443
  -b  Bind IP, default: 0.0.0.0
  -f  From source host
  -t  To target host

Example: ${scriptName} -p 80 -s 443 -f project.net -t www.project.net
EOF
}

trim()
{
  echo -n "$1" | xargs
}

httpPort="80"
sslPort="443"
bindIp="0.0.0.0"
fromHostName=""
toHostName=""

while getopts hp:s:b:f:t:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) httpPort=$(trim "$OPTARG");;
    s) sslPort=$(trim "$OPTARG");;
    b) bindIp=$(trim "$OPTARG");;
    f) fromHostName=$(trim "$OPTARG");;
    t) toHostName=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${httpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${sslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${bindIp}" ]]; then
  echo "No bind IP specified!"
  exit 1
fi

if [[ -z "${fromHostName}" ]]; then
  echo "No source host name specified!"
  exit 1
fi

if [[ -z "${toHostName}" ]]; then
  echo "No target host name specified!"
  exit 1
fi

echo "Adding Ngnix configuration: /etc/nginx/conf.d/${fromHostName}.conf"
cat <<EOF > "/etc/nginx/conf.d/${fromHostName}.conf"
server {
  listen ${bindIp}:${httpPort};
  server_name ${fromHostName};
  return 301 http://${toHostName}\$request_uri;
}
server {
  listen ${bindIp}:${sslPort};
  server_name ${fromHostName};
  return 301 https://${toHostName}\$request_uri;
}
EOF

echo "Restarting Nginx"
service nginx restart
