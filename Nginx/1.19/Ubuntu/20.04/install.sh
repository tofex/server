#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  HTTP port, default: 80
  -s  SSL Port, default: 443
  -c  SSL certificate file, default: /etc/ssl/certs/ssl-cert-snakeoil.pem
  -k  SSL key file, default: /etc/ssl/private/ssl-cert-snakeoil.key

Example: ${scriptName} -p 80 -s 443
EOF
}

trim()
{
  echo -n "$1" | xargs
}

httpPort="80"
sslPort="443"
sslCertFile="/etc/ssl/certs/ssl-cert-snakeoil.pem"
sslKeyFile="/etc/ssl/private/ssl-cert-snakeoil.key"

while getopts hp:s:c:k:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) httpPort=$(trim "$OPTARG");;
    s) sslPort=$(trim "$OPTARG");;
    c) sslCertFile=$(trim "$OPTARG");;
    k) sslKeyFile=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${httpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${sslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${sslCertFile}" ]]; then
  echo "No SSL certificate file specified!"
  exit 1
fi

if [[ ! -f "${sslCertFile}" ]]; then
  echo "Invalid SSL certificate file specified!"
  exit 1
fi

if [[ -z "${sslKeyFile}" ]]; then
  echo "No SSL key file specified!"
  exit 1
fi

if [[ ! -f "${sslKeyFile}" ]]; then
  echo "Invalid SSL key file specified!"
  exit 1
fi

add-certificate "default" "${sslCertFile}" "${sslKeyFile}"

add-repository "nginx.list" "http://nginx.org/packages/mainline/ubuntu/" "focal" "nginx" "http://nginx.org/keys/nginx_signing.key" "y"
install-package nginx 1.19.0-1~focal
install-package ssl-cert

add-file-content-before /etc/security/limits.conf "nginx       soft    nofile  32768" "# End of file" 1
add-file-content-before /etc/security/limits.conf "nginx       hard    nofile  65536" "# End of file" 1
sysctl -p

service nginx stop

rm -rf /var/log/nginx/*

replace-file-content /etc/nginx/nginx.conf "nginx-access.log" "access.log"
replace-file-content /etc/nginx/nginx.conf "nginx-error.log" "error.log"
replace-file-content /etc/nginx/nginx.conf "sendfile off" "sendfile on"
replace-file-content /etc/nginx/nginx.conf "worker_connections  32768;" "worker_connections  1024;"
add-file-content-after /etc/nginx/nginx.conf "worker_rlimit_nofile 32768;" "pid        /var/run/nginx.pid;" 1

echo "<?php phpinfo();" > /usr/share/nginx/html/index.php

usermod -a -G www-data nginx

service nginx start

mkdir -p /opt/install/
crudini --set /opt/install/env.properties nginx version "1.19"
crudini --set /opt/install/env.properties nginx httpPort "${httpPort}"
crudini --set /opt/install/env.properties nginx sslPort "${sslPort}"
