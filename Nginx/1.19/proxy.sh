#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  HTTP port, default: 80
  -s  SSL port, default: 443
  -e  Web user, default: www-data
  -g  Web group, default: www-data
  -c  Target system protocol, default: http
  -x  Target system host name, default: localhost
  -y  Target system port
  -l  Log path, default: /var/log/nginx
  -v  Server name
  -t  SSL terminated (yes/no), default: no
  -f  Force SSL (yes/no), default: yes
  -u  Basic auth user name (optional)
  -p  Basic auth password (optional)
  -i  Basic auth user file path (optional), default: /var/www
  -o  Overwrite existing files (yes/no), default: no

Example: ${scriptName} -w /var/www/project01/htdocs -v project01.net -t yes -f no
EOF
}

trim()
{
  echo -n "$1" | xargs
}

nginxHttpPort=80
nginxSslPort=443
webUser="www-data"
webGroup="www-data"
proxyProtocol="http"
proxyHost="localhost"
proxyPort=
logPath="/var/log/nginx"
serverName=
sslTerminated="no"
forceSsl="yes"
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath="/var/www"
overwrite="no"

while getopts hr:s:e:g:c:x:y::l:v:t:f:u:p:i:o:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) nginxHttpPort=$(trim "$OPTARG");;
    s) nginxSslPort=$(trim "$OPTARG");;
    e) webUser=$(trim "$OPTARG");;
    g) webGroup=$(trim "$OPTARG");;
    c) proxyProtocol=$(trim "$OPTARG");;
    x) proxyHost=$(trim "$OPTARG");;
    y) proxyPort=$(trim "$OPTARG");;
    l) logPath=$(trim "$OPTARG");;
    v) serverName=$(trim "$OPTARG");;
    t) sslTerminated=$(trim "$OPTARG");;
    f) forceSsl=$(trim "$OPTARG");;
    u) basicAuthUserName=$(trim "$OPTARG");;
    p) basicAuthPassword=$(trim "$OPTARG");;
    i) basicAuthUserFilePath=$(trim "$OPTARG");;
    o) overwrite=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${nginxHttpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${nginxSslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${proxyProtocol}" ]]; then
  echo "No proxy protocol specified!"
  exit 1
fi

if [[ -z "${proxyHost}" ]]; then
  echo "No proxy host specified!"
  exit 1
fi

if [[ -z "${proxyPort}" ]]; then
  echo "No proxy port specified!"
  exit 1
fi

if [[ -z "${logPath}" ]]; then
  echo "No log path port specified!"
  exit 1
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${sslTerminated}" ]]; then
  echo "No SSL terminated specified!"
  exit 1
fi

if [[ -z "${forceSsl}" ]]; then
  echo "No force SSL specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" \
  -r "${nginxHttpPort}" \
  -s "${nginxSslPort}" \
  -e "${webUser}" \
  -g "${webGroup}" \
  -c "${proxyProtocol}" \
  -x "${proxyHost}" \
  -y "${proxyPort}" \
  -l "${logPath}" \
  -v "${serverName}" \
  -t "${sslTerminated}" \
  -f "${forceSsl}" \
  -u "${basicAuthUserName}" \
  -p "${basicAuthPassword}" \
  -i "${basicAuthUserFilePath}" \
  -o "${overwrite}"
