#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  HTTP port, default: 80
  -s  SSL Port, default: 443
  -b  Bind IP, default: 0.0.0.0
  -f  From source host
  -t  To target host

Example: ${scriptName} -p 80 -s 443 -f project.net -t www.project.net
EOF
}

trim()
{
  echo -n "$1" | xargs
}

httpPort="80"
sslPort="443"
bindIp="0.0.0.0"
fromHostName=""
toHostName=""

while getopts hp:s:b:f:t:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) httpPort=$(trim "$OPTARG");;
    s) sslPort=$(trim "$OPTARG");;
    b) bindIp=$(trim "$OPTARG");;
    f) fromHostName=$(trim "$OPTARG");;
    t) toHostName=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${httpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${sslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${bindIp}" ]]; then
  echo "No bind IP specified!"
  exit 1
fi

if [[ -z "${fromHostName}" ]]; then
  echo "No source host name specified!"
  exit 1
fi

if [[ -z "${toHostName}" ]]; then
  echo "No target host name specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -p "${httpPort}" -s "${sslPort}" -b "${bindIp}" -f "${fromHostName}" -t "${toHostName}"
