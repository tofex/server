#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Bind address, default: 127.0.0.1
  -p  Port, default: 9200

Example: ${scriptName} -b 0.0.0.0 -p 9200
EOF
}

trim()
{
  echo -n "$1" | xargs
}

bindAddress="127.0.0.1"
port="9200"

while getopts hb:p:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) bindAddress=$(trim "$OPTARG");;
    p) port=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${bindAddress}" ]]; then
  echo "No bind address specified!"
  exit 1
fi

if [[ -z "${port}" ]] || [[ "${port}" == "-" ]]; then
  echo "No port specified!"
  exit 1
fi

adduser --system --shell /bin/bash -U 10001 --no-create-home opensearch
groupadd opensearch
usermod -aG opensearch opensearch
mkdir -p /home/opensearch
chown -R opensearch /home/opensearch

wget -nv https://artifacts.opensearch.org/releases/bundle/opensearch/2.9.0/opensearch-2.9.0-linux-x64.tar.gz
tar xf opensearch-2.9.0-linux-x64.tar.gz
mv opensearch-2.9.0 /opt/opensearch
chown -R opensearch /opt/opensearch

echo "Setting bind address to: ${bindAddress}"
replace-file-content /opt/opensearch/config/opensearch.yml "network.host: ${bindAddress}" "#network.host: 192.168.0.1" 0

echo "Setting port to: ${port}"
replace-file-content /opt/opensearch/config/opensearch.yml "http.port: ${port}" "#http.port: 9200" 0

echo "Setting discovery seed hosts to: single-node"
replace-file-content /opt/opensearch/config/opensearch.yml "discovery.type: single-node" "#discovery.seed_hosts: [\"host1\", \"host2\"]" 0

echo "plugins.security.disabled: true" >> /opt/opensearch/config/opensearch.yml

if [[ -f /.dockerenv ]]; then
  echo "Creating start script at: /usr/local/bin/opensearch.sh"
  cat <<EOF > /usr/local/bin/opensearch.sh
#!/bin/bash -e
ulimit -n 65535
sysctl -w vm.max_map_count=262144
sudo -H -u opensearch bash -c "/opt/opensearch/opensearch-tar-install.sh"
EOF
  chmod +x /usr/local/bin/opensearch.sh
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties opensearch version "2.9"
crudini --set /opt/install/env.properties opensearch port "${port}"
