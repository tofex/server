#!/bin/bash -e

install-package libwxbase3.0-0v5
install-package libnotify4
install-package libsctp1
install-package-from-deb libwxgtk3.0-0v5 3.0.4+dfsg-12 http://security.ubuntu.com/ubuntu/pool/universe/w/wxwidgets3.0/libwxgtk3.0-0v5_3.0.4+dfsg-12_amd64.deb
install-package-from-deb libssl1.0.0 1.0.2n-1ubuntu5.3 http://security.ubuntu.com/ubuntu/pool/main/o/openssl1.0/libssl1.0.0_1.0.2n-1ubuntu5.3_amd64.deb
install-package-from-deb esl-erlang 1:18.3 "https://packages.erlang-solutions.com/erlang/debian/pool/esl-erlang_18.3.4.11-1~ubuntu~bionic_amd64.deb"
install-package-from-deb rabbitmq-server 3.5.3-1 "http://www.rabbitmq.com/releases/rabbitmq-server/v3.5.3/rabbitmq-server_3.5.3-1_all.deb"

rabbitmq-plugins enable rabbitmq_management

echo "[{rabbit, [{loopback_users, []}]}]." > /etc/rabbitmq/rabbitmq.config

service rabbitmq-server restart

mkdir -p /opt/install/
crudini --set /opt/install/env.properties rabbitmq version "3.5"
crudini --set /opt/install/env.properties rabbitmq port "5672"
