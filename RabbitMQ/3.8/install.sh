#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Bind address, default: 127.0.0.1
  -p  Port, default: 5672
  -m  Management port, default: 15672

Example: ${scriptName} -b 0.0.0.0 -p 5672 -m 15672
EOF
}

trim()
{
  echo -n "$1" | xargs
}

bindAddress="127.0.0.1"
port="5672"
managementPort="15672"

while getopts hb:p:m:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) bindAddress=$(trim "$OPTARG");;
    p) port=$(trim "$OPTARG");;
    m) managementPort=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${bindAddress}" ]]; then
  echo "No bind address specified!"
  exit 1
fi

if [[ -z "${port}" ]] || [[ "${port}" == "-" ]]; then
  echo "No port specified!"
  exit 1
fi

if [[ -z "${managementPort}" ]] || [[ "${managementPort}" == "-" ]]; then
  echo "No management port specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -b "${bindAddress}" -p "${port}" -m "${managementPort}"
