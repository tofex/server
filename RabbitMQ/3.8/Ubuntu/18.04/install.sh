#!/bin/bash -e

add-server-key-id "hkps://keys.openpgp.org" "0x0A9AF2115F4687BD29803A206B73A36E6026DFCA"
add-server-key-id "keyserver.ubuntu.com" "F77F1EDA57EBB1CC"
add-repository "rabbitmq.list" "https://packagecloud.io/rabbitmq/rabbitmq-server/ubuntu/" "bionic" "main" "https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey" "y"

## Install Erlang packages
install-package erlang-base
install-package erlang-asn1
install-package erlang-crypto
install-package erlang-eldap
install-package erlang-ftp
install-package erlang-inets
install-package erlang-mnesia
install-package erlang-os-mon
install-package erlang-parsetools
install-package erlang-public-key
install-package erlang-runtime-tools
install-package erlang-snmp
install-package erlang-ssl
install-package erlang-syntax-tools
install-package erlang-tftp
install-package erlang-tools
install-package erlang-xmerl

## Install rabbitmq-server and its dependencies
install-package rabbitmq-server 3.8.21-1

rabbitmq-plugins enable rabbitmq_management

echo "[{rabbit, [{loopback_users, []}]}]." > /etc/rabbitmq/rabbitmq.config

service rabbitmq-server restart

# add tofex user and delete guest
rabbitmqctl add_user tofex tofexjena
rabbitmqctl set_user_tags tofex administrator
rabbitmqctl set_permissions -p / tofex ".*" ".*" ".*"
rabbitmqctl delete_user guest

mkdir -p /opt/install/
crudini --set /opt/install/env.properties rabbitmq version "3.8"
crudini --set /opt/install/env.properties rabbitmq port "5672"
