#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -b  Bind address, default: 127.0.0.1
  -p  Port, default: 5672
  -m  Management port, default: 15672

Example: ${scriptName} -b 0.0.0.0 -p 5672 -m 15672
EOF
}

trim()
{
  echo -n "$1" | xargs
}

bindAddress="127.0.0.1"
port="5672"
managementPort="15672"

while getopts hb:p:m:? option; do
  case ${option} in
    h) usage; exit 1;;
    b) bindAddress=$(trim "$OPTARG");;
    p) port=$(trim "$OPTARG");;
    m) managementPort=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${bindAddress}" ]]; then
  echo "No bind address specified!"
  exit 1
fi

if [[ -z "${port}" ]] || [[ "${port}" == "-" ]]; then
  echo "No port specified!"
  exit 1
fi

if [[ -z "${managementPort}" ]] || [[ "${managementPort}" == "-" ]]; then
  echo "No management port specified!"
  exit 1
fi

add-server-key-id "hkps://keys.openpgp.org" "0x0A9AF2115F4687BD29803A206B73A36E6026DFCA"
add-server-key-id "keyserver.ubuntu.com" "F77F1EDA57EBB1CC"
add-repository "rabbitmq.list" "https://packagecloud.io/rabbitmq/rabbitmq-server/ubuntu/" "focal" "main" "https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey" "y"

## Install Erlang packages
install-package erlang-base
install-package erlang-asn1
install-package erlang-crypto
install-package erlang-eldap
install-package erlang-ftp
install-package erlang-inets
install-package erlang-mnesia
install-package erlang-os-mon
install-package erlang-parsetools
install-package erlang-public-key
install-package erlang-runtime-tools
install-package erlang-snmp
install-package erlang-ssl
install-package erlang-syntax-tools
install-package erlang-tftp
install-package erlang-tools
install-package erlang-xmerl

## Install rabbitmq-server and its dependencies
install-package rabbitmq-server 3.8.12

rabbitmq-plugins enable rabbitmq_management

echo "[{rabbit, [{loopback_users, []}, {tcp_listeners, [{\"${bindAddress}\", ${port}}]}]}, {rabbitmq_management, [{listener, [{ip, \"${bindAddress}\"}, {port, ${managementPort}}]}]}]." > /etc/rabbitmq/rabbitmq.config

service rabbitmq-server restart

echo "Adding tofex user"
rabbitmqctl add_user tofex tofexjena
rabbitmqctl set_user_tags tofex administrator
rabbitmqctl set_permissions -p / tofex ".*" ".*" ".*"

echo "Deleting guest user"
rabbitmqctl delete_user guest

if [[ -f /.dockerenv ]]; then
  echo "Stopping RabbitMQ server"
  service rabbitmq-server stop

  echo "Creating start script at: /usr/local/bin/rabbitmq.sh"
  cat <<EOF > /usr/local/bin/rabbitmq.sh
#!/bin/bash -e
sudo -H -u rabbitmq bash -c "/usr/sbin/rabbitmq-server"
EOF
  chmod +x /usr/local/bin/rabbitmq.sh
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties rabbitmq version "3.8"
crudini --set /opt/install/env.properties rabbitmq port "${port}"
crudini --set /opt/install/env.properties rabbitmq managementPort "${managementPort}"
