#!/bin/bash -e

if [[ $(which install-package | wc -l) -eq 1 ]]; then
  echo "Using custom install function"
  customInstall=1
else
  echo "Using native install function"
  customInstall=0
fi

if [[ $(which git | wc -l) -eq 0 ]]; then
  if [[ "${customInstall}" == 1 ]]; then
    install-package git
  else
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages git 2>&1
  fi
fi

echo "Determining latest hash"
gitHash=$(git ls-remote https://bitbucket.org/tofex/server.git)
if [[ -z "${gitHash}" ]]; then
  echo "Could not list remote hashes, try again in 3 seconds"
  sleep 3
  gitHash=$(git ls-remote https://bitbucket.org/tofex/server.git)
  if [[ -z "${gitHash}" ]]; then
    echo "Could not list remote hashes, try again in another 3 seconds"
    sleep 3
    gitHash=$(git ls-remote https://bitbucket.org/tofex/server.git)
    if [[ -z "${gitHash}" ]]; then
      echo "Could not list remote hashes, giving up"
      exit 1
    fi
  fi
fi
hash=$(echo "${gitHash}" | head -1 | sed "s/HEAD//" | head -c 12)
if [[ -z "${hash}" ]]; then
  echo "Could not determine latest hash"
  exit 1
fi
echo "Latest hash: ${hash}"

scriptPath="/opt/install/tofex-server-${hash}"
initializedFlagFile="${scriptPath}/initialized.flag"

if [[ -f "${initializedFlagFile}" ]]; then
  echo "Version already initialized"
else
  echo "Checking for system upgrades"
  checkUnattendedUpgrades=$(which check-unattended-upgrades)
  if [[ -n "${checkUnattendedUpgrades}" ]]; then
    check-unattended-upgrades
  else
    apt=$(which apt)
    if [[ -n "${apt}" ]]; then
        first=1
        while [[ $(ps aux | grep -i "${apt}" | grep -v grep | wc -l) -gt 0 ]] || [[ $(ps aux | grep -i "apt.systemd.daily lock_is_held" | grep -v grep | wc -l) -gt 0 ]]; do
            if [[ "${first}" == 1 ]]; then
               echo -n "System not ready"
            else
                echo -n "."
            fi
            sleep 1
            first=0
        done
        if [[ "${first}" == 0 ]]; then
            echo ""
        fi
    fi
  fi

  echo "Updating packages"
  if [[ -n "$(which update-apt)" ]]; then
    update-apt
  else
    apt-get update || apt-get update
  fi

  echo "Checking for system upgrades"
  checkUnattendedUpgrades=$(which check-unattended-upgrades)
  if [[ -n "${checkUnattendedUpgrades}" ]]; then
    check-unattended-upgrades
  else
    apt=$(which apt)
    if [[ -n "${apt}" ]]; then
        first=1
        while [[ $(ps aux | grep -i "${apt}" | grep -v grep | wc -l) -gt 0 ]] || [[ $(ps aux | grep -i "apt.systemd.daily lock_is_held" | grep -v grep | wc -l) -gt 0 ]]; do
            if [[ "${first}" == 1 ]]; then
               echo -n "System not ready"
            else
                echo -n "."
            fi
            sleep 1
            first=0
        done
        if [[ "${first}" == 0 ]]; then
            echo ""
        fi
    fi
  fi

  echo "Installing required packages"
  if [[ $(which install-package | wc -l) -eq 1 ]]; then
    echo "Using custom install function"
    if [[ $(which curl | wc -l) -eq 0 ]]; then
      install-package curl
    fi
    echo "Installing crudini"
    install-package crudini
    echo "Installing git"
    install-package git
    echo "Installing unzip"
    install-package unzip
    echo "Installing ssl-cert"
    install-package ssl-cert
    echo "Installing pwgen"
    install-package pwgen
    echo "Installing apache2-utils"
    install-package apache2-utils
    echo "Installing bc"
    install-package bc
    echo "Installing sudo"
    install-package sudo
    echo "Installing jq"
    install-package jq
    echo "Installing lsb-release"
    install-package lsb-release
    echo "Installing gnupg"
    install-package gnupg
    echo "Installing debian-keyring"
    install-package debian-keyring
    echo "Installing debian-archive-keyring"
    install-package debian-archive-keyring
    echo "Installing apt-transport-https"
    install-package apt-transport-https
  else
    echo "Using native install function"
    if [[ $(which curl | wc -l) -eq 0 ]]; then
      UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages curl 2>&1
    fi
    echo "Installing crudini"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages crudini 2>&1
    echo "Installing git"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages git 2>&1
    echo "Installing unip"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages unzip 2>&1
    echo "Installing ssl-cert"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages ssl-cert 2>&1
    echo "Installing pwgen"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages pwgen 2>&1
    echo "Installing apache2-utils"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages apache2-utils 2>&1
    echo "Installing bc"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages bc 2>&1
    echo "Installing sudo"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages sudo 2>&1
    echo "Installing jq"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages jq 2>&1
    echo "Installing lsb-release"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages lsb-release 2>&1
    echo "Installing gnupg"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages gnupg 2>&1
    echo "Installing debian-keyring"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages debian-keyring 2>&1
    echo "Installing debian-archive-keyring"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages debian-archive-keyring 2>&1
    echo "Installing apt-transport-https"
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages apt-transport-https 2>&1
  fi

  echo "Preparing SSH keys for user: root"
  mkdir -p /root/.ssh/
  touch /root/.ssh/known_hosts
  chmod 0600 /root/.ssh/known_hosts

  echo "Adding Bitbucket SSH Keys"
  sed -i "/^bitbucket\.org/d" /root/.ssh/known_hosts
  ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

  echo "Adding Github SSH Keys"
  sed -i "/^github\.com/d" /root/.ssh/known_hosts
  ssh-keyscan github.com >> /root/.ssh/known_hosts

  echo "Adding Gitlab SSH Keys"
  sed -i "/^gitlab\.com/d" /root/.ssh/known_hosts
  ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
fi

if [[ -L /opt/install/server ]]; then
  echo "Removing old version"
  rm /opt/install/server
fi
mkdir -p /opt/install/
cd /opt/install/

if [[ -d "${scriptPath}" ]]; then
  echo "Latest version already downloaded"
else
  echo "Downloading latest version: https://bitbucket.org/tofex/server/get/${hash}.zip"
  result=$(wget -nv "https://bitbucket.org/tofex/server/get/${hash}.zip" 2>&1)

  if [[ ! -f "${hash}.zip" ]]; then
    if [[ "${result}" =~ "ERROR 429" ]]; then
      counter=0

      until [[ ${counter} -gt 20 ]]; do
        ((counter++))
        echo "Waiting three seconds to avoid too many requests timeout"
        sleep 3
        echo "Downloading latest version: https://bitbucket.org/tofex/server/get/${hash}.zip (retry #${counter})"
        wget -nv "https://bitbucket.org/tofex/server/get/${hash}.zip"
        if [[ -f "${hash}.zip" ]]; then
          break;
        fi
      done
    else
      echo "${result}"
      exit 1
    fi
  fi

  if [[ ! -f "${hash}.zip" ]]; then
    echo "Download failed"
    exit 1
  fi

  echo "Extracting latest version"
  unzip -o -q "${hash}.zip"

  echo "Removing download"
  rm -rf "${hash}.zip"
fi

cd "${scriptPath}"

echo "Scripts downloaded at: ${scriptPath}"
echo "Linking scripts to: /opt/install/server"
ln -s "${scriptPath}" /opt/install/server

if [[ ! -f "${initializedFlagFile}" ]]; then
  ./Server/init.sh
fi

touch "${initializedFlagFile}"
