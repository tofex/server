#!/bin/bash -e

sender=${1:-webmaster@localhost.local}

if [[ -d "/usr/local/php/etc/ini" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /usr/local/php/etc/ini/mailcatcher.ini
fi
if [[ -d "/etc/php5/conf.d" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php5/conf.d/mailcatcher.ini
fi
if [[ -d "/etc/php5/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php5/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/5.5/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/5.5/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/5.6/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/5.6/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/7.0/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/7.0/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/7.1/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/7.1/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/7.2/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/7.2/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/7.3/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/7.3/mods-available/mailcatcher.ini
fi
if [[ -d "/etc/php/7.4/mods-available" ]]; then
    echo "sendmail_path = /usr/bin/env $(which catchmail) -f ${sender}" > /etc/php/7.4/mods-available/mailcatcher.ini
fi

if [[ -n $(which phpenmod) ]]; then
    phpenmod mailcatcher
fi

if [[ -n $(which php5enmod) ]]; then
    php5enmod mailcatcher
fi

if [[ ! -f /.dockerenv ]]; then
  if [[ $(which php5-fpm | wc -l) -gt 0 ]]; then
    service php5-fpm restart
  fi

  if [[ $(which apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(which nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
