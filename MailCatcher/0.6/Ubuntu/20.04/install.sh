#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../../Ruby/2.7/install.sh"

install-package openssl
install-package libssl-dev
install-package libreadline-dev
install-package libgdbm-dev
install-package libsqlite3-dev
install-package make
install-package gcc
install-package g++

"${currentPath}/../../../../Ruby/gem/mime-types/2.99/install.sh"
"${currentPath}/../../../../Ruby/gem/net-imap/0.3.7/install.sh"
"${currentPath}/../../../../Ruby/gem/mailcatcher/0.6/install.sh"

if [[ ! -f /.dockerenv ]]; then
  echo "Creating service at: /etc/systemd/system/mailcatcher.service"
  cat <<EOF > /etc/systemd/system/mailcatcher.service
[Unit]
Description = MailCatcher
After=network.target
After=systemd-user-sessions.service
[Service]
Type=simple
Restart=on-failure
User=root
ExecStart=$(which mailcatcher) --foreground --smtp-ip 0.0.0.0 --ip 0.0.0.0
[Install]
WantedBy=multi-user.target
EOF
  chmod 744 /etc/systemd/system/mailcatcher.service

  echo "Enabling mailcatcher service"
  systemctl enable mailcatcher.service

  echo "Starting mailcatcher service"
  service mailcatcher start
  service mailcatcher status
else
  echo "Creating start script at: /usr/local/bin/mailcatcher.sh"
  cat <<EOF > /usr/local/bin/mailcatcher.sh
#!/bin/bash -e
$(which mailcatcher) --foreground --smtp-ip 0.0.0.0 --ip 0.0.0.0
EOF
  chmod +x /usr/local/bin/mailcatcher.sh
fi
