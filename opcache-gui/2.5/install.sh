#!/bin/bash -e

composer create-project --repository-url=https://packagist.org/ amnuts/opcache-gui=2.5.4 --no-interaction --prefer-dist /var/www/opcache-gui
./server/Apache/2.4/vhost.sh -w /var/www/opcache-gui/ -v opcache.localhost.local
