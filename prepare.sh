#!/bin/bash -e

scriptPath="/opt/install/server"

cd "${scriptPath}"

initializedFlagFile="${scriptPath}/initialized.flag"

if [[ -f "${initializedFlagFile}" ]]; then
  echo "Version already initialized"
else
  echo "Checking for system upgrades"
  checkUnattendedUpgrades=$(which check-unattended-upgrades)
  if [[ -n "${checkUnattendedUpgrades}" ]]; then
    check-unattended-upgrades
  else
    apt=$(which apt)
    if [[ -n "${apt}" ]]; then
        first=1
        while [[ $(ps aux | grep -i "${apt}" | grep -v grep | wc -l) -gt 0 ]] || [[ $(ps aux | grep -i "apt.systemd.daily lock_is_held" | grep -v grep | wc -l) -gt 0 ]]; do
            if [[ "${first}" == 1 ]]; then
               echo -n "System not ready"
            else
                echo -n "."
            fi
            sleep 1
            first=0
        done
        if [[ "${first}" == 0 ]]; then
            echo ""
        fi
    fi
  fi

  echo "Updating packages"
  if [[ -n "$(which update-apt)" ]]; then
    update-apt
  else
    apt-get update
  fi

  echo "Checking for system upgrades"
  checkUnattendedUpgrades=$(which check-unattended-upgrades)
  if [[ -n "${checkUnattendedUpgrades}" ]]; then
    check-unattended-upgrades
  else
    apt=$(which apt)
    if [[ -n "${apt}" ]]; then
        first=1
        while [[ $(ps aux | grep -i "${apt}" | grep -v grep | wc -l) -gt 0 ]] || [[ $(ps aux | grep -i "apt.systemd.daily lock_is_held" | grep -v grep | wc -l) -gt 0 ]]; do
            if [[ "${first}" == 1 ]]; then
               echo -n "System not ready"
            else
                echo -n "."
            fi
            sleep 1
            first=0
        done
        if [[ "${first}" == 0 ]]; then
            echo ""
        fi
    fi
  fi

  echo "Installing required packages"
  if [[ $(which install-package | wc -l) -eq 1 ]]; then
    echo "Using custom install function"
    if [[ $(which curl | wc -l) -eq 0 ]]; then
      install-package curl
    fi
    install-package crudini
    install-package git
    install-package unzip
    install-package ssl-cert
    install-package pwgen
    install-package apache2-utils
    install-package bc
    install-package sudo
    install-package jq
    install-package lsb-release
  else
    echo "Using native install function"
    if [[ $(which curl | wc -l) -eq 0 ]]; then
      UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages curl 2>&1
    fi
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages crudini 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages git 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages unzip 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages ssl-cert 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages pwgen 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages apache2-utils 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages bc 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages sudo 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages jq 2>&1
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages lsb-release 2>&1
  fi

  echo "Preparing SSH keys for user: root"
  mkdir -p /root/.ssh/
  touch /root/.ssh/known_hosts
  chmod 0600 /root/.ssh/known_hosts

  echo "Adding Bitbucket SSH Keys"
  sed -i "/^bitbucket\.org/d" /root/.ssh/known_hosts
  ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

  echo "Adding Github SSH Keys"
  sed -i "/^github\.com/d" /root/.ssh/known_hosts
  ssh-keyscan github.com >> /root/.ssh/known_hosts

  echo "Adding Gitlab SSH Keys"
  sed -i "/^gitlab\.com/d" /root/.ssh/known_hosts
  ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
fi

if [[ ! -f "${initializedFlagFile}" ]]; then
  ./Server/init.sh
fi

touch "${initializedFlagFile}"
