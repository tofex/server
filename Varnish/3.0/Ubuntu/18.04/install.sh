#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host name, default: localhost
  -p  Port, default: 6081
  -a  Admin port, default: 6082

Example: ${scriptName} -n localhost -p 6081
EOF
}

trim()
{
  echo -n "$1" | xargs
}

varnishHost="localhost"
varnishPort="6081"
varnishAdminPort="6082"

while getopts hn:p:a:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) varnishHost=$(trim "$OPTARG");;
    p) varnishPort=$(trim "$OPTARG");;
    a) varnishAdminPort=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${varnishHost}" ]]; then
  echo "No Varnish host specified!"
  exit 1
fi

if [[ -z "${varnishPort}" ]]; then
  echo "No Varnish port specified!"
  exit 1
fi

if [[ -z "${varnishAdminPort}" ]]; then
  echo "No Varnish admin port specified!"
  exit 1
fi

install-package autotools-dev
install-package automake
install-package libtool
install-package autoconf
install-package libncurses5-dev
install-package xsltproc
install-package groff-base
install-package libpcre3-dev
install-package pkg-config
install-package libjemalloc-dev
install-package libreadline-dev
install-package libeditline0

mkdir -p /tmp/varnish
cd /tmp/varnish
wget -nv https://varnish-cache.org/_downloads/varnish-3.0.7.tgz
tar -xf varnish-3.0.7.tgz | cat
cd varnish-3.0.7
sh autogen.sh
sh configure
make
make install

service varnish stop
update-rc.d -f varnish remove

if [[ -f /etc/systemd/system/varnish.service ]]; then
  reloadDaemon=1
else
  reloadDaemon=0
fi

if [[ ! -f /etc/varnish/secret ]]; then
  mkdir -p /etc/varnish
  dd if=/dev/random of=/etc/varnish/secret count=1
fi

cat <<EOF > /etc/systemd/system/varnish.service
[Unit]
Description=Varnish Cache, a high-performance HTTP accelerator
[Service]
Type=forking
LimitNOFILE=131072
LimitMEMLOCK=85983232
LimitCORE=infinity
ExecStart=/usr/local/sbin/varnishd -a :${varnishPort} -T ${varnishHost}:${varnishAdminPort} -f /etc/varnish/varnish.vcl -S /etc/varnish/secret -s malloc,256m
[Install]
WantedBy=multi-user.target
EOF

cp /etc/varnish/default.vcl /etc/varnish/varnish.vcl

if [[ "${reloadDaemon}" == 1 ]]; then
  systemctl daemon-reload
fi

echo "Enabling Varnish autostart"
systemctl enable varnish.service

echo "Restarting Varnish"
service varnish restart

echo "Storing Varnish installation information"
mkdir -p /opt/install/
crudini --set /opt/install/env.properties varnish version "3.0"
crudini --set /opt/install/env.properties varnish port "${varnishPort}"
crudini --set /opt/install/env.properties varnish adminPort "${varnishAdminPort}"
