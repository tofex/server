#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host name, default: localhost
  -p  Port, default: 6081
  -a  Admin port, default: 6082

Example: ${scriptName} -n localhost -p 6081
EOF
}

trim()
{
  echo -n "$1" | xargs
}

varnishHost="localhost"
varnishPort="6081"
varnishAdminPort="6082"

while getopts hn:p:a:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) varnishHost=$(trim "$OPTARG");;
    p) varnishPort=$(trim "$OPTARG");;
    a) varnishAdminPort=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${varnishHost}" ]]; then
  echo "No Varnish host specified!"
  exit 1
fi

if [[ -z "${varnishPort}" ]]; then
  echo "No Varnish port specified!"
  exit 1
fi

if [[ -z "${varnishAdminPort}" ]]; then
  echo "No Varnish admin port specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -n "${varnishHost}" -p "${varnishPort}" -a "${varnishAdminPort}"
