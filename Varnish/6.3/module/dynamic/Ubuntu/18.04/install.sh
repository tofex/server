#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName}

Example: ${scriptName}
EOF
}

trim()
{
  echo -n "$1" | xargs
}

while getopts h:? option; do
  case ${option} in
    h) usage; exit 1;;
    ?) usage; exit 1;;
  esac
done

install-package apt-transport-https
install-package debian-archive-keyring
install-package curl
install-package gnupg
install-package libssl1.1
install-package libssl-dev
add-repository "varnish-cache-6.3.list" "https://packagecloud.io/varnishcache/varnish63/ubuntu/" "bionic" "main" "https://packagecloud.io/varnishcache/varnish63/gpgkey" "y"
install-package varnish-dev 6.3.2-1~bionic
install-package python-docutils
install-package libgetdns-dev
install-package libgetdns10

mkdir -p /tmp/vmod/dynamic
cd /tmp/vmod/dynamic
git clone --branch 6.3 --recursive https://github.com/nigoroll/libvmod-dynamic.git
cd libvmod-dynamic
./autogen.sh
./configure
make
make install
cd /
rm -rf /tmp/vmod/dynamic

service varnish restart
