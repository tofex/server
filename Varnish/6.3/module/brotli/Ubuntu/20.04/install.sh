#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName}

Example: ${scriptName}
EOF
}

trim()
{
  echo -n "$1" | xargs
}

while getopts h:? option; do
  case ${option} in
    h) usage; exit 1;;
    ?) usage; exit 1;;
  esac
done

install-package apt-transport-https
install-package debian-archive-keyring
install-package curl
install-package gnupg
install-package libssl1.1
install-package libssl-dev
add-repository "varnish-cache-6.3.list" "https://packagecloud.io/varnishcache/varnish63/ubuntu/" "bionic" "main" "https://packagecloud.io/varnishcache/varnish63/gpgkey" "y"
install-package varnish-dev 6.3.2-1~bionic

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

/bin/mkdir -p /usr/local/share/man/man3
/bin/install -c -m 644 "${currentPath}"/../../vfp_brotli.3 /usr/local/share/man/man3
cp "${currentPath}"/../../libvmod_brotli.* /usr/lib/varnish/vmods/
chmod +x /usr/lib/varnish/vmods/libvmod_brotli.*

service varnish restart
