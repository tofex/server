.\" Man page generated from reStructuredText.
.
.TH VMOD BROTLI 3 "" "" ""
.SH NAME
VMOD brotli \- Varnish Fetch Processor for brotli de-/compression
.
.nr rst2man-indent-level 0
.
.de1 rstReportMargin
\\$1 \\n[an-margin]
level \\n[rst2man-indent-level]
level margin: \\n[rst2man-indent\\n[rst2man-indent-level]]
-
\\n[rst2man-indent0]
\\n[rst2man-indent1]
\\n[rst2man-indent2]
..
.de1 INDENT
.\" .rstReportMargin pre:
. RS \\$1
. nr rst2man-indent\\n[rst2man-indent-level] \\n[an-margin]
. nr rst2man-indent-level +1
.\" .rstReportMargin post:
..
.de UNINDENT
. RE
.\" indent \\n[an-margin]
.\" old: \\n[rst2man-indent\\n[rst2man-indent-level]]
.nr rst2man-indent-level -1
.\" new: \\n[rst2man-indent\\n[rst2man-indent-level]]
.in \\n[rst2man-indent\\n[rst2man-indent-level]]u
..
.\" 
.
.\" NB:  This file is machine generated, DO NOT EDIT!
.
.\" 
.
.\" Edit vmod.vcc and run make instead
.
.\" 
.
.SH SYNOPSIS
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
import brotli;

# The built\-in "unbr" filter decompresses brotli\-encoded backend
# responses with default parameters.
sub vcl_backend_response {
      if (beresp.http.Content\-Encoding == "br") {
              set beresp.filters = "unbr";
      }
}

# The built\-in "br" filter compresses backend responses with default
# parameters.
sub vcl_backend_response {
      if (bereq.http.Accept\-Encoding ~ "\ebbr\eb") {
              set beresp.filters = "br";
      }
}

# Create a brotli compression filter with custom parameters.
new <obj> = brotli.encoder(STRING name, BYTES buffer, INT quality,
                           BOOL large_win, INT lgwin, ENUM mode)
VOID <obj>.create_stats()

# Create a brotli decompression filter with custom parameters.
new <obj> = brotli.decoder(STRING name, BYTES buffer, BOOL large_win)
VOID <obj>.create_stats()

# VFP version
STRING brotli.version()

# encoder library version
STRING brotli.encoder_version()

# decoder library version
STRING brotli.decoder_version()
.ft P
.fi
.UNINDENT
.UNINDENT
.SH DESCRIPTION
.sp
VFP brotli is a Varnish Fetch Processor to support the brotli
compression algorithm for responses fetched from backends. The VFP
integrates Varnish with the \fI\%Google brotli library\fP\&.
.sp
The Brotli Compressed Data Format is specified in \fI\%RFC 7932\fP\&. Details
of the compression algorithm are beyond the scope of this manual; see
the RFC and library documentation for more information.
.sp
A VFP is technically similar to a Varnish Module (VMOD). In
particular, it must be installed against Varnish in the same way a
VMOD is installed; and as with a VMOD, a VCL source must instruct
Varnish to load the VFP using the \fBimport\fP statement. But unlike a
VMOD, a VFP\(aqs primary purpose is not to extend the VCL language;
instead, a VFP creates filters for backend fetches that can be named
as strings in the space\-separated list assigned to the VCL variable
\fBberesp.filters\fP (see \fBvcl(7)\fP).
.sp
VFP brotli always creates two filters named \fB"br"\fP and \fB"unbr"\fP,
for compression and decompression respectively, with default
parameters for the compression algorithm:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
import brotli;

# Use the built\-in "br" filter for brotli compression with default
# parameters, if the Accept\-Encoding request header indicates that
# brotli compression is accepted.
sub vcl_backend_response {
      if (bereq.http.Accept\-Encoding ~ "\ebbr\eb") {
              set beresp.filters = "br";
      }
}

# Use the built\-in "unbr" filter for brotli decompression with
# default parameters, if the Content\-Encoding response header
# indicates that the response is compressed using brotli.
sub vcl_backend_response {
      if (beresp.http.Content\-Encoding == "br") {
              set beresp.filters = "unbr";
      }
}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Note that the content encoding type \fBbr\fP has been standardized to
indicate brotli compression, for use in headers such as
\fBContent\-Encoding\fP and \fBAccept\-Encoding\fP\&.
.sp
Note also that \fBberesp.filters\fP may only be written or read in the
VCL subroutine \fBvcl_backend_response\fP\&.
.sp
When a brotli filter appears in \fBberesp.filters\fP, it is applied to
the incoming backend response body. The resulting response body is
passed to the client, and if the response is cached, then the body is
stored in the cache as it results from the filtering.
.sp
To use non\-default settings for the compression algorithm, create an
\fBencoder\fP or \fBdecoder\fP object as specified below. The string
passed in the \fBname\fP parameter of the object constructor can be used
in a \fBberesp.filters\fP list, as with \fB"br"\fP and \fB"unbr"\fP:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
import brotli;

sub vcl_init {
      # Create a compression filter with a reduced quality level,
      # for faster processing, but larger compressed responses
      # (the default quality level is 11).
      new brQ10 = brotli.encoder("brQ10", quality=10);

      # Create a decompression filter that uses a 1MiB buffer
      # (the default buffer size is 32KiB).
      new unbr1M = brotli.decoder("unbr1M", buffer=1m);
}

sub vcl_backend_response {
      # Use the custom compression filter defined above.
      if (bereq.http.Accept\-Encoding ~ "\ebbr\eb") {
              set beresp.filters = "brQ10";
      }

      # Use the custom decompression filter.
      if (beresp.http.Content\-Encoding == "br") {
              set beresp.filters = "unbr1M";
      }
}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Parameter settings for the compression algorithm represent various
ways to affect the trade\-off between speed of processing and the rate
of compression (how much compression reduces the size of an object).
.sp
The VFP always creates counter statistics, observable with a tool like
\fBvarnishstat(1)\fP, for the standard \fB"br"\fP and \fB"unbr"\fP
filters. You may also optionally create statistics for custom filters
created with the \fBencoder\fP and \fBdecoder\fP constructors. See
\fI\%STATISTICS\fP below for details.
.SS Compression and HTTP
.sp
The brotli VFP interacts with the HTTP protocol (headers and response
codes) in much the same way that Varnish does with its built\-in gzip
support:
.INDENT 0.0
.IP \(bu 2
Compression filters (the built\-in \fB"br"\fP filter, or a custom
filter created from the \fBencoder\fP constructor) are not executed
for a backend response that already has a \fBContent\-Encoding\fP
header.
.IP \(bu 2
Decompression filters (built\-in \fB"unbr"\fP or a custom \fBdecoder\fP
filter) are not executed unless the backend response has the
\fBContent\-Encoding\fP header set to \fBbr\fP\&.
.IP \(bu 2
When a compression filter is applied, the VFP sets the
\fBContent\-Encoding\fP response header to \fBbr\fP; this header value
may appear in a client response, and it may be cached with the
response. The value \fBAccept\-Encoding\fP is also added to the
\fBVary\fP response header.
.IP \(bu 2
When a decompression filter is applied, the \fBContent\-Encoding\fP
response header is removed.
.IP \(bu 2
Any \fBContent\-Length\fP response header fetched from the backend is
removed \-\- Varnish may add a new \fBContent\-Length\fP header in a
client response, set to the changed size of the response body. When
streaming is enabled, Varnish sends the client response for a fetch
with chunked encoding (and hence with no \fBContent\-Length\fP header).
.IP \(bu 2
If the backend response has an \fBETag\fP header, then the \fBETag\fP
value is "weakened" (prefixed with \fBW/\fP), for weak validation
according to \fI\%RFC 7232\fP section 2.1. This is because the response
after de\-/compression is not byte\-identical with the fetched
response.
.IP \(bu 2
The filters ignore partial responses to range requests (status 206
for "Partial Content").
.UNINDENT
.sp
Note that (unlike gzip) brotli compression does not work together with
Edge Side Includes (ESI). See \fI\%LIMITATIONS\fP below for a discussion of
this issue, and of possible solutions with VCL.
.SS new xencoder = brotli.encoder(STRING name, BYTES buffer, INT quality, BOOL large_win, INT lgwin, ENUM mode)
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
new xencoder = brotli.encoder(
   STRING name,
   BYTES buffer=32768,
   INT quality=11,
   BOOL large_win=0,
   INT lgwin=22,
   ENUM {GENERIC, TEXT, FONT} mode=GENERIC
)
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Create a compression filter named \fBname\fP with custom parameters for
the brotli algorithm. The string given in \fBname\fP may then be used in
\fBberesp.filters\fP\&.
.sp
The default values for the parameters correspond to the defaults used
in the standard compression filter (so if you create a filter with all
defaults, it is functionally identical to the \fB"br"\fP filter).
.sp
The \fBname\fP MAY NOT be \fB"br"\fP or \fB"unbr"\fP, or the same as any of
the standard filters built into Varnish: \fB"esi"\fP, \fB"esi_gzip"\fP,
\fB"gzip"\fP, \fB"gunzip"\fP or \fB"testgunzip"\fP\&. The results are
undefined (and almost certainly not what you want) if you use a name
that is also used by another third\-party VFP.
.sp
The parameters are:
.INDENT 0.0
.IP \(bu 2
\fBbuffer\fP (default 32KiB): Like the \fBvarnishd\fP parameter
\fBgzip_buffer\fP, this is the size of the temporary internal buffer
used for compression. As with \fBgzip_buffer\fP, a buffer size that is
too small results in more overhead, and if it is too large, then it
probably wastes memory.
.IP \(bu 2
\fBquality\fP (default 11): sets a compression level, where 0
represents the fastest execution, and 11 represents the highest
compression rate (smallest compressed result). \fBquality\fP MUST be
in the range 0 to 11, inclusive.
.IP \(bu 2
\fBlarge_win\fP (default \fBfalse\fP): if \fBtrue\fP, use Large Window
Brotli, which may yield better results for response bodies larger
than 16 MiB.  The Large Window algorithm differs from the standard
algorithm; under certain circumstances, it may be necessary to use
the Large Window setting for both compression and decompression.
.IP \(bu 2
\fBlgwin\fP (default 22): set the size of the LZ77 sliding window used
by the brotli algorithm.  The window size is (2 ^ \fBlgwin\fP \- 16),
and larger window sizes may result in better compression rates.
\fBlgwin\fP MUST be in the range 10 to 24, inclusive.
.IP \(bu 2
\fBmode\fP (default \fBGENERIC\fP): provide a hint about the expected
contents of the response body; the compression may benefit from
optimizations based on assumptions about the content. Possible
values are:
.INDENT 2.0
.IP \(bu 2
\fBGENERIC\fP: no assumptions are made about the content
.IP \(bu 2
\fBTEXT\fP: for UTF\-8 formatted text
.IP \(bu 2
\fBFONT\fP: for the WOFF 2.0 font format (the original application
for brotli)
.UNINDENT
.UNINDENT
.sp
For more details about the parameters and their effects, see \fI\%RFC
7932\fP and the library documentation.
.sp
Example:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
import brotli;

sub vcl_init {
      # Create a compression filter with a 1MiB buffer, optimized
      # for WOFF.
      new br_woff = brotli.encoder("br_woff", buffer=1m, mode=FONT);
}

sub vcl_backend_response {
      # Use the custom filter for the WOFF media type.
      if (beresp.http.Content\-Type ~ "\ebfont/woff2?\eb") {
              set beresp.filters = "br_woff";
      }
}
.ft P
.fi
.UNINDENT
.UNINDENT
.SS VOID xencoder.create_stats()
.sp
Create statistics, observable with a tool like \fBvarnishstat(1)\fP, for
the custom compression filter. These are the same as the counters
created for the standard \fB"br"\fP filter. See \fI\%STATISTICS\fP below for
details.
.sp
The \fB\&.create_stats()\fP method MAY NOT be called in any VCL subroutine
besides \fBvcl_init\fP\&. If it is called in another subroutine, then VCL
failure is invoked \-\- ordinarily, the request fails with a "503 VCL
failed" response.
.sp
Example:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
import brotli;

sub vcl_init {
      # Create a custom compression filter with stats.
      new mybr = brotli.encoder("mybr", buffer=64k);
      mybr.create_stats();
}
.ft P
.fi
.UNINDENT
.UNINDENT
.SS new xdecoder = brotli.decoder(STRING name, BYTES buffer, BOOL large_win)
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
new xdecoder = brotli.decoder(
   STRING name,
   BYTES buffer=32768,
   BOOL large_win=0
)
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Create a decompression filter named \fBname\fP with custom parameters,
suitable for use in \fBberesp.filters\fP\&.
.sp
As with the \fBencoder\fP object, the default values for the parameters
correspond to the defaults used in the standard \fB"unbr"\fP
decompression filter.
.sp
Also as with \fBencoder\fP, the \fBname\fP MAY NOT be \fB"br"\fP or
\fB"unbr"\fP, or the same as any of Varnish\(aqs standard filters. It
SHOULD NOT be the same as a name used by another third\-party VFP.
.sp
The parameters are:
.INDENT 0.0
.IP \(bu 2
\fBbuffer\fP (default 32KiB): like the \fBbuffer\fP parameter for the
\fBencoder\fP object, this is the size of the temporary internal
buffer used for decompression.
.IP \(bu 2
\fBlarge_win\fP (default \fBfalse\fP): if \fBtrue\fP, use Large Window
Brotli. As noted above, it may be necessary to use the Large Window
setting for both compression and decompression.
.UNINDENT
.sp
Example:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
import brotli;

sub vcl_init {
      # Create a decompression filter with a small buffer size.
      new unbr_small = brotli.decoder("unbr_small", buffer=1k);
}

sub vcl_backend_response {
      # Use the custom filter when the URL begins with "/tiny"
      if (bereq.url ~ "/tiny" && beresp.http.Content\-Encoding ~ "br") {
              set beresp.filters = "unbr_tiny";
      }
}
.ft P
.fi
.UNINDENT
.UNINDENT
.SS VOID xdecoder.create_stats()
.sp
Create statistics for the custom decompression filter, like the
\fB\&.create_stats()\fP method for \fBencoder\fP\&. See \fI\%STATISTICS\fP below
for details.
.sp
As with \fBencoder.create_stats()\fP, this method MAY NOT be called in
any VCL subroutine besides \fBvcl_init\fP, otherwise VCL failure is invoked.
.SS STRING encoder_version()
.sp
Return the version string for the brotli encoder library.
.sp
Example:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
std.log("Using brotli encoder version: " + brotli.encoder_version());
.ft P
.fi
.UNINDENT
.UNINDENT
.SS STRING decoder_version()
.sp
Return the version string for the brotli decoder library.
.sp
Example:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
std.log("Using brotli decoder version: " + brotli.decoder_version());
.ft P
.fi
.UNINDENT
.UNINDENT
.SS STRING version()
.sp
Return the version string for this VFP.
.sp
Example:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
std.log("Using VFP brotli version: " + brotli.version());
.ft P
.fi
.UNINDENT
.UNINDENT
.SH STATISTICS
.sp
The VFP creates counters in the \fBBROTLI.*\fP namespace, which can be
read and monitored with a tool like \fBvarnishstat(1)\fP, or any other
client of the Varnish statistics API.
.sp
Counters for the built\-in filters \fB"br"\fP and \fB"unbr"\fP are always
created, with names of the form \fBBROTLI.br.*\fP and
\fBBROTLI.unbr.*\fP\&. If you use the \fBencoder\fP and/or \fBdecoder\fP
objects to create filters with custom parameters, you may optionally
use the \fB\&.create_stats()\fP method for either object type to create
counters for the custom filters. If so, then these counters have names
of the form:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
BROTLI.<vcl_name>.<filter_name>.*
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\&... where \fB<vcl_name>\fP is the name of the VCL configuration, and
\fB<filter_name>\fP is the name of the filter as used in
\fBberesp.filters\fP\&.
.sp
The stats are:
.INDENT 0.0
.IP \(bu 2
\fBops\fP: total number of de\-/compression operations. Always incremented
even when the filter does not make any changes, for example when a
compression filter finds that a backend response already has a
\fBContent\-Encoding\fP header.
.IP \(bu 2
\fBin\fP: total number of input bytes processed by the filter
.IP \(bu 2
\fBout\fP: total number of ouput bytes produced by the filter
.UNINDENT
.sp
All three stats are counters, meaning that they are always monotonic
increasing, except when the Varnish worker process restarts and they
are reset to 0.
.sp
So these stats are always created when the VFP is imported:
.INDENT 0.0
.IP \(bu 2
\fBBROTLI.br.ops\fP
.IP \(bu 2
\fBBROTLI.br.in\fP
.IP \(bu 2
\fBBROTLI.br.out\fP
.IP \(bu 2
\fBBROTLI.unbr.ops\fP
.IP \(bu 2
\fBBROTLI.unbr.in\fP
.IP \(bu 2
\fBBROTLI.unbr.out\fP
.UNINDENT
.sp
And stats for custom filters are also created if you so choose.
.sp
The work of the VFP in individual request/response transactions can
also be monitored in the Varnish log, recorded with the \fBVfpAcct\fP
tag (see \fBvsl(7)\fP). By default, \fBVfpAcct\fP is not written to the
log; to see it, set the \fBvarnishd\fP parameter \fBvsl_mask\fP to
\fB+VfpAcct\fP\&.
.SH ERRORS
.sp
If errors are encountered during compression or decompression, they
are reported in the Varnish log with the tag \fBFetchError\fP (see
\fBvsl(7)\fP). The \fBFetchError\fP message for decompression errors
includes the error message from the brotli decoder library.
.sp
If the decoder reaches end\-of\-stream indicators, signaling a completed
decompression, but there are still additional bytes in the response
body, then the filter fails with the \fBFetchError\fP message "Junk
after brotli data".
.sp
The backend fetch fails when any VFP filter fails, which can lead to a
"503 Backend fetch failed" response from Varnish. When streaming is
enabled (i.e when the VCL variable \fBberesp.do_stream\fP variable is
\fBtrue\fP, which is the default), the error might not be detected until
after the response code and header have already been sent to the
client; then it is too late to change the response status. In such
cases, the client response body may be empty, and the network
connection to the client is closed.
.SH REQUIREMENTS
.sp
The VFP currently requires the Varnish master branch since commit
\fB54af42d\fP\&.
.sp
The VFP also requires the \fI\%Google brotli library\fP, see
\fI\%INSTALL.rst\fP in the source directory for details.
.sp
The VFP has been tested with version 1.0.5 of both the encoder and
decoder libraries.
.sp
If you are building the VFP from source, you will need development
versions of the brotli libraries. See \fI\%INSTALL.rst\fP\&.
.SH INSTALLATION
.sp
See \fI\%INSTALL.rst\fP in the source repository.
.SH LIMITATIONS
.sp
Memory and CPU usage for compression and decompression is largely
driven by the choice of parameters for the brotli algorithm. If you
need to limit resource consumption, consider using custom filters, and
adjusting the parameters as needed.
.sp
Memory used by the VFP, such as the temporary internal buffer and
other internal structures, is allocated from the heap, and hence is
limited by available virtual memory.
.sp
As noted above, brotli compression does not work together with Edge
Side Includes, or ESI includes, as gzip support does for standard
Varnish. If you are using ESI, then brotli\-compressed backend
responses will have to be decompressed on fetch. Without any special
measures taken in VCL, client responses with ESI\-included content will
have to be delivered uncompressed.
.SH SEE ALSO
.INDENT 0.0
.IP \(bu 2
varnishd(1)
.IP \(bu 2
vcl(7)
.IP \(bu 2
varnishstat(1)
.IP \(bu 2
vsl(7)
.IP \(bu 2
VMOD source repository: \fI\%https://code.uplex.de/uplex\-varnish/libvfp\-brotli\fP
.IP \(bu 2
brotli library repository: \fI\%https://github.com/google/brotli\fP
.IP \(bu 2
RFC 7932 (brotli specification): \fI\%https://tools.ietf.org/html/rfc7932\fP
.IP \(bu 2
RFC 7232 (HTTP conditional requests, with the specification for weak
validation): \fI\%https://tools.ietf.org/html/rfc7232\fP
.UNINDENT
.SH COPYRIGHT
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
All rights reserved

Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>

See LICENSE
.ft P
.fi
.UNINDENT
.UNINDENT
.\" Generated by docutils manpage writer.
.
