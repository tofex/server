#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName}

Example: ${scriptName}
EOF
}

trim()
{
  echo -n "$1" | xargs
}

while getopts h:? option; do
  case ${option} in
    h) usage; exit 1;;
    ?) usage; exit 1;;
  esac
done

install-package apt-transport-https
install-package ca-certificates
install-package debian-archive-keyring
install-package curl
install-package gnupg
install-package libssl1.1
install-package libssl-dev
add-repository "varnish-cache-6.3.list" "https://packagecloud.io/varnishcache/varnish63/ubuntu/" "bionic" "main" "https://packagecloud.io/varnishcache/varnish63/gpgkey" "y"
install-package varnish-dev 6.3.2-1~bionic
install-package python-docutils
update-ca-certificates

mkdir -p /tmp/vmod
cd /tmp/vmod
git clone --branch master --recursive https://code.uplex.de/uplex-varnish/libvmod-objesi.git
cd libvmod-objesi
./bootstrap
make 
make install

cd /
rm -rf /tmp/vmod

service varnish restart
