#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -s  Allow sudo only with password
  -d  Disallow sudo
  -v  Verbose

Example: ${scriptName} -s
EOF
}

trim()
{
  echo -n "$1" | xargs
}

sudoWithPassword=0
disallowSudo=0
verbose=0

while getopts hsdv? option; do
  case ${option} in
    h) usage; exit 1;;
    s) sudoWithPassword=1;;
    d) disallowSudo=1;;
    v) verbose=1;;
    ?) usage; exit 1;;
  esac
done

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
distribution=$(lsb_release -i | awk '{print $3}')

if [[ ! -d /var/www ]]; then
  mkdir /var/www
  chown www-data:www-data /var/www
fi

if [[ "${sudoWithPassword}" == 1 ]]; then
  if [[ "${verbose}" == 1 ]]; then
    "${currentPath}/../Server/${distribution}/prepare-user.sh" -u "www-data" -s -v
  else
    "${currentPath}/../Server/${distribution}/prepare-user.sh" -u "www-data" -s
  fi
elif [[ "${disallowSudo}" == 1 ]]; then
  if [[ "${verbose}" == 1 ]]; then
    "${currentPath}/../Server/${distribution}/prepare-user.sh" -u "www-data" -d -v
  else
    "${currentPath}/../Server/${distribution}/prepare-user.sh" -u "www-data" -d
  fi
else
  if [[ "${verbose}" == 1 ]]; then
    "${currentPath}/../Server/${distribution}/prepare-user.sh" -u "www-data" -v
  else
    "${currentPath}/../Server/${distribution}/prepare-user.sh" -u "www-data"
  fi
fi

chown -hR www-data:www-data /var/www
