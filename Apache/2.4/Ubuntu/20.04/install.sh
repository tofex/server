#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF

usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  HTTP port, default: 80
  -s  SSL Port, default: 443
  -c  SSL certificate file, default: /etc/ssl/certs/ssl-cert-snakeoil.pem
  -k  SSL key file, default: /etc/ssl/private/ssl-cert-snakeoil.key

Example: ${scriptName} -p 80 -s 443
EOF
}

trim()
{
  echo -n "$1" | xargs
}

httpPort="80"
sslPort="443"
sslCertFile="/etc/ssl/certs/ssl-cert-snakeoil.pem"
sslKeyFile="/etc/ssl/private/ssl-cert-snakeoil.key"

while getopts hp:s:c:k:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) httpPort=$(trim "$OPTARG");;
    s) sslPort=$(trim "$OPTARG");;
    c) sslCertFile=$(trim "$OPTARG");;
    k) sslKeyFile=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${httpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${sslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${sslCertFile}" ]]; then
  echo "No SSL certificate file specified!"
  exit 1
fi

if [[ ! -f "${sslCertFile}" ]]; then
  echo "Invalid SSL certificate file specified!"
  exit 1
fi

if [[ -z "${sslKeyFile}" ]]; then
  echo "No SSL key file specified!"
  exit 1
fi

if [[ ! -f "${sslKeyFile}" ]]; then
  echo "Invalid SSL key file specified!"
  exit 1
fi

add-certificate "default" "${sslCertFile}" "${sslKeyFile}"

install-package apache2-bin 2.4
install-package apache2-data 2.4
install-package apache2 2.4
install-package ssl-cert

a2enmod rewrite ssl headers expires

service apache2 stop

rm -rf /var/log/apache2/*

echo "ServerName localhost" > /etc/apache2/conf-available/server.conf
echo "EnableSendfile off" >> /etc/apache2/conf-available/server.conf
a2enconf server.conf

replace-file-content /etc/apache2/apache2.conf "LogLevel info" "LogLevel warn"

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p /opt/install/
crudini --set /opt/install/env.properties apache version "2.4"

"${currentPath}/ports.sh" -p "80" -n "${httpPort}" -o "443" -s "${sslPort}"

rm -f /etc/apache2/sites-available/default-ssl.conf

echo "<?php phpinfo();" > /var/www/html/index.php

if [[ -f /.dockerenv ]]; then
  echo "Creating start script at: /usr/local/bin/apache.sh"
  cat <<EOF > /usr/local/bin/apache.sh
#!/usr/bin/env bash
trap stop SIGTERM SIGINT SIGQUIT SIGHUP ERR
stop() {
  echo "Stopping Apache2"
  /usr/sbin/apachectl -k graceful-stop
  exit
}
for command in "\$@"; do
  echo "Run: \${command}"
  /bin/bash "\${command}"
done
echo "Starting Apache2"
/usr/sbin/apache2ctl start
tail -f /var/log/apache2/error.log & wait \$!
EOF
  chmod +x /usr/local/bin/apache.sh
else
  echo "Starting Apache"
  service apache2 start
fi

"${currentPath}/default.sh" -p "${httpPort}" -s "${sslPort}" -c "${sslCertFile}" -k "${sslKeyFile}"
