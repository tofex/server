#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help                   Show this message
  --apacheHttpPort         HTTP port, default: 80
  --apacheSslPort          SSL port, default: 443
  --webPath                Web path, required if no proxy data
  --webUser                Web user, default: www-data
  --webGroup               Web group, default: www-data
  --proxyHostPath          Proxy host path (optional), default: /
  --proxyProtocol          Proxy protocol (optional), default: http
  --proxyHost              Proxy host (optional)
  --proxyPort              Proxy port (optional), default: 80 (http) or 443 (https)
  --proxyPath              Proxy path (optional), default: /
  --logPath                Log path, default: /var/log/apache2
  --logLevel               Log level, default: warn
  --serverName             Server name
  --serverAdmin            Server admin, default: webmaster@<server name>
  --sslTerminated          SSL terminated (yes/no), default: no
  --forceSsl               Force SSL (yes/no), default: yes
  --basicAuthUserName      Basic auth user name (optional)
  --basicAuthPassword      Basic auth password (optional)
  --basicAuthUserFilePath  Basic auth user file path (optional), default: /var/www
  --overwrite              Overwrite existing files (yes/no), default: no

Example: ${scriptName} --webPath /var/www/project01/htdocs --serverName project01.net --sslTerminated no --forceSsl yes
EOF
}

apacheHttpPort=
apacheSslPort=
webPath=
webUser=
webGroup=
proxyHostPath=
proxyProtocol=
proxyHost=
proxyPort=
proxyPath=
logPath=
logLevel=
serverName=
serverAdmin=
sslTerminated=
forceSsl=
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath=
overwrite=

source "${currentPath}/../../../../prepare-parameters.sh"

if [[ -z "${apacheHttpPort}" ]]; then
  apacheHttpPort=80
fi

if [[ -z "${apacheSslPort}" ]]; then
  apacheSslPort=443
fi

if [[ -z "${webPath}" ]] || [[ "${webPath}" == "-" ]]; then
  if [[ -z "${proxyHost}" ]] || [[ "${proxyHost}" == "-" ]]; then
    echo "No web path or proxy specified!"
    exit 1
  else
    webPath="-"
  fi
elif [[ "${webPath}" != */ ]]; then
  webPath="${webPath}/"
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ -z "${proxyHostPath}" ]]; then
  proxyHostPath="/"
fi

if [[ -z "${proxyProtocol}" ]]; then
  proxyProtocol="http"
fi

if [[ -z "${proxyHost}" ]]; then
  proxyHost="-"
fi

if [[ -z "${proxyPort}" ]]; then
  if [[ "${proxyProtocol}" == "http" ]]; then
    proxyPort=80
  elif [[ "${proxyProtocol}" == "https" ]]; then
    proxyPort=443
  else
    proxyPort="-"
  fi
fi

if [[ -z "${proxyPath}" ]] || [[ "${proxyPath}" == "-" ]]; then
  proxyPath="/"
fi

if [[ -z "${logPath}" ]]; then
  logPath="/var/log/apache2"
fi

if [[ -z "${logLevel}" ]]; then
  logLevel="warn"
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${serverAdmin}" ]]; then
  serverAdmin="webmaster@${serverName}"
fi

if [[ -z "${sslTerminated}" ]]; then
  sslTerminated="no"
fi

if [[ -z "${forceSsl}" ]]; then
  forceSsl="yes"
fi

if [[ -z "${basicAuthUserName}" ]]; then
  basicAuthUserName="-"
fi

if [[ -z "${basicAuthPassword}" ]]; then
  basicAuthPassword="-"
fi

if [[ -z "${basicAuthUserFilePath}" ]]; then
  basicAuthUserFilePath="/var/www"
fi

if [[ -z "${overwrite}" ]]; then
  overwrite="no"
fi

"${currentPath}/vhost/prepare/configuration-file.sh" \
  --serverName "${serverName}" \
  --overwrite "${overwrite}"

if [[ ${forceSsl} == "yes" ]]; then
  if [[ ${sslTerminated} == "yes" ]]; then
    if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
      "${currentPath}/vhost/force-ssl-terminated-basic-auth.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --apacheSslPort "${apacheSslPort}" \
        --webPath "${webPath}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --basicAuthUserName "${basicAuthUserName}" \
        --basicAuthPassword "${basicAuthPassword}" \
        --basicAuthUserFilePath "${basicAuthUserFilePath}" \
        --append yes
    else
      "${currentPath}/vhost/force-ssl-terminated.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --apacheSslPort "${apacheSslPort}" \
        --webPath "${webPath}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --append yes
    fi
  else
    if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
      "${currentPath}/vhost/force-ssl-basic-auth.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --apacheSslPort "${apacheSslPort}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --basicAuthUserName "${basicAuthUserName}" \
        --basicAuthPassword "${basicAuthPassword}" \
        --basicAuthUserFilePath "${basicAuthUserFilePath}" \
        --append yes
    else
      "${currentPath}/vhost/force-ssl.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --apacheSslPort "${apacheSslPort}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --append yes
    fi
  fi
else
  if [[ -n "${proxyHost}" ]] && [[ "${proxyHost}" != "-" ]]; then
    if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
      "${currentPath}/vhost/http-proxy-basic-auth.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --proxyHostPath "${proxyHostPath}" \
        --proxyProtocol "${proxyProtocol}" \
        --proxyHost "${proxyHost}" \
        --proxyPort "${proxyPort}" \
        --proxyPath "${proxyPath}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --basicAuthUserName "${basicAuthUserName}" \
        --basicAuthPassword "${basicAuthPassword}" \
        --basicAuthUserFilePath "${basicAuthUserFilePath}" \
        --append yes
    else
      "${currentPath}/vhost/http-proxy.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --proxyHostPath "${proxyHostPath}" \
        --proxyProtocol "${proxyProtocol}" \
        --proxyHost "${proxyHost}" \
        --proxyPort "${proxyPort}" \
        --proxyPath "${proxyPath}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --append yes
    fi
  else
    if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
      "${currentPath}/vhost/http-path-basic-auth.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --webPath "${webPath}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --basicAuthUserName "${basicAuthUserName}" \
        --basicAuthPassword "${basicAuthPassword}" \
        --basicAuthUserFilePath "${basicAuthUserFilePath}" \
        --append yes
    else
      "${currentPath}/vhost/http-path.sh" \
        --apacheHttpPort "${apacheHttpPort}" \
        --webPath "${webPath}" \
        --webUser "${webUser}" \
        --webGroup "${webGroup}" \
        --logPath "${logPath}" \
        --logLevel "${logLevel}" \
        --serverName "${serverName}" \
        --serverAdmin "${serverAdmin}" \
        --append yes
    fi
  fi
fi

if [[ -n "${proxyHost}" ]] && [[ "${proxyHost}" != "-" ]]; then
  if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
    "${currentPath}/vhost/ssl-proxy-basic-auth.sh" \
      --apacheSslPort "${apacheSslPort}" \
      --webUser "${webUser}" \
      --webGroup "${webGroup}" \
      --proxyHostPath "${proxyHostPath}" \
      --proxyProtocol "${proxyProtocol}" \
      --proxyHost "${proxyHost}" \
      --proxyPort "${proxyPort}" \
      --proxyPath "${proxyPath}" \
      --logPath "${logPath}" \
      --logLevel "${logLevel}" \
      --serverName "${serverName}" \
      --serverAdmin "${serverAdmin}" \
      --basicAuthUserName "${basicAuthUserName}" \
      --basicAuthPassword "${basicAuthPassword}" \
      --basicAuthUserFilePath "${basicAuthUserFilePath}" \
      --append yes
  else
    "${currentPath}/vhost/ssl-proxy.sh" \
      --apacheSslPort "${apacheSslPort}" \
      --webUser "${webUser}" \
      --webGroup "${webGroup}" \
      --proxyHostPath "${proxyHostPath}" \
      --proxyProtocol "${proxyProtocol}" \
      --proxyHost "${proxyHost}" \
      --proxyPort "${proxyPort}" \
      --proxyPath "${proxyPath}" \
      --logPath "${logPath}" \
      --logLevel "${logLevel}" \
      --serverName "${serverName}" \
      --serverAdmin "${serverAdmin}" \
      --append yes
  fi
else
  if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
    "${currentPath}/vhost/ssl-path-basic-auth.sh" \
      --apacheSslPort "${apacheSslPort}" \
      --webPath "${webPath}" \
      --webUser "${webUser}" \
      --webGroup "${webGroup}" \
      --logPath "${logPath}" \
      --logLevel "${logLevel}" \
      --serverName "${serverName}" \
      --serverAdmin "${serverAdmin}" \
      --basicAuthUserName "${basicAuthUserName}" \
      --basicAuthPassword "${basicAuthPassword}" \
      --basicAuthUserFilePath "${basicAuthUserFilePath}" \
      --append yes
  else
    "${currentPath}/vhost/ssl-path.sh" \
      --apacheSslPort "${apacheSslPort}" \
      --webPath "${webPath}" \
      --webUser "${webUser}" \
      --webGroup "${webGroup}" \
      --logPath "${logPath}" \
      --logLevel "${logLevel}" \
      --serverName "${serverName}" \
      --serverAdmin "${serverAdmin}" \
      --append yes
  fi
fi

if [[ ! -f "/etc/apache2/sites-enabled/${serverName}.conf" ]]; then
  echo "Enabling configuration at: /etc/apache2/sites-enabled/${serverName}.conf"
  sudo a2ensite "${serverName}.conf"
fi

if [[ -f /.dockerenv ]]; then
  echo "Reloading Apache"
  sudo service apache2 reload
else
  echo "Restarting Apache"
  sudo service apache2 restart
fi
