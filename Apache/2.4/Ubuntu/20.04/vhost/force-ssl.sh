#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help                   Show this message
  --apacheHttpPort         HTTP port, default: 80
  --apacheSslPort          SSL port, default: 443
  --webUser                Web user, default: www-data
  --webGroup               Web group, default: www-data
  --logPath                Log path, default: /var/log/apache2
  --logLevel               Log level, default: warn
  --serverName             Server name
  --serverAdmin            Server admin, default: webmaster@<server name>
  --append                 Append to existing configuration if configuration file already exists (yes/no), default: no

Example: ${scriptName} --serverName project01.net
EOF
}

apacheHttpPort=
apacheSslPort=
webUser=
webGroup=
logPath=
logLevel=
serverName=
serverAdmin=
append=

source "${currentPath}/../../../../../prepare-parameters.sh"

if [[ -z "${apacheHttpPort}" ]]; then
  apacheHttpPort=80
fi

if [[ -z "${apacheSslPort}" ]]; then
  apacheSslPort=443
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ -z "${logPath}" ]]; then
  logPath="/var/log/apache2"
fi

if [[ -z "${logLevel}" ]]; then
  logLevel="warn"
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${serverAdmin}" ]]; then
  serverAdmin="webmaster@${serverName}"
fi

if [[ -z "${append}" ]]; then
  append="no"
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'rewrite_module' | wc -l) == 0 ]]; then
  echo "Enabling rewrite module"
  a2enmod rewrite
fi

"${currentPath}/prepare/log-path.sh" \
  --logPath "${logPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}"

"${currentPath}/prepare/configuration-file.sh" \
  --serverName "${serverName}" \
  --append "${append}"

configurationFile="/etc/apache2/sites-available/${serverName}.conf"

echo "Creating HTTP to SSL redirect at: ${configurationFile}"

cat <<EOF | sudo tee -a "${configurationFile}" > /dev/null
<VirtualHost *:${apacheHttpPort}>
  ServerName ${serverName}
  ServerAdmin ${serverAdmin}
  Redirect / https://${serverName}:${apacheSslPort}/
  LogLevel ${logLevel}
  ErrorLog ${logPath}/${serverName}-apache-http-error.log
  CustomLog ${logPath}/${serverName}-apache-http-access.log combined
</VirtualHost>
EOF
