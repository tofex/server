#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help     Show this message
  --webPath  Web path
  --webUser  Web user, default: www-data
  --webUser  Web group, default: www-data

Example: ${scriptName} --webPath /var/www/project/htdocs
EOF
}

webPath=
webUser=
webGroup=

source "${currentPath}/../../../../../../prepare-parameters.sh"

if [[ -z "${webPath}" ]]; then
  echo "No web path specified!"
  exit 1
elif [[ "${webPath}" != */ ]]; then
  webPath="${webPath}/"
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ ! -d "${webPath}" ]]; then
  echo "Creating web path at: ${webPath}"
  sudo mkdir -p "${webPath}"
fi

webPathUser=$(stat -c '%U' "${webPath}")
webPathGroup=$(stat -c '%G' "${webPath}")

if [[ "${webUser}" != "${webPathUser}" ]] || [[ "${webGroup}" != "${webPathGroup}" ]]; then
  echo "Setting owner of web path: ${webPath} to: ${webUser}:${webGroup}"
  sudo chown "${webUser}":"${webGroup}" "${webPath}"
fi
