#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help      Show this message
  --logPath   Log path, default: /var/log/apache2
  --webUser   Web user, default: www-data
  --webGroup  Web group, default: www-data

Example: ${scriptName} -l /var/www/project/log
EOF
}

logPath=
webUser=
webGroup=

source "${currentPath}/../../../../../../prepare-parameters.sh"

if [[ -z "${logPath}" ]]; then
  echo "No log path specified!"
  exit 1
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ ! -d "${logPath}" ]]; then
  echo "Creating log path at: ${logPath}"
  sudo mkdir -p "${logPath}"
fi

logPathUser=$(stat -c '%U' "${logPath}")
logPathGroup=$(stat -c '%G' "${logPath}")

if [[ "${webUser}" != "${logPathUser}" ]] || [[ "${webGroup}" != "${logPathGroup}" ]]; then
  echo "Setting owner of log path: ${logPath} to: ${webUser}:${webGroup}"
  sudo chown "${webUser}":"${webGroup}" "${logPath}"
fi
