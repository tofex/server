#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help        Show this message
  --serverName  Server name
  --overwrite   Overwrite existing files (yes/no), default: no
  --append      Append to existing configuration if configuration file already exists (yes/no), default: no

Example: ${scriptName} --serverName project01.net
EOF
}

serverName=
overwrite=
append=

source "${currentPath}/../../../../../../prepare-parameters.sh"

if [[ -z "${overwrite}" ]]; then
  overwrite="no"
fi

if [[ -z "${append}" ]]; then
  append="no"
fi

configurationFile="/etc/apache2/sites-available/${serverName}.conf"

if [[ -f "${configurationFile}" ]]; then
  if [[ "${overwrite}" == "no" ]] && [[ "${append}" == "no" ]]; then
    echo "Configuration \"${configurationFile}\" already exists"
    exit 1
  fi
  if [[ "${overwrite}" == "yes" ]]; then
    echo "Removing previous configuration file at: ${configurationFile}"
    rm -rf "${configurationFile}"
    echo "Creating configuration at: ${configurationFile}"
    touch "${configurationFile}"
  fi
else
  echo "Creating configuration at: ${configurationFile}"
  touch "${configurationFile}"
fi
