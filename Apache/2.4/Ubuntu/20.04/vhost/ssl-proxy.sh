#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help            Show this message
  --apacheSslPort   SSL port, default: 443
  --webUser         Web user, default: www-data
  --webGroup        Web group, default: www-data
  --proxyHostPath   Proxy host path, default: /
  --proxyProtocol   Proxy protocol, default: http
  --proxyHost       Proxy host
  --proxyPort       Proxy port, default: 80 (http) or 443 (https)
  --proxyPath       Proxy path, default: /
  --logPath         Log path, default: /var/log/apache2
  --logLevel        Log level, default: warn
  --serverName      Server name
  --serverAdmin     Server admin, default: webmaster@<server name>
  --append          Append to existing configuration if configuration file already exists (yes/no), default: no

Example: ${scriptName} --serverName project01.net
EOF
}

apacheSslPort=
webUser=
webGroup=
proxyHostPath=
proxyProtocol=
proxyHost=
proxyPort=
proxyPath=
logPath=
logLevel=
serverName=
serverAdmin=
append=

source "${currentPath}/../../../../../prepare-parameters.sh"

if [[ -z "${apacheSslPort}" ]]; then
  apacheSslPort=443
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ -z "${proxyHostPath}" ]]; then
  proxyHostPath="/"
fi

if [[ -z "${proxyProtocol}" ]]; then
  proxyProtocol="http"
fi

if [[ -z "${proxyHost}" ]]; then
  echo "No proxy host specified!"
  exit 1
fi

if [[ -z "${proxyPort}" ]]; then
  if [[ "${proxyProtocol}" == "http" ]]; then
    proxyPort=80
  elif [[ "${proxyProtocol}" == "https" ]]; then
    proxyPort=443
  else
    echo "No proxy port specified!"
    exit 1
  fi
fi

if [[ -z "${proxyPath}" ]] || [[ "${proxyPath}" == "-" ]]; then
  proxyPath="/"
fi

if [[ -z "${logPath}" ]]; then
  logPath="/var/log/apache2"
fi

if [[ -z "${logLevel}" ]]; then
  logLevel="warn"
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${serverAdmin}" ]]; then
  serverAdmin="webmaster@${serverName}"
fi

if [[ -z "${append}" ]]; then
  append="no"
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'ssl_module' | wc -l) == 0 ]]; then
  echo "Enabling SSL module"
  a2enmod ssl
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'headers_module' | wc -l) == 0 ]]; then
  echo "Enabling headers module"
  a2enmod headers
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'expires_module' | wc -l) == 0 ]]; then
  echo "Enabling expires module"
  a2enmod expires
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'proxy_module' | wc -l) == 0 ]]; then
  echo "Enabling proxy module"
  a2enmod proxy
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'proxy_http_module' | wc -l) == 0 ]]; then
  echo "Enabling proxy HTTP module"
  a2enmod proxy_http
fi

"${currentPath}/prepare/log-path.sh" \
  --logPath "${logPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}"

"${currentPath}/prepare/configuration-file.sh" \
  --serverName "${serverName}" \
  --append "${append}"

configurationFile="/etc/apache2/sites-available/${serverName}.conf"

echo "Creating SSL proxy at: ${configurationFile} to: ${proxyProtocol}://${proxyHost}:${proxyPort}${proxyPath}"

cat <<EOF | sudo tee -a "${configurationFile}" > /dev/null
<IfModule mod_ssl.c>
  <VirtualHost *:${apacheSslPort}>
    SSLEngine on
    ServerName ${serverName}
    ServerAdmin ${serverAdmin}
    ProxyPass "${proxyHostPath}" "${proxyProtocol}://${proxyHost}:${proxyPort}${proxyPath}"
    ProxyPassReverse "${proxyHostPath}" "${proxyProtocol}://${proxyHost}:${proxyPort}${proxyPath}"
    ProxyPreserveHost On
    ProxyRequests Off
    RequestHeader unset Authorization
    LogLevel ${logLevel}
    ErrorLog ${logPath}/${serverName}-apache-ssl-error.log
    CustomLog ${logPath}/${serverName}-apache-ssl-access.log combined
  </VirtualHost>
</IfModule>
EOF
