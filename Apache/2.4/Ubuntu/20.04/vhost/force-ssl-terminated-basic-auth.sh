#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help                   Show this message
  --apacheHttpPort         HTTP port, default: 80
  --apacheSslPort          SSL port, default: 443
  --webPath                Web path
  --webUser                Web user, default: www-data
  --webGroup               Web group, default: www-data
  --logPath                Log path, default: /var/log/apache2
  --logLevel               Log level, default: warn
  --serverName             Server name
  --serverAdmin            Server admin, default: webmaster@<server name>
  --basicAuthUserName      Basic auth user name
  --basicAuthPassword      Basic auth password
  --basicAuthUserFilePath  Basic auth user file path, default: /var/www
  --append                 Append to existing configuration if configuration file already exists (yes/no), default: no

Example: ${scriptName} --webPath /var/www/project01/htdocs --serverName project01.net --basicAuthUserName login --basicAuthPassword password
EOF
}

apacheHttpPort=
apacheSslPort=
webPath=
webUser=
webGroup=
logPath=
logLevel=
serverName=
serverAdmin=
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath=
append=

source "${currentPath}/../../../../../prepare-parameters.sh"

if [[ -z "${apacheHttpPort}" ]]; then
  apacheHttpPort=80
fi

if [[ -z "${apacheSslPort}" ]]; then
  apacheSslPort=443
fi

if [[ -z "${webPath}" ]]; then
  echo "No web path specified!"
  exit 1
elif [[ "${webPath}" != */ ]]; then
  webPath="${webPath}/"
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ -z "${logPath}" ]]; then
  logPath="/var/log/apache2"
fi

if [[ -z "${logLevel}" ]]; then
  logLevel="warn"
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${serverAdmin}" ]]; then
  serverAdmin="webmaster@${serverName}"
fi

if [[ -z "${basicAuthUserName}" ]]; then
  echo "No basic auth user name specified!"
fi

if [[ -z "${basicAuthPassword}" ]]; then
  echo "No basic auth password specified!"
fi

if [[ -z "${basicAuthUserFilePath}" ]]; then
  basicAuthUserFilePath="/var/www"
fi

if [[ -z "${append}" ]]; then
  append="no"
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'rewrite_module' | wc -l) == 0 ]]; then
  echo "Enabling rewrite module"
  a2enmod rewrite
fi

if [[ $(apache2ctl -M | tail -n +2 | awk '{print $1}' | grep 'auth_basic_module' | wc -l) == 0 ]]; then
  echo "Enabling basic auth module"
  a2enmod auth_basic
fi

"${currentPath}/prepare/web-path.sh" \
  --webPath "${webPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}"

"${currentPath}/prepare/log-path.sh" \
  --logPath "${logPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}"

"${currentPath}/prepare/basic-auth-file.sh" \
  --serverName "${serverName}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}" \
  --basicAuthUserName "${basicAuthUserName}" \
  --basicAuthPassword "${basicAuthPassword}" \
  --basicAuthUserFilePath "${basicAuthUserFilePath}"

"${currentPath}/prepare/configuration-file.sh" \
  --serverName "${serverName}" \
  --append "${append}"

configurationFile="/etc/apache2/sites-available/${serverName}.conf"
basicAuthUserFile="${basicAuthUserFilePath}/${serverName}.htpasswd"

echo "Creating HTTP to SSL redirect with terminated SSL at: ${configurationFile} with basic auth file: ${basicAuthUserFile}"

cat <<EOF | sudo tee -a "${configurationFile}" > /dev/null
<VirtualHost *:${apacheHttpPort}>
  ServerName ${serverName}
  ServerAdmin ${serverAdmin}
  DocumentRoot ${webPath}
  <Directory ${webPath}>
    AuthType Basic
    AuthName "${serverName}"
    AuthUserFile "${basicAuthUserFile}"
    Require valid-user
    Options FollowSymLinks
    AllowOverride All
    Order Allow,Deny
    Allow from all
  </Directory>
  RewriteEngine On
  RewriteCond %{HTTP:X-Forwarded-Proto} =http
  RewriteRule .* https://%{HTTP:Host}:${apacheSslPort}%{REQUEST_URI} [L,R=permanent]
  SetEnv HTTPS on
  LogLevel ${logLevel}
  ErrorLog ${logPath}/${serverName}-apache-http-error.log
  CustomLog ${logPath}/${serverName}-apache-http-access.log combined
</VirtualHost>
EOF
