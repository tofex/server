#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help           Show this message
  --apacheSslPort  SSL port, default: 443
  --webPath        Web path
  --webUser        Web user, default: www-data
  --webGroup       Web group, default: www-data
  --logPath        Log path, default: /var/log/apache2
  --logLevel       Log level, default: warn
  --serverName     Server name
  --serverAdmin    Server admin, default: webmaster@<server name>
  --append         Append to existing configuration if configuration file already exists (yes/no), default: no

Example: ${scriptName} --webPath /var/www/project01/htdocs --serverName project01.net
EOF
}

apacheSslPort=
webPath=
webUser=
webGroup=
logPath=
logLevel=
serverName=
serverAdmin=
append=

source "${currentPath}/../../../../../prepare-parameters.sh"

if [[ -z "${apacheSslPort}" ]]; then
  apacheSslPort=443
fi

if [[ -z "${webPath}" ]]; then
  echo "No web path specified!"
  exit 1
elif [[ "${webPath}" != */ ]]; then
  webPath="${webPath}/"
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ -z "${logPath}" ]]; then
  logPath="/var/log/apache2"
fi

if [[ -z "${logLevel}" ]]; then
  logLevel="warn"
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${serverAdmin}" ]]; then
  serverAdmin="webmaster@${serverName}"
fi

if [[ -z "${append}" ]]; then
  append="no"
fi

"${currentPath}/prepare/web-path.sh" \
  --webPath "${webPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}"

"${currentPath}/prepare/log-path.sh" \
  --logPath "${logPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}"

"${currentPath}/prepare/configuration-file.sh" \
  --serverName "${serverName}" \
  --append "${append}"

configurationFile="/etc/apache2/sites-available/${serverName}.conf"

echo "Creating SSL host at: ${configurationFile}"

cat <<EOF | sudo tee -a "${configurationFile}" > /dev/null
<IfModule mod_ssl.c>
  <VirtualHost *:${apacheSslPort}>
    SSLEngine on
    ServerName ${serverName}
    ServerAdmin ${serverAdmin}
    DocumentRoot ${webPath}
    <Directory ${webPath}>
      Options FollowSymLinks
      AllowOverride All
      Order Allow,Deny
      Allow from all
    </Directory>
    LogLevel ${logLevel}
    ErrorLog ${logPath}/${serverName}-apache-ssl-error.log
    CustomLog ${logPath}/${serverName}-apache-ssl-access.log combined
  </VirtualHost>
</IfModule>
EOF
