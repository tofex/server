#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  HTTP port, default: 80
  -s  SSL port, default: 443
  -e  Web user, default: www-data
  -g  Web group, default: www-data
  -c  Target system protocol
  -x  Target system host name
  -y  Target system port
  -l  Log path, default: /var/log/apache2
  -v  Server name
  -t  SSL terminated (yes/no), default: no
  -f  Force SSL (yes/no), default: yes
  -u  Basic auth user name (optional)
  -p  Basic auth password (optional)
  -i  Basic auth user file path (optional), default: /var/www
  -o  Overwrite existing files (yes/no), default: no

Example: ${scriptName} -w /var/www/project01/htdocs -v project01.net -t yes -f no
EOF
}

trim()
{
  echo -n "$1" | xargs
}

apacheHttpPort=80
apacheSslPort=443
webUser="www-data"
webGroup="www-data"
proxyProtocol="http"
proxyHost=
proxyPort=
logPath="/var/log/apache2"
serverName=
sslTerminated="no"
forceSsl="yes"
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath="/var/www"
overwrite="no"

while getopts hr:s:e:g:c:x:y::l:v:t:f:u:p:i:o:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) apacheHttpPort=$(trim "$OPTARG");;
    s) apacheSslPort=$(trim "$OPTARG");;
    e) webUser=$(trim "$OPTARG");;
    g) webGroup=$(trim "$OPTARG");;
    c) proxyProtocol=$(trim "$OPTARG");;
    x) proxyHost=$(trim "$OPTARG");;
    y) proxyPort=$(trim "$OPTARG");;
    l) logPath=$(trim "$OPTARG");;
    v) serverName=$(trim "$OPTARG");;
    t) sslTerminated=$(trim "$OPTARG");;
    f) forceSsl=$(trim "$OPTARG");;
    u) basicAuthUserName=$(trim "$OPTARG");;
    p) basicAuthPassword=$(trim "$OPTARG");;
    i) basicAuthUserFilePath=$(trim "$OPTARG");;
    o) overwrite=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${apacheHttpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${apacheSslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${proxyProtocol}" ]]; then
  echo "No proxy protocol specified!"
  exit 1
fi

if [[ -z "${proxyHost}" ]]; then
  echo "No proxy host specified!"
  exit 1
fi

if [[ -z "${proxyPort}" ]]; then
  echo "No proxy port specified!"
  exit 1
fi

if [[ -z "${logPath}" ]]; then
  echo "No log path port specified!"
  exit 1
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${sslTerminated}" ]]; then
  echo "No SSL terminated specified!"
  exit 1
fi

if [[ -z "${forceSsl}" ]]; then
  echo "No force SSL specified!"
  exit 1
fi

echo "Enabling reverse proxy module"
a2enmod proxy
a2enmod proxy_http

echo "Restarting Apache"
service apache2 restart

if [[ ! -d "${logPath}" ]]; then
  sudo mkdir -p "${logPath}"
  sudo chown "${webUser}":"${webGroup}" "${logPath}"
fi

if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]] && [[ ! -d "${basicAuthUserFilePath}" ]]; then
  sudo mkdir -p "${basicAuthUserFilePath}"
  sudo chown "${webUser}":"${webGroup}" "${basicAuthUserFilePath}"
fi

if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
  echo "Adding basic user in file at: ${basicAuthUserFilePath}/${serverName}.htpasswd"
  if [[ -f "${basicAuthUserFilePath}/${serverName}.htpasswd" ]]; then
    $(htpasswd -vb "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}" >/dev/null 2>&1)
    result=$?
    if [[ "${result}" -ne 0 ]]; then
      htpasswd -b "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}"
    else
      echo "User already added"
    fi
  else
    htpasswd -b -c "${basicAuthUserFilePath}/${serverName}.htpasswd" "${basicAuthUserName}" "${basicAuthPassword}"
  fi
  sudo chown "${webUser}":"${webGroup}" "${basicAuthUserFilePath}/${serverName}.htpasswd"
fi

if [[ "${overwrite}" == "no" ]]; then
  if [[ -f "/etc/apache2/sites-available/${serverName}.conf" ]]; then
    echo "Configuration \"/etc/apache2/sites-available/${serverName}.conf\" already exists"
    exit 1
  fi
fi

echo "Creating configuration at: /etc/apache2/sites-available/${serverName}.conf"

if [[ ${forceSsl} == "yes" ]] && [[ ${sslTerminated} == "no" ]]; then
  cat <<EOF | sudo tee "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
<VirtualHost *:${apacheHttpPort}>
  ServerName ${serverName}
  ServerAdmin webmaster@localhost
  Redirect / https://${serverName}/
  LogLevel warn
  ErrorLog ${logPath}/${serverName}-apache-http-error.log
  CustomLog ${logPath}/${serverName}-apache-http-access.log combined
</VirtualHost>
EOF
else
  cat <<EOF | sudo tee "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
<VirtualHost *:${apacheHttpPort}>
  ServerName ${serverName}
  ServerAdmin webmaster@localhost
  ProxyPass "/" "${proxyProtocol}://${proxyHost}:${proxyPort}/"
  ProxyPassReverse "/" "${proxyProtocol}://${proxyHost}:${proxyPort}/"
  RequestHeader unset Authorization
  ProxyRequests Off
  <Proxy *>
EOF

  if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
    cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
    AuthType Basic
    AuthName "${serverName}"
    AuthBasicProvider file
    AuthUserFile "${basicAuthUserFilePath}/${serverName}.htpasswd"
    Require valid-user
    RewriteEngine On
    RewriteRule .* - [E=PROXY_USER:%{LA-U:REMOTE_USER},NS]
    RequestHeader set X-WEBAUTH-USER "%{PROXY_USER}e"
EOF
  fi

  cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
  </Proxy>
EOF

  if [[ ${sslTerminated} == "yes" ]]; then
    cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
  RewriteEngine On
  RewriteCond %{HTTP:X-Forwarded-Proto} =http
  RewriteRule .* https://%{HTTP:Host}%{REQUEST_URI} [L,R=permanent]
EOF
  fi

  cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
  LogLevel warn
  ErrorLog ${logPath}/${serverName}-apache-http-error.log
  CustomLog ${logPath}/${serverName}-apache-http-access.log combined
</VirtualHost>
EOF
fi

if [[ ${sslTerminated} == "no" ]]; then
  cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
<IfModule mod_ssl.c>
  <VirtualHost *:${apacheSslPort}>
    SSLEngine on
    ServerName ${serverName}
    ServerAdmin webmaster@localhost
    RequestHeader unset Authorization
    ProxyRequests Off
    ProxyPass "/" "${proxyProtocol}://${proxyHost}:${proxyPort}/"
    ProxyPassReverse "/" "${proxyProtocol}://${proxyHost}:${proxyPort}/"
    <Proxy *>
EOF

  if [[ -n "${basicAuthUserName}" ]] && [[ "${basicAuthUserName}" != "-" ]]; then
    cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
      AuthType Basic
      AuthName "${serverName}"
      AuthBasicProvider file
      AuthUserFile "${basicAuthUserFilePath}/${serverName}.htpasswd"
      Require valid-user
      RewriteEngine On
      RewriteRule .* - [E=PROXY_USER:%{LA-U:REMOTE_USER},NS]
      RequestHeader set X-WEBAUTH-USER "%{PROXY_USER}e"
EOF
  fi

  cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
    </Proxy>
EOF

  cat <<EOF | sudo tee -a "/etc/apache2/sites-available/${serverName}.conf" > /dev/null
    LogLevel warn
    ErrorLog ${logPath}/${serverName}-apache-ssl-error.log
    CustomLog ${logPath}/${serverName}-apache-ssl-access.log combined
  </VirtualHost>
</IfModule>
EOF
fi

echo "Enabling configuration at: /etc/apache2/sites-enabled/${serverName}.conf"
test ! -f "/etc/apache2/sites-enabled/${serverName}.conf" && sudo a2ensite "${serverName}.conf"

echo "Restarting Apache"
sudo service apache2 restart
