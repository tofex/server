#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  Old HTTP port
  -n  New HTTP port
  -o  Old SSL port
  -s  New SSL port

Example: ${scriptName} -p 80 -n 8080 -o 443 -s 8443
EOF
}

trim()
{
  echo -n "$1" | xargs
}

oldHttpPort="80"
newHttpPort="8080"
oldSslPort="443"
newSslPort="8443"

while getopts hp:n:o:s:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) oldHttpPort=$(trim "$OPTARG");;
    n) newHttpPort=$(trim "$OPTARG");;
    o) oldSslPort=$(trim "$OPTARG");;
    s) newSslPort=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${oldHttpPort}" ]]; then
  echo "No old HTTP port specified!"
  exit 1
fi

if [[ -z "${newHttpPort}" ]]; then
  echo "No new HTTP port specified!"
  exit 1
fi

if [[ -z "${oldSslPort}" ]]; then
  echo "No old SSL port specified!"
  exit 1
fi

if [[ -z "${newSslPort}" ]]; then
  echo "No new SSL port specified!"
  exit 1
fi

replace-file-content /etc/apache2/ports.conf "${newHttpPort}" "${oldHttpPort}"
replace-file-content /etc/apache2/ports.conf "${newSslPort}" "${oldSslPort}"

mkdir -p /opt/install/
crudini --set /opt/install/env.properties apache httpPort "${newHttpPort}"
crudini --set /opt/install/env.properties apache sslPort "${newSslPort}"
