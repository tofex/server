#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  --help                   Show this message
  --apacheHttpPort         HTTP port, default: 80
  --apacheSslPort          SSL port, default: 443
  --webPath                Web path, required if no proxy data
  --webUser                Web user, default: www-data
  --webGroup               Web group, default: www-data
  --proxyHostPath          Proxy host path (optional), default: /
  --proxyProtocol          Proxy protocol (optional), default: http
  --proxyHost              Proxy host (optional)
  --proxyPort              Proxy port (optional), default: 80 (http) or 443 (https)
  --proxyPath              Proxy path (optional), default: /
  --logPath                Log path, default: /var/log/apache2
  --logLevel               Log level, default: warn
  --serverName             Server name
  --serverAdmin            Server admin, default: webmaster@<server name>
  --sslTerminated          SSL terminated (yes/no), default: no
  --forceSsl               Force SSL (yes/no), default: yes
  --basicAuthUserName      Basic auth user name (optional)
  --basicAuthPassword      Basic auth password (optional)
  --basicAuthUserFilePath  Basic auth user file path (optional), default: /var/www
  --overwrite              Overwrite existing files (yes/no), default: no

Example: ${scriptName} --webPath /var/www/project01/htdocs --serverName project01.net --sslTerminated no --forceSsl yes
EOF
}

apacheHttpPort=
apacheSslPort=
webPath=
webUser=
webGroup=
proxyHostPath=
proxyProtocol=
proxyHost=
proxyPort=
proxyPath=
logPath=
logLevel=
serverName=
serverAdmin=
sslTerminated=
forceSsl=
basicAuthUserName=
basicAuthPassword=
basicAuthUserFilePath=
overwrite=

source "${currentPath}/../../prepare-parameters.sh"

if [[ -z "${apacheHttpPort}" ]]; then
  apacheHttpPort=80
fi

if [[ -z "${apacheSslPort}" ]]; then
  apacheSslPort=443
fi

if [[ -z "${webPath}" ]] || [[ "${webPath}" == "-" ]]; then
  if [[ -z "${proxyHost}" ]] || [[ "${proxyHost}" == "-" ]]; then
    echo "No web path or proxy host specified!"
    exit 1
  else
    webPath="-"
  fi
elif [[ "${webPath}" != */ ]]; then
  webPath="${webPath}/"
fi

if [[ -z "${webUser}" ]]; then
  webUser="www-data"
fi

if [[ -z "${webGroup}" ]]; then
  webGroup="www-data"
fi

if [[ -z "${proxyHostPath}" ]]; then
  proxyHostPath="/"
fi

if [[ -z "${proxyProtocol}" ]]; then
  proxyProtocol="http"
fi

if [[ -z "${proxyHost}" ]]; then
  proxyHost="-"
fi

if [[ -z "${proxyPort}" ]]; then
  if [[ "${proxyProtocol}" == "http" ]]; then
    proxyPort=80
  elif [[ "${proxyProtocol}" == "https" ]]; then
    proxyPort=443
  else
    proxyPort="-"
  fi
fi

if [[ -z "${proxyPath}" ]]; then
  proxyPath="/"
fi

if [[ -z "${logPath}" ]]; then
  logPath="/var/log/apache2"
fi

if [[ -z "${logLevel}" ]]; then
  logLevel="warn"
fi

if [[ -z "${serverName}" ]]; then
  echo "No server name specified!"
  exit 1
fi

if [[ -z "${serverAdmin}" ]]; then
  serverAdmin="webmaster@${serverName}"
fi

if [[ -z "${sslTerminated}" ]]; then
  sslTerminated="no"
fi

if [[ -z "${forceSsl}" ]]; then
  forceSsl="yes"
fi

if [[ -z "${basicAuthUserName}" ]]; then
  basicAuthUserName="-"
fi

if [[ -z "${basicAuthPassword}" ]]; then
  basicAuthPassword="-"
fi

if [[ -z "${basicAuthUserFilePath}" ]]; then
  basicAuthUserFilePath="/var/www"
fi

if [[ -z "${overwrite}" ]]; then
  overwrite="no"
fi

distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" \
  --apacheHttpPort "${apacheHttpPort}" \
  --apacheSslPort "${apacheSslPort}" \
  --webPath "${webPath}" \
  --webUser "${webUser}" \
  --webGroup "${webGroup}" \
  --proxyHostPath "${proxyHostPath}" \
  --proxyProtocol "${proxyProtocol}" \
  --proxyHost "${proxyHost}" \
  --proxyPort "${proxyPort}" \
  --proxyPath "${proxyPath}" \
  --logPath "${logPath}" \
  --logLevel "${logLevel}" \
  --serverName "${serverName}" \
  --serverAdmin "${serverAdmin}" \
  --sslTerminated "${sslTerminated}" \
  --forceSsl "${forceSsl}" \
  --basicAuthUserName "${basicAuthUserName}" \
  --basicAuthPassword "${basicAuthPassword}" \
  --basicAuthUserFilePath "${basicAuthUserFilePath}" \
  --overwrite "${overwrite}"
