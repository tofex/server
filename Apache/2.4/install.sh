#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF

usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  HTTP port, default: 80
  -s  SSL Port, default: 443
  -c  SSL certificate file, default: /etc/ssl/certs/ssl-cert-snakeoil.pem
  -k  SSL key file, default: /etc/ssl/private/ssl-cert-snakeoil.key

Example: ${scriptName} -p 80 -s 443
EOF
}

trim()
{
  echo -n "$1" | xargs
}

httpPort="80"
sslPort="443"
sslCertFile="/etc/ssl/certs/ssl-cert-snakeoil.pem"
sslKeyFile="/etc/ssl/private/ssl-cert-snakeoil.key"

while getopts hp:s:c:k:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) httpPort=$(trim "$OPTARG");;
    s) sslPort=$(trim "$OPTARG");;
    c) sslCertFile=$(trim "$OPTARG");;
    k) sslKeyFile=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${httpPort}" ]]; then
  echo "No HTTP port specified!"
  exit 1
fi

if [[ -z "${sslPort}" ]]; then
  echo "No SSL port specified!"
  exit 1
fi

if [[ -z "${sslCertFile}" ]]; then
  echo "No SSL certificate file specified!"
  exit 1
fi

if [[ ! -f "${sslCertFile}" ]]; then
  echo "Invalid SSL certificate file specified!"
  exit 1
fi

if [[ -z "${sslKeyFile}" ]]; then
  echo "No SSL key file specified!"
  exit 1
fi

if [[ ! -f "${sslKeyFile}" ]]; then
  echo "Invalid SSL key file specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -p "${httpPort}" -s "${sslPort}" -c "${sslCertFile}" -k "${sslKeyFile}"
