#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -u  User to add keys to
  -v  Verbose

Example: ${scriptName} -u www-data
EOF
}

trim()
{
  echo -n "$1" | xargs
}

userName=
verbose=0

while getopts hu:v? option; do
  case ${option} in
    h) usage; exit 1;;
    u) userName=$(trim "$OPTARG");;
    v) verbose=1;;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${userName}" ]]; then
  echo "No user name specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  distributionReleaseScript="${currentPath}/${distribution}/${scriptName}"
  if [[ ! -f "${distributionReleaseScript}" ]]; then
    echo "Missing script: ${distributionReleaseScript}"
    exit 1
  fi
fi

if [[ "${verbose}" == 1 ]]; then
  "${distributionReleaseScript}" -u "${userName}" -v
else
  "${distributionReleaseScript}" -u "${userName}"
fi
