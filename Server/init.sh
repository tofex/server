#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseFunctionScript="${currentPath}/${distribution}/${release}/functions.sh"
if [[ -f "${distributionReleaseFunctionScript}" ]]; then
  source "${distributionReleaseFunctionScript}"
else
  distributionFunctionScript="${currentPath}/${distribution}/functions.sh"
  if [[ -f "${distributionFunctionScript}" ]]; then
    source "${distributionFunctionScript}"
  else
    echo "Missing script: ${distributionFunctionScript}"
    exit 1
  fi
fi

"${currentPath}/add-keys.sh" -u "root"
