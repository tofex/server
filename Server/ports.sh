#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -o  Host

Example: ${scriptName} -o 127.0.0.1
EOF
}

trim()
{
  echo -n "$1" | xargs
}

host=

while getopts ho:? option; do
  case ${option} in
    h) usage; exit 1;;
    o) host=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ $(which nmap | wc -l) == 0 ]]; then
  if [[ $(which install-package | wc -l) -eq 1 ]]; then
    install-package nmap
  else
    UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages nmap 2>&1
  fi
fi

if [[ -z "${host}" ]]; then
  echo -n "Getting external IP"
  externalIp=$(curl -s -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip | cat)
  if [[ -z "${externalIp}" ]]; then
    externalIp=$(curl -s https://ipinfo.io/ip | cat)
  fi

  if [[ -z "${externalIp}" ]]; then
    echo ": Could not determine external IP"
    exit 1
  fi
  echo ": ${externalIp}"
  host="${externalIp}"
fi

nmap --open "${host}" | grep "^[0-9]" | sort -n
