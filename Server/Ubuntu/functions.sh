#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Adding system functions"

cat <<EOF > /usr/local/bin/has-repository
#!/bin/bash -e
urlPart="\${1}"
if [ \$(find /etc/apt/ -name *.list | xargs cat | grep  ^[[:space:]]*deb | grep -v deb-src | grep "\${urlPart}" | wc -l) -gt 0 ]; then
    exit 0
else
    exit 1
fi
EOF
chmod +x /usr/local/bin/has-repository

cat <<EOF > /usr/local/bin/add-ppa-repository
#!/bin/bash -e
if [[ \${1} == ppa:* ]]; then
    launchPadUrl=\$(echo "\${1}" | sed -e "s/ppa:/ppa.launchpad.net\//")
    if has-repository "\${launchPadUrl}"; then
        echo "Repository \${1} already installed"
        exit 0
    fi
fi
install-package software-properties-common
install-package dirmngr
install-package apt-transport-https
install-package lsb-release
install-package ca-certificates
echo "Installing repository: \${1}"
add-apt-repository -y "\${1}" 2>&1
update-apt
EOF
chmod +x /usr/local/bin/add-ppa-repository

cat <<EOF > /usr/local/bin/add-server-key-id
#!/bin/bash -e
server="\${1}"
key="\${2}"
apt-key adv --keyserver "\${server}" --recv-keys "\${key}"
EOF
chmod +x /usr/local/bin/add-server-key-id

cat <<EOF > /usr/local/bin/add-repository
#!/bin/bash -e
aptFileName="/etc/apt/sources.list.d/\${1}"
repoUri="\${2}"
distribution="\${3}"
component="\${4}"
keyUri="\${5}"
addSource="\${6}"
binaryEntry="deb \${repoUri} \${distribution} \${component}"
sourceEntry="deb-src \${repoUri} \${distribution} \${component}"
if [ -f \${aptFileName} ]; then
  if [ \$(grep -Fx "\${binaryEntry}" \${aptFileName} | wc -l) -gt 0 ]; then
    echo "Repository \${2} already installed"
    exit 0
  fi
fi
echo "Installing repository: \${2}"
if [ ! -z \${keyUri} ]; then
  curl -L \${keyUri} | sudo apt-key add -
fi
if [ -f \${aptFileName} ]; then
  echo "\${binaryEntry}" >> \${aptFileName}
else
  echo "\${binaryEntry}" > \${aptFileName}
fi
if [ "\${addSource}" == "y" ]; then
  echo "\${sourceEntry}" >> \${aptFileName}
fi
update-apt
EOF
chmod +x /usr/local/bin/add-repository

cat <<EOF > /usr/local/bin/get-available-package-versions
#!/bin/bash -e
function quoteRegex()
{
  sed 's/[]\.\/|\$(){}?+*^[]/\\\\&/g' <<< "\$*" | sed 's/ /\\\\\s*/g'
}
packageName="\${1}"
baseVersion="\${2}"
if [ ! -z "\${baseVersion}" ]; then
  baseVersion=\$(quoteRegex "\${2}")
fi
while read -r line; do
  exploded=(\$(echo "\${line}" | tr "|" "\n"))
  if [ ! -z "\${baseVersion}" ]; then
    if [ \$(echo \${exploded[1]} | grep -P "^\${baseVersion}" | wc -l ) -gt 0 ]; then
      echo \${exploded[1]}
    fi
  else
    echo \${exploded[1]}
  fi
done < <(apt-cache madison \${packageName} | sort -rV)
EOF
chmod +x /usr/local/bin/get-available-package-versions

cat <<EOF > /usr/local/bin/get-latest-package-version
#!/bin/bash -e
packageName="\${1}"
baseVersion="\${2}"
versions=(\$(get-available-package-versions "\${packageName}" "\${baseVersion}"))
comparableVersions=()
declare -A originalVersions
for version in "\${versions[@]}"; do
  comparableVersion=\$(echo "\${version}" | sed 's/^[0-9]://')
  comparableVersions+=("\${comparableVersion}")
  originalVersions["\${comparableVersion}"]="\${version}"
done
sortedVersions=(\$(echo "\${comparableVersions[@]}" | tr " " "\n" | sed 's/^[0-9]://' | sort -rV))
latestVersion="\${sortedVersions[0]}"
echo "\${originalVersions[\${latestVersion}]}"
EOF
chmod +x /usr/local/bin/get-latest-package-version

cat <<EOF > /usr/local/bin/get-installed-package-version
#!/bin/bash -e
packageName="\${1}"
/usr/bin/dpkg-query -W \${packageName} 2>/dev/null | grep -P "^\${packageName}" | cat | awk '{print \$2}'
EOF
chmod +x /usr/local/bin/get-installed-package-version

cat <<EOF > /usr/local/bin/install-package
#!/bin/bash -e
packageName="\${1}"
baseVersion="\${2}"
latestVersion=\$(get-latest-package-version "\${packageName}" "\${baseVersion}")
installedVersion=\$(get-installed-package-version "\${packageName}")
if [ -z "\${latestVersion}" ]; then
  echo "Could not find latest version of package: \${packageName}"
  echo "Possible candidates:"
  echo \$(get-available-package-versions "\${packageName}") | tr " " "\n" | sort -rV | uniq
  exit -1
fi
if [ "\$latestVersion" == "\$installedVersion" ]; then
  echo "Latest package \${packageName}=\${latestVersion} already installed"
else
  echo "Installing latest package \${packageName}=\${latestVersion}"
  checkUnattendedUpgrades=\$(which check-unattended-upgrades)
  if [[ -n "\${checkUnattendedUpgrades}" ]]; then
    check-unattended-upgrades
  fi
  UCF_FORCE_CONFOLD=1 DEBIAN_FRONTEND=noninteractive apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages \${packageName}=\${latestVersion} 2>&1
fi
EOF
chmod +x /usr/local/bin/install-package

cat <<EOF > /usr/local/bin/install-package-from-deb
#!/bin/bash -e
packageName="\${1}"
version="\${2}"
uri="\${3}"
installedVersion=\$(get-installed-package-version "\${packageName}")
if [ "\$version" == "\$installedVersion" ]; then
    echo "Package \${packageName}=\${version} already installed"
else
    echo "Installing package \${packageName}=\${version}"
    wget -nv \${uri} -P /tmp
    dpkg --install "/tmp/\${uri##*/}"
    rm -rf "/tmp/\${uri##*/}"
fi
EOF
chmod +x /usr/local/bin/install-package-from-deb

cat <<EOF > /usr/local/bin/install-package-from-source
#!/bin/bash -e
packageName="\${1}"
version="\${2}"
uri="\${3}"
installedVersion=\$(get-installed-package-version "\${packageName}")
if [ "\${version}" == "\${installedVersion}" ]; then
    echo "Package \${packageName}=\${version} already installed"
else
    echo "Installing package \${packageName}=\${version}"
    mkdir -p /tmp/install-package-from-source
    cd /tmp/install-package-from-source
    wget -qO- \${uri} | tar xzpf -
    if [ ! -f configure ]; then
        extractedFolder=\$(find /tmp/install-package-from-source/* -maxdepth 0 -type d)
        cd \${extractedFolder}
    fi
    ./configure
    make all install
    cd /
    rm -rf /tmp/install-package-from-source
fi
EOF
chmod +x /usr/local/bin/install-package-from-source

cat <<EOF > /usr/local/bin/install-pecl-package
#!/bin/bash -e
packageName="\${1}"
version="\${2}"
installedVersion=\$(pecl list | grep -P "^\${packageName}\s" | grep -ohP "[0-9]+(\.[0-9]+)*" | cat)
if [ "\$version" == "\$installedVersion" ]; then
    echo "Latest PECL package \${packageName}=\${version} already installed"
else
    echo "Installing PECL latest package \${packageName}=\${version}"
    yes '' | pecl install -f \${packageName}-\${version}
fi
EOF
chmod +x /usr/local/bin/install-pecl-package

cat <<EOF > /usr/local/bin/install-pear-package
#!/bin/bash -e
packageName="\${1}"
version="\${2}"
installedVersion=\$(pear list | grep -P "^\${packageName}\s" | grep -ohP "[0-9]+(\.[0-9]+)*" | cat)
if [ "\$version" == "\$installedVersion" ]; then
    echo "Latest PEAR package \${packageName}=\${version} already installed"
else
    echo "Installing PEAR latest package \${packageName}=\${version}"
    pear install \${packageName}-\${version}
fi
EOF
chmod +x /usr/local/bin/install-pear-package

cat <<EOF > /usr/local/bin/get-latest-gem-version
#!/bin/bash -e
function quoteRegex()
{
    sed 's/[]\.\/|\$(){}?+*^[]/\\\\&/g' <<< "\$*" | sed 's/ /\\\\\s*/g'
}
gemName="\${1}"
baseVersion=
if [ -z "\${2}" ]; then
    baseVersion="[0-9]+\.[0-9]+"
else
    baseVersion=\$(quoteRegex "\${2}")
fi
versions=(\$(gem list --remote --all \${gemName} | grep -P "^\${gemName}\s" | grep -ohP "[0-9]+(\.[0-9]+)*" | grep -P "^\${baseVersion}" | sort -rV))
latestVersion=\${versions[0]}
echo \${latestVersion}
EOF
chmod +x /usr/local/bin/get-latest-gem-version

cat <<EOF > /usr/local/bin/get-installed-gem-version
#!/bin/bash -e
gemName="\${1}"
gem list --local \${gemName} | grep -P "^\${gemName}\s" | grep -ohP "[0-9]+(\.[0-9]+)*" | cat
EOF
chmod +x /usr/local/bin/get-installed-gem-version

cat <<EOF > /usr/local/bin/install-gem
#!/bin/bash -e
gemName="\${1}"
baseVersion="\${2}"
latestVersion=\$(get-latest-gem-version "\${gemName}" "\${baseVersion}")
installedVersion=\$(get-installed-gem-version "\${gemName}")
if [ -z "\${latestVersion}" ]; then
    echo "Could not find latest version of gem: \${gemName}"
    echo "Possible candidates:"
    gem list --remote --all \${gemName} | grep -P "^\${gemName}\s" | grep -ohP "[0-9]+(\.[0-9]+)*" | sort -rV | uniq
    exit -1
fi
if [ "\$latestVersion" == "\$installedVersion" ]; then
    echo "Latest gem \${gemName}=\${latestVersion} already installed"
else
    echo "Installing latest gem \${gemName}=\${latestVersion}"
    gem install \${gemName} -v \${latestVersion}
fi
EOF
chmod +x /usr/local/bin/install-gem

cat <<EOF > /usr/local/bin/check-unattended-upgrades
#!/bin/bash -e
apt=\$(which apt)
if [[ -n "\${apt}" ]]; then
    first=1
    while [[ \$(ps aux | grep -i "\${apt}" | grep -v grep | wc -l) -gt 0 ]] || [[ \$(ps aux | grep -i "apt.systemd.daily lock_is_held" | grep -v grep | wc -l) -gt 0 ]]; do
        if [[ "\${first}" == 1 ]]; then
           echo -n "System not ready"
        else
            echo -n "."
        fi
        sleep 1
        first=0
    done
    if [[ "\${first}" == 0 ]]; then
        echo ""
    fi
fi
EOF
chmod +x /usr/local/bin/check-unattended-upgrades

cat <<EOF > /usr/local/bin/add-host-name
#!/bin/bash -e
hostName="\${1}"
ipAddress="\${2}"
if [[ -z "\${ipAddress}" ]]; then
  ipAddress="127.0.0.1"
fi
if [ \$(grep -F "\${ipAddress} \${hostName}" /etc/hosts | wc -l) -eq 0 ]; then
  echo "\${ipAddress} \${hostName}" >> /etc/hosts
fi
EOF
chmod +x /usr/local/bin/add-host-name

cat <<EOF > /usr/local/bin/update-host-name
#!/bin/bash -e
hostName="\${1}"
ipAddress="\${2}"
if [[ -z "\${ipAddress}" ]]; then
  ipAddress="127.0.0.1"
fi
if [ \$(grep -E "\s+\${hostName}\s*\$" /etc/hosts | wc -l) -eq 0 ]; then
  echo "\${ipAddress} \${hostName}" >> /etc/hosts
else
  lineNumber=\$(grep -En "\s+\${hostName}\s*\$" /etc/hosts | awk -F: '{print \$1}')
  sed -i "\${lineNumber}s/.*/\${ipAddress} \${hostName}/" /etc/hosts
fi
EOF
chmod +x /usr/local/bin/update-host-name

cat <<EOF > /usr/local/bin/save-file
#!/bin/bash -e
savePath="/var/install/save"
fileName=\${1}
local=\${2:-0}
if [ -f "\${fileName}" ] || [ -d "\${fileName}" ]; then
    if [ "\${local}" -eq 1 ]; then
        saveFileName="\${fileName}.old"
    else
        saveFileName=\${savePath}/\$(echo "\${fileName}" | sed -e 's/^\///')
    fi
    echo "Saving file: \${fileName} to: \${saveFileName}"
    mkdir -p \$(dirname "\${saveFileName}")
    cp -a -r "\${fileName}" "\${saveFileName}"
else
    echo "Requested file: \${fileName} to save does not exist"
fi
EOF
chmod +x /usr/local/bin/save-file

cat <<EOF > /usr/local/bin/store-file
#!/bin/bash -e
storePath="/var/install/store"
fileName=\${1}
storeFileName=\${storePath}/\$(echo "\${fileName}" | sed -e 's/^\///')
if [ -f "\${fileName}" ] || [ -d "\${fileName}" ]; then
    if [ ! -f "\${storeFileName}" ] && [ ! -d "\${storeFileName}" ]; then
        echo "Storing file: \${fileName} to: \${saveFileName}"
        mkdir -p \$(dirname "\${storeFileName}")
        cp -a -r "\${fileName}" "\${storeFileName}"
    else
        echo "Store file: \${storeFileName} already exists"
    fi
else
    echo "Requested file: \${fileName} to store does not exist"
fi
EOF
chmod +x /usr/local/bin/store-file

cat <<EOF > /usr/local/bin/restore-file
#!/bin/bash -e
storePath="/var/install/store"
fileName=\${1}
storeFileName=\${storePath}/\$(echo "\${fileName}" | sed -e 's/^\///')
if [ -f "\${storeFileName}" ] || [ -d "\${storeFileName}" ]; then
    echo "Restoring file: \${fileName} from: \${storeFileName}"
    rm -rf "\${fileName}"
    cp -a -r "\${storeFileName}" "\${fileName}"
else
    echo "Requested file: \${storeFileName} to restore to: \${fileName} does not exist"
fi
EOF
chmod +x /usr/local/bin/restore-file

cat <<EOF > /usr/local/bin/add-ssh-key
#!/bin/bash -e
file="\${1}"
echo "Using file: \${file}"
shift
while [ "\${1}" ]; do
  home=\$(awk -F: -v u="\${1}" '\$1==u{print \$6}' /etc/passwd)
  fileName=\$(basename "\${file}")
  mkdir -p -m 700 "\${home}/.ssh"
  touch "\${home}/.ssh/authorized_keys"
  if [[ \$(grep -f "\${file}" "\${home}/.ssh/authorized_keys" | wc -l) -eq 0 ]]; then
    echo "Adding public key to: \${home}/.ssh/authorized_keys"
    echo "# \${fileName}" >> "\${home}/.ssh/authorized_keys"
    cat "\${file}" >> "\${home}/.ssh/authorized_keys"
    chmod 600 "\${home}/.ssh/authorized_keys"
    chown "\${1}": "\${home}/.ssh/authorized_keys"
  else
    echo "Key already added to: \${home}/.ssh/authorized_keys"
  fi
  shift
done
EOF
chmod +x /usr/local/bin/add-ssh-key

cat <<EOF > /usr/local/bin/add-ssh-key-quiet
#!/bin/bash -e
file="\${1}"
shift
while [ "\${1}" ]; do
  home=\$(awk -F: -v u="\${1}" '\$1==u{print \$6}' /etc/passwd)
  fileName=\$(basename "\${file}")
  mkdir -p -m 700 "\${home}/.ssh"
  touch "\${home}/.ssh/authorized_keys"
  if [[ \$(grep -f "\${file}" "\${home}/.ssh/authorized_keys" | wc -l) -eq 0 ]]; then
    echo "# \${fileName}" >> "\${home}/.ssh/authorized_keys"
    cat "\${file}" >> "\${home}/.ssh/authorized_keys"
    chmod 600 "\${home}/.ssh/authorized_keys"
    chown "\${1}": "\${home}/.ssh/authorized_keys"
  fi
  shift
done
EOF
chmod +x /usr/local/bin/add-ssh-key-quiet

cat <<EOF > /usr/local/bin/append-ssh-key
#!/bin/bash -e
file="\${1}"
echo "Using file: \${file}"
shift
while [ "\${1}" ]; do
  home=\$(awk -F: -v u="\${1}" '\$1==u{print \$6}' /etc/passwd)
  fileName=\$(basename "\${file}")
  mkdir -p -m 700 "\${home}/.ssh"
  touch "\${home}/.ssh/authorized_keys"
  echo "Adding public key to: \${home}/.ssh/authorized_keys"
  echo "# \${fileName}" >> "\${home}/.ssh/authorized_keys"
  cat "\${file}" >> "\${home}/.ssh/authorized_keys"
  chmod 600 "\${home}/.ssh/authorized_keys"
  chown "\${1}": "\${home}/.ssh/authorized_keys"
  shift
done
EOF
chmod +x /usr/local/bin/append-ssh-key

cat <<EOF > /usr/local/bin/generate-ssh-key
#!/bin/bash -e
currentUser=\$(whoami)
home=\$(awk -F: -v u="\${currentUser}" '\$1==u{print \$6}' /etc/passwd)
if [[ ! -f \${home}/.ssh/id_rsa ]] && [[ ! -f \${home}/.ssh/id_rsa.pub ]]; then
  ssh-keygen -b 4096 -t rsa -f \${home}/.ssh/id_rsa -q -N ""
else
  echo "Key already exists"
fi
EOF
chmod +x /usr/local/bin/generate-ssh-key

cat <<EOF > /usr/local/bin/get-ssh-public-key
#!/bin/bash -e
currentUser=\$(whoami)
home=\$(awk -F: -v u="\${currentUser}" '\$1==u{print \$6}' /etc/passwd)
if [[ -f \${home}/.ssh/id_rsa.pub ]]; then
  cat \${home}/.ssh/id_rsa.pub
fi
EOF
chmod +x /usr/local/bin/get-ssh-public-key

cat <<EOF > /usr/local/bin/add-certificate
#!/bin/bash -e
hostName="\${1}"
sslCertificate="\${2}"
sslKey="\${3}"
if [ ! -f "\${sslCertificate}" ] || [ ! -f "\${sslKey}" ]; then
  echo "Creating certificate files with key at: \${sslKey} and certificate at: \${sslCertificate}"
  \$sslKeyPath=\$(basename "\${sslKey}")
  if [[ ! -d "\${sslKeyPath}" ]]; then
    echo "Creating directory at: \${sslKeyPath}"
    mkdir -p "\${sslKeyPath}"
  fi
  \$sslCertificatePath=\$(basename "\${sslCertificate}")
  if [[ ! -d "\${sslCertificatePath}" ]]; then
    echo "Creating directory at: \${sslCertificatePath}"
    mkdir -p "\${sslCertificatePath}"
  fi
  openssl req -nodes -new -x509 -days 3650 -subj "/C=DE/ST=Thuringia/L=Jena/O=Server/OU=Development/CN=\${hostName}" -keyout "\${sslKey}" -out "\${sslCertificate}"
else
  echo "Certificate files already exist"
fi
EOF
chmod +x /usr/local/bin/add-certificate

cat <<EOF > /usr/local/bin/replace-file-content
#!/bin/bash -e
function quoteInsert()
{
    sed 's/\//\\\\&/g' <<< "\$*" | sed 's/"/\\\\&/g'
}
function quoteRegex()
{
    sed 's/[]\.\/|\$(){}?+*^[]/\\\\&/g' <<< "\$*" | sed 's/ /\\\\\s*/g' | sed 's/"/\\\\&/g'
}
function pregQuoteRegex()
{
    echo \$(quoteRegex "\$*") | sed s/^-/\\\\\\\\-/g
}
fileName="\${1}"
replace="\${2}"
find="\${3}"
checkReplaced=\${4:-1}
findPattern=\$(quoteRegex "\${find}")
pregFindPattern=\$(pregQuoteRegex "\${find}")
replacePattern=\$(pregQuoteRegex "\${replace}")
replace=\$(quoteInsert "\${replace}")
if [ \$(grep -P "\${pregFindPattern}" "\${fileName}" | wc -l) -gt 0 ] &&
    ([ \${checkReplaced} -eq 0 ] || [ \$(grep -P "\${replacePattern}" "\${fileName}" | wc -l) -eq 0 ]); then
    echo sed -i -e \"s/\${findPattern}/\${replace}/g\" \${fileName}
    sed -i -e "s/\${findPattern}/\${replace}/g" \${fileName}
fi
EOF
chmod +x /usr/local/bin/replace-file-content

cat <<EOF > /usr/local/bin/add-file-content-before
#!/bin/bash -e
function quoteInsert()
{
    sed 's/\//\\\\&/g' <<< "\$*" | sed 's/"/\\\\&/g'
}
function quoteRegex()
{
    sed 's/[]\.\/|\$(){}?+*^[]/\\\\&/g' <<< "\$*" | sed 's/ /\\\\\s*/g' | sed 's/"/\\\\&/g'
}
function pregQuoteRegex()
{
    echo \$(quoteRegex "\$*") | sed s/^-/\\\\\\\\-/g
}
fileName="\${1}"
content="\${2}"
before="\${3}"
lineBreak="\${4}"
if [ "\${lineBreak}" == 1 ]; then
    lineBreak="\\\\n"
else
    lineBreak=
fi
beforePattern=\$(quoteRegex "\${before}")
beforeContent=\$(quoteInsert "\${before}")
pregBeforePattern=\$(pregQuoteRegex "\${before}")
contentPattern=\$(pregQuoteRegex "\${content}")
content=\$(quoteInsert "\${content}")
if [ \$(grep -P \${pregBeforePattern} "\${fileName}" | wc -l) -gt 0 ] && [ \$(grep -P \${contentPattern}\$ "\${fileName}" | wc -l) -eq 0 ]; then
    echo "Insert: \${content}"
    sed -i -e "s/\${beforePattern}/\${content}\${lineBreak}\${beforeContent}/g" \${fileName}
fi
EOF
chmod +x /usr/local/bin/add-file-content-before

cat <<EOF > /usr/local/bin/add-file-content-after
#!/bin/bash -e
function quoteInsert()
{
    sed 's/\//\\\\&/g' <<< "\$*" | sed 's/"/\\\\&/g'
}
function quoteRegex()
{
    sed 's/[]\.\/|\$(){}?+*^[]/\\\\&/g' <<< "\$*" | sed 's/ /\\\\\s*/g' | sed 's/"/\\\\&/g'
}
function pregQuoteRegex()
{
    echo \$(quoteRegex "\$*") | sed s/^-/\\\\\\\\-/g
}
fileName="\${1}"
content="\${2}"
after="\${3}"
lineBreak="\${4}"
if [ "\${lineBreak}" == 1 ]; then
    lineBreak="\\\\n"
else
    lineBreak=
fi
afterPattern=\$(quoteRegex "\${after}")
afterContent=\$(quoteInsert "\${after}")
pregAfterPattern=\$(pregQuoteRegex "\${after}")
contentPattern=\$(pregQuoteRegex "\${content}")
content=\$(quoteInsert "\${content}")
if [ \$(grep -P \${pregAfterPattern} "\${fileName}" | wc -l) -gt 0 ] && [ \$(grep -P \${contentPattern}\$ "\${fileName}" | wc -l) -eq 0 ]; then
    echo "Insert: \${content}"
    sed -i -e "s/\${afterPattern}/\${afterContent}\${lineBreak}\${content}/g" \${fileName}
fi
EOF
chmod +x /usr/local/bin/add-file-content-after

cat <<EOF > /usr/local/bin/container-exists
#!/bin/bash -e
containerName="\${1}"
docker ps -a | grep -E "\\s\${containerName}\$" | wc -l
EOF
chmod +x /usr/local/bin/container-exists

cat <<EOF > /usr/local/bin/container-running
#!/bin/bash -e
containerName="\${1}"
docker ps | grep -E "\\s\${containerName}\$" | wc -l
EOF
chmod +x /usr/local/bin/container-running

cat <<EOF > /usr/local/bin/container-create
#!/bin/bash -e
imageName="\${1}"
shift
containerName="\${1}"
shift
ports=("\$@")
if [[ \$(container-running "\${containerName}") == 0 ]]; then
  command="docker create"
  for port in "\${ports[@]}"; do
    command+=" -p \${port}"
  done
  command+=" --name \"\${containerName}\" \"\${imageName}\""
  echo "Creating container: \${containerName} with image: \${imageName}"
  bash -c "\${command}"
fi
EOF
chmod +x /usr/local/bin/container-create

cat <<EOF > /usr/local/bin/container-start
#!/bin/bash -e
containerName="\${1}"
if [[ \$(container-exists "\${containerName}") == 1 ]]; then
  echo "Starting container: \${containerName}"
  docker start "\${containerName}"
fi
EOF
chmod +x /usr/local/bin/container-start

cat <<EOF > /usr/local/bin/container-stop
#!/bin/bash -e
containerName="\${1}"
if [[ \$(container-running "\${containerName}") == 1 ]]; then
  echo "Stopping container: \${containerName}"
  docker stop "\${containerName}"
fi
EOF
chmod +x /usr/local/bin/container-stop

cat <<EOF > /usr/local/bin/container-remove
#!/bin/bash -e
containerName="\${1}"
if [[ \$(container-exists "\${containerName}") == 1 ]]; then
  echo "Removing container: \${containerName}"
  docker rm "\${containerName}"
fi
EOF
chmod +x /usr/local/bin/container-remove

cat <<EOF > /usr/local/bin/container-ip
#!/bin/bash -e
containerName="\${1}"
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "\${containerName}"
EOF
chmod +x /usr/local/bin/container-ip

cat <<EOF > /usr/local/bin/container-copy
#!/bin/bash -e
containerName="\${1}"
localFileName="\${2}"
remoteFileName="\${3}"
if [[ -z "\${remoteFileName}" ]]; then
  remoteFileName=\$(basename "\${localFileName}")
fi
if [[ \$(container-running "\${containerName}") == 1 ]]; then
  echo "Copying script from: \${localFileName} to container: \${containerName} at: \${remoteFileName}"
  docker cp "\${localFileName}" "\${containerName}:\${remoteFileName}"
fi
EOF
chmod +x /usr/local/bin/container-copy

cat <<EOF > /usr/local/bin/container-copy-quiet
#!/bin/bash -e
containerName="\${1}"
localFileName="\${2}"
remoteFileName="\${3}"
if [[ -z "\${remoteFileName}" ]]; then
  remoteFileName=\$(basename "\${localFileName}")
fi
if [[ \$(container-running "\${containerName}") == 1 ]]; then
  docker cp "\${localFileName}" "\${containerName}:\${remoteFileName}"
fi
EOF
chmod +x /usr/local/bin/container-copy-quiet

cat <<EOF > /usr/local/bin/image-exists
#!/bin/bash -e
imageName="\${1}"
imageVersion="\${2}"
docker images | grep -E "^\${imageName}\\s+\${imageVersion}\\s" | wc -l
EOF
chmod +x /usr/local/bin/image-exists

cat <<EOF > /usr/local/bin/image-remove
#!/bin/bash -e
imageName="\${1}"
imageVersion="\${2}"
if [[ \$(image-exists "\${imageName}" "\${imageVersion}") == 1 ]]; then
  echo "Removing image: \${imageName}:\${imageVersion}"
  docker image rm "\${imageName}:\${imageVersion}"
fi
EOF
chmod +x /usr/local/bin/image-remove

cat <<EOF > /usr/local/bin/run-as-user
#!/bin/bash -e
userName="\${1}"
shift
command="\${1}"
shift
parameters=("\$@")
for parameter in "\${parameters[@]}"; do
  command+=" \"\${parameter}\""
done
sudo -H -u "\${userName}" bash -c "if [[ \\\$(which ini-parse | wc -l) -eq 0 ]]; then if [[ -f ~/.profile ]]; then source ~/.profile; elif [[ -f ~/.bash_profile ]]; then source ~/.bash_profile; fi; fi; \${command}"
EOF
chmod +x /usr/local/bin/run-as-user

functionFiles=( $(find "${currentPath}/functions" -type f -name "*.sh") )

for functionFile in "${functionFiles[@]}"; do
  functionScriptName=$(basename "${functionFile}" | sed 's/\(.*\)\..*/\1/')
  cp "${functionFile}" "/usr/local/bin/${functionScriptName}"
  chmod +x "/usr/local/bin/${functionScriptName}"
done
