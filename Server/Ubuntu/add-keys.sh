#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -u  User to add keys to
  -v  Verbose

Example: ${scriptName} -u www-data
EOF
}

trim()
{
  echo -n "$1" | xargs
}

userName=
verbose=0

while getopts hu:v? option; do
  case ${option} in
    h) usage; exit 1;;
    u) userName=$(trim "$OPTARG");;
    v) verbose=1;;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${userName}" ]]; then
  echo "No user name specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ "${verbose}" == 1 ]]; then
  echo "Adding keys to user: ${userName}"
  add-ssh-key "${currentPath}/../Keys/aknollmann.pub" "${userName}"
  add-ssh-key "${currentPath}/../Keys/lstritzke.pub" "${userName}"
  add-ssh-key "${currentPath}/../Keys/mbronischewsky.pub" "${userName}"
  add-ssh-key "${currentPath}/../Keys/padler.pub" "${userName}"
else
  add-ssh-key-quiet "${currentPath}/../Keys/aknollmann.pub" "${userName}"
  add-ssh-key-quiet "${currentPath}/../Keys/lstritzke.pub" "${userName}"
  add-ssh-key-quiet "${currentPath}/../Keys/mbronischewsky.pub" "${userName}"
  add-ssh-key-quiet "${currentPath}/../Keys/padler.pub" "${userName}"
fi
