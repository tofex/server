#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -u  User name
  -g  User group, default: sftp_users
  -p  Password
  -c  Copy home from this user

Example: ${scriptName} -u login -g group -p password -c dropshipper
EOF
}

trim()
{
  echo -n "$1" | xargs
}

userName=
userGroup=
password=
copyUser=

while getopts hu:g:p:c:? option; do
  case ${option} in
    h) usage; exit 1;;
    u) userName=$(trim "$OPTARG");;
    g) userGroup=$(trim "$OPTARG");;
    p) password=$(trim "$OPTARG");;
    c) copyUser=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${userName}" ]]; then
  echo "No user name specified!"
  exit 1
fi

if [[ -z "${password}" ]]; then
  password="$(pwgen -s -c -n -1 -n -v -N 1 32)"
fi

if [[ -z "${userGroup}" ]]; then
  userGroup="sftp_users"
fi

echo "Adding user: ${userName}"
if [[ -d "/home/${userName}" ]]; then
  useradd -G "${userGroup}" -s "$(which mysecureshell)" "${userName}"
else
  useradd -G "${userGroup}" -s "$(which mysecureshell)" -m "${userName}"
fi

echo "Setting password"
echo "${userName}:${password}" | chpasswd

echo "Preparing home: /home/${userName}"
chmod 755 "/home/${userName}"
chown root "/home/${userName}"
chgrp -R "${userGroup}" "/home/${userName}"

echo "Creating upload directory: /home/${userName}/upload"
mkdir -p "/home/${userName}/upload"
chown "${userName}":"${userGroup}" "/home/${userName}/upload/"

echo "Finished adding user: ${userName} with password: ${password}"

if [[ -n "${copyUser}" ]] && [[ -d "/home/${copyUser}/" ]]; then
  echo "Preparing home default data from user: ${copyUser}"
  cp -ar "/home/${copyUser}"/* "/home/${userName}/"
  chown -hR "${userName}":"${userGroup}" "/home/${userName}/"
fi
