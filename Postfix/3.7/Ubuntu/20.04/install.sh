#!/bin/bash -e

debconf-set-selections <<< "postfix postfix/mailname string localhost.local"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
install-package mailutils "1:3.7"
install-package postfix "3.4"
replace-file-content /etc/postfix/main.cf "inet_interfaces = loopback-only" "inet_interfaces = all"
systemctl restart postfix
