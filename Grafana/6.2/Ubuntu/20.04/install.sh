#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  Port, default: 3000

Example: ${scriptName} -p 3000
EOF
}

trim()
{
  echo -n "$1" | xargs
}

port="3000"

while getopts hp:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) port=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${port}" ]]; then
  echo "No port specified!"
  exit 1
fi

install-package libfontconfig1

echo "Installing Grafana"
wget https://dl.grafana.com/oss/release/grafana_6.2.1_amd64.deb
dpkg -i grafana_6.2.1_amd64.deb

replace-file-content /etc/grafana/grafana.ini "http_port = ${port}" ";http_port = 3000"

systemctl daemon-reload
systemctl start grafana-server
systemctl enable grafana-server.service

echo "Installing Pie Chart Plugin"
grafana-cli plugins install grafana-piechart-panel

echo "Restarting Grafana"
service grafana-server restart

mkdir -p /opt/install/
crudini --set /opt/install/env.properties grafana version "6.2"
crudini --set /opt/install/env.properties grafana port "${port}"
