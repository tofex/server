#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message

Example: ${scriptName}
EOF
}

trim()
{
  echo -n "$1" | xargs
}

while getopts h? option; do
  case ${option} in
    h) usage; exit 1;;
    ?) usage; exit 1;;
  esac
done

install-package openssh-server 1:9.6

echo "Creating start script at: /usr/local/bin/openssh.sh"
cat <<EOF > /usr/local/bin/openssh.sh
#!/bin/bash -e
/etc/init.d/ssh start
EOF
chmod +x /usr/local/bin/openssh.sh
