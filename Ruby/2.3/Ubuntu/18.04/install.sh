#!/bin/bash -e

install-package build-essential
install-package python3-software-properties
add-ppa-repository ppa:brightbox/ruby-ng
install-package ruby2.3
install-package ruby2.3-dev
