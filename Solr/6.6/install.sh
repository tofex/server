#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  Port, default: 8983
  -s  Service name, default: solr

Example: ${scriptName} -s solr01
EOF
}

trim()
{
  echo -n "$1" | xargs
}

port="8983"
serviceName="solr"

while getopts hp:s:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) port=$(trim "$OPTARG");;
    s) serviceName=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${port}" ]] || [[ "${port}" == "-" ]]; then
  echo "No port specified!"
  exit 1
fi

if [[ -z "${serviceName}" ]] || [[ "${serviceName}" == "-" ]]; then
  echo "No service name specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -p "${port}" -s "${serviceName}"
