#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -p  Port, default: 8983
  -s  Service name, default: solr

Example: ${scriptName} -s solr01
EOF
}

trim()
{
  echo -n "$1" | xargs
}

port=
serviceName=

while getopts hp:s:? option; do
  case ${option} in
    h) usage; exit 1;;
    p) port=$(trim "$OPTARG");;
    s) serviceName=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${port}" ]] || [[ "${port}" == "-" ]]; then
  port="8983"
fi

if [[ -z "${serviceName}" ]] || [[ "${serviceName}" == "-" ]]; then
  serviceName="solr"
fi

install-package python3-software-properties
install-package debconf-utils
install-package openjdk-8-jre

if [[ ! -d /opt/${serviceName} ]]; then
  cd /tmp

  echo "Downloading Solr"
  rm -rf solr-8.6.3.tgz
  rm -rf solr-8.6.3
  wget -nv http://archive.apache.org/dist/lucene/solr/8.6.3/solr-8.6.3.tgz

  echo "Unpacking Solr"
  tar xzf solr-8.6.3.tgz

  echo "Creating Solr service with name \"${serviceName}\" on port ${port}"
  solr-8.6.3/bin/install_solr_service.sh solr-8.6.3.tgz -p "${port}" -s "${serviceName}"

  echo "Cleaning up Solr download"
  rm -rf solr-8.6.3.tgz
  rm -rf solr-8.6.3

  echo "Stopping Solr service with name \"${serviceName}\""
  service "${serviceName}" stop

  echo "Updating Solr configuration"
  replace-file-content "/var/${serviceName}/log4j.properties" "log4j.rootLogger=WARN, file, CONSOLE" "log4j.rootLogger=INFO, file, CONSOLE"

  echo "Starting Solr service with name \"${serviceName}\""
  service "${serviceName}" start
fi

if [[ -f /.dockerenv ]]; then
  echo "Stopping Solr"
  service "${serviceName}" stop

  echo "Removing service"
  systemctl disable solr.service

  echo "Creating start script at: /usr/local/bin/solr.sh"
  cat <<EOF > /usr/local/bin/solr.sh
#!/bin/bash -e
ulimit -n 65535
sudo -H -u solr bash -c "SOLR_INCLUDE=/etc/default/solr.in.sh /opt/solr/bin/solr start -f"
EOF
  chmod +x /usr/local/bin/solr.sh
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties solr version "8.6"
crudini --set /opt/install/env.properties solr port "${port}"
crudini --set /opt/install/env.properties solr serviceName "${serviceName}"
