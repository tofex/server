#!/bin/bash -e

install-package php7.0-bcmath
install-package php7.0-curl
install-package php7.0-gd
install-package php7.0-intl
install-package php7.0-mbstring
install-package php7.0-mcrypt
install-package php7.0-mysql
install-package php7.0-soap
install-package php7.0-xmlrpc
install-package php7.0-xsl
install-package php7.0-zip

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../module/redis/5.1/install.sh"

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
