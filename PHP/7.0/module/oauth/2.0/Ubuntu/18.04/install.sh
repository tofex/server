#!/bin/bash -e

install-package build-essential
install-package php7.0-dev
install-package php-pear 1:1.10

install-pecl-package "oauth" 2.0.3

if [[ -d "/etc/php/5.6/mods-available" ]]; then
    echo "extension=oauth.so" > /etc/php/5.6/mods-available/oauth.ini
fi

echo "extension=oauth.so" > /etc/php/7.0/mods-available/oauth.ini

phpenmod oauth

if [[ ! -f /.dockerenv ]]; then
  if [[ $(which apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(which nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
