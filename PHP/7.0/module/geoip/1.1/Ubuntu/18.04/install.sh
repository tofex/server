#!/bin/bash -e

install-package build-essential
install-package php7.0-dev
install-package php-pear 1:1.10
install-package libgeoip-dev

install-pecl-package "geoip" 1.1.1

if [[ -d "/etc/php/5.6/mods-available" ]]; then
    echo "extension=geoip.so" > /etc/php/5.6/mods-available/geoip.ini
fi

echo "extension=geoip.so" > /etc/php/7.0/mods-available/geoip.ini

phpenmod geoip

if [[ ! -f /.dockerenv ]]; then
  if [[ $(which apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
    sleep 5
  fi

  if [[ $(which nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
