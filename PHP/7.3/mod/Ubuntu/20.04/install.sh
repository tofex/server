#!/bin/bash -e

install-package python3-software-properties
add-ppa-repository ppa:ondrej/php
install-package php7.3-dev
install-package php7.3-cli
install-package php7.3-common
install-package libapache2-mod-php7.3
install-package php7.3
install-package php-apcu
install-package php7.3-xml
install-package php-pear 1:1.10
install-package libxml2-dev
install-package libcurl4-openssl-dev
install-package libpcre3-dev

mkdir -p /var/log/php
chown root:www-data /var/log/php
chmod 0660 /var/log/php

replace-file-content /etc/php/7.3/apache2/php.ini "max_execution_time = 3600" "max_execution_time = 30"
replace-file-content /etc/php/7.3/apache2/php.ini "max_input_time = 3600" "max_input_time = 60"
replace-file-content /etc/php/7.3/apache2/php.ini "max_input_vars = 100000" "; max_input_vars = 1000"
replace-file-content /etc/php/7.3/apache2/php.ini "memory_limit = 4096M" "memory_limit = 128M"
add-file-content-after /etc/php/7.3/apache2/php.ini "error_log = /var/log/php/apache.log" "error_log = syslog" 1

replace-file-content /etc/php/7.3/cli/php.ini "max_execution_time = 14400" "max_execution_time = 30"
replace-file-content /etc/php/7.3/cli/php.ini "max_input_time = 14400" "max_input_time = 60"
replace-file-content /etc/php/7.3/cli/php.ini "memory_limit = 4096M" "memory_limit = -1"
add-file-content-after /etc/php/7.3/cli/php.ini "error_log = /var/log/php/cli.log" "error_log = syslog" 1

update-alternatives --set php $(which php7.3)

if [[ ! -f /.dockerenv ]]; then
  if [[ $(which apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(which nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties php version "7.3"
crudini --set /opt/install/env.properties php type "mod"
