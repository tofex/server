#!/bin/bash -e

install-package php7.3-bcmath
install-package php7.3-curl
install-package php7.3-gd
install-package php7.3-intl
install-package php7.3-mbstring
install-package php7.3-mysql
install-package php7.3-soap
install-package php7.3-xmlrpc
install-package php7.3-xsl
install-package php7.3-zip

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../module/mcrypt/1.0/install.sh"
"${currentPath}/../../../module/redis/5.1/install.sh"
"${currentPath}/../../../module/solr/2.5/install.sh"

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    echo "Restarting Apache"
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    echo "Restarting Nginx"
    service nginx restart
  fi
fi
