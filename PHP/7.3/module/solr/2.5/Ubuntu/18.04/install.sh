#!/bin/bash -e

echo "Checking curl"
test ! -e /usr/local/include/curl && ln -s /usr/include/x86_64-linux-gnu/curl /usr/local/include/curl || echo "Successful"

install-package build-essential
install-package php-pear 1:1.10
install-package libxml2-dev
install-package libcurl4-openssl-dev
install-package libpcre3-dev
install-package zlib1g-dev
install-package liblzma-dev
install-package pkg-config
install-package autogen
install-package wget

if [[ -n "$(php -m | grep -e ^solr\$ | cat)" ]]; then
  echo "PHP module solr already installed"
  exit 0
fi

cd /tmp
wget -nv https://pecl.php.net/get/solr-2.5.0.tgz
tar zxvf solr-2.5.0.tgz
cd solr-2.5.0
phpize
aclocal -I m4
autoconf
./configure
make
make install

if [[ -d "/etc/php/5.6/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/5.6/mods-available/solr.ini"
  echo "extension=solr.so" > /etc/php/5.6/mods-available/solr.ini
fi

echo "Creating configuration at: /etc/php/7.3/mods-available/solr.ini"
echo "extension=solr.so" > /etc/php/7.3/mods-available/solr.ini

if [[ -n $(which phpenmod) ]]; then
  echo "Enabling module solr"
  phpenmod solr
fi

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    echo "Restarting Apache"
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    echo "Restarting Nginx"
    service nginx restart
  fi
fi
