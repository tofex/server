#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -m  Memory usage in MB, default: 256
  -s  Memory to store string in MB, default: 16
  -v  Validate timestamps (yes/no), default: no
  -f  Maximum number of files, default: 65407
  -w  Maximum percentage of wasted memory, default: 10
  -d  Store cache also in this directory (optional)

Example: ${scriptName} -v no
EOF
}

trim()
{
  echo -n "$1" | xargs
}

memory=
stringBuffer=
validateTimestamps=
maxFiles=
maxWasted=
cacheDirectory=

while getopts hm:s:v:f:w:d:? option; do
  case ${option} in
    h) usage; exit 1;;
    m) memory=$(trim "$OPTARG");;
    s) stringBuffer=$(trim "$OPTARG");;
    v) validateTimestamps=$(trim "$OPTARG");;
    f) maxFiles=$(trim "$OPTARG");;
    w) maxWasted=$(trim "$OPTARG");;
    d) cacheDirectory=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${memory}" ]]; then
  memory="512"
fi

if [[ -z "${stringBuffer}" ]]; then
  stringBuffer="16"
fi

if [[ -z "${validateTimestamps}" ]]; then
  validateTimestamps="no"
fi

if [[ "${validateTimestamps}" == "yes" ]]; then
  validateTimestamps=1
else
  validateTimestamps=0
fi

if [[ -z "${maxFiles}" ]]; then
  maxFiles="65407"
fi

if [[ -z "${maxWasted}" ]]; then
  maxWasted="10"
fi

moduleFile=$(find /usr/lib/php/ -name opcache.so)

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  cat <<EOF | sudo tee /etc/php/7.3/apache2/conf.d/10-opcache.ini > /dev/null
; configuration for php opcache module
; priority=10
zend_extension=${moduleFile}
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=${memory}
opcache.interned_strings_buffer=${stringBuffer}
opcache.max_accelerated_files=${maxFiles}
opcache.max_wasted_percentage=${maxWasted}
opcache.validate_timestamps=${validateTimestamps}
opcache.file_cache_only=0
EOF

  if [[ -n "${cacheDirectory}" ]]; then
    mkdir -p "${cacheDirectory}"
    chown www-data:www-data "${cacheDirectory}"
    cat <<EOF | sudo tee -a /etc/php/7.3/apache2/conf.d/10-opcache.ini > /dev/null
opcache.file_cache=${cacheDirectory}
EOF
  fi

  sudo service apache2 restart
fi
