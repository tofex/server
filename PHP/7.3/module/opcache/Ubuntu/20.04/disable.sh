#!/bin/bash -e

moduleFile=$(find /usr/lib/php/ -name opcache.so)

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  cat <<EOF | sudo tee /etc/php/7.3/apache2/conf.d/10-opcache.ini > /dev/null
; configuration for php opcache module
; priority=10
zend_extension=${moduleFile}
opcache.enable=0
EOF

  sudo service apache2 restart
fi
