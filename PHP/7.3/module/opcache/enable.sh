#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -m  Memory usage in MB, default: 256
  -s  Memory to store string in MB, default: 16
  -v  Validate timestamps (yes/no), default: no
  -f  Maximum number of files, default: 65407
  -w  Maximum percentage of wasted memory, default: 10
  -d  Store cache also in this directory (optional)

Example: ${scriptName} -v no
EOF
}

trim()
{
  echo -n "$1" | xargs
}

memory=
stringBuffer=
validateTimestamps=
maxFiles=
maxWasted=
cacheDirectory=

while getopts hm:s:v:f:w:d:? option; do
  case ${option} in
    h) usage; exit 1;;
    m) memory=$(trim "$OPTARG");;
    s) stringBuffer=$(trim "$OPTARG");;
    v) validateTimestamps=$(trim "$OPTARG");;
    f) maxFiles=$(trim "$OPTARG");;
    w) maxWasted=$(trim "$OPTARG");;
    d) cacheDirectory=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${memory}" ]]; then
  memory="512"
fi

if [[ -z "${stringBuffer}" ]]; then
  stringBuffer="16"
fi

if [[ -z "${validateTimestamps}" ]]; then
  validateTimestamps="no"
fi

if [[ "${validateTimestamps}" != "yes" ]]; then
  validateTimestamps="no"
fi

if [[ -z "${maxFiles}" ]]; then
  maxFiles="65407"
fi

if [[ -z "${maxWasted}" ]]; then
  maxWasted="10"
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

if [[ -n "${cacheDirectory}" ]]; then
  "${distributionReleaseScript}" \
    -m "${memory}" \
    -s "${stringBuffer}" \
    -v "${validateTimestamps}" \
    -f "${maxFiles}" \
    -w "${maxWasted}" \
    -d "${cacheDirectory}"
else
  "${distributionReleaseScript}" \
    -m "${memory}" \
    -s "${stringBuffer}" \
    -v "${validateTimestamps}" \
    -f "${maxFiles}" \
    -w "${maxWasted}"
fi
