#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

install-package build-essential
install-package php7.3-dev
install-package php-pear 1:1.10

echo "no" | install-pecl-package "redis" 5.1.1

if [[ -d "/etc/php/5.6/mods-available" ]]; then
  echo "Creating configuration at: /etc/php/5.6/mods-available/redis.ini"
  echo "extension=redis.so" > /etc/php/5.6/mods-available/redis.ini
fi

echo "Creating configuration at: /etc/php/7.3/mods-available/redis.ini"
echo "extension=redis.so" > /etc/php/7.3/mods-available/redis.ini

if [[ -n $(which phpenmod) ]]; then
  echo "Enabling module redis"
  phpenmod redis
fi

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    echo "Restarting Apache"
    service apache2 restart
    sleep 5
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    echo "Restarting Nginx"
    service nginx restart
  fi
fi
