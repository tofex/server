#!/bin/bash -e

install-package php8.2-bcmath
install-package php8.2-curl
install-package php8.2-gd
install-package php8.2-intl
install-package php8.2-mbstring
install-package php8.2-mysql
install-package php8.2-soap
install-package php8.2-xmlrpc
install-package php8.2-xsl
install-package php8.2-zip

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../module/mcrypt/1.0/install.sh"
"${currentPath}/../../../module/redis/5.3/install.sh"
"${currentPath}/../../../module/solr/2.6/install.sh"

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
