#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

install-package build-essential
install-package php8.2-dev
install-package php-pear 1:1.10

echo "no" | install-pecl-package "redis" 5.3.7

echo "Creating configuration at: /etc/php/8.2/mods-available/redis.ini"
echo "extension=redis.so" > /etc/php/8.2/mods-available/redis.ini

if [[ -n $(which phpenmod) ]]; then
  echo "Enabling module redis"
  phpenmod redis
fi

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
