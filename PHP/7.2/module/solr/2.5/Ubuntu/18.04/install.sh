#!/bin/bash -e

test ! -e /usr/local/include/curl && ln -s /usr/include/x86_64-linux-gnu/curl /usr/local/include/curl || echo "Successful"

install-package build-essential
install-package php-pear 1:1.10
install-package libxml2-dev
install-package libcurl4-openssl-dev
install-package libpcre3-dev
install-package zlib1g-dev
install-package liblzma-dev

install-pecl-package "solr" 2.5.0

if [[ -d "/etc/php/5.6/mods-available" ]]; then
  echo "extension=solr.so" > /etc/php/5.6/mods-available/solr.ini
fi

if [[ -d "/etc/php/7.0/mods-available" ]]; then
  echo "extension=solr.so" > /etc/php/7.0/mods-available/solr.ini
fi

if [[ -d "/etc/php/7.1/mods-available" ]]; then
  echo "extension=solr.so" > /etc/php/7.1/mods-available/solr.ini
fi

if [[ -d "/etc/php/7.2/mods-available" ]]; then
  echo "extension=solr.so" > /etc/php/7.2/mods-available/solr.ini
fi

if [[ -d "/etc/php/7.3/mods-available" ]]; then
  echo "extension=solr.so" > /etc/php/7.3/mods-available/solr.ini
fi

if [[ -d "/etc/php/7.4/mods-available" ]]; then
  echo "extension=solr.so" > /etc/php/7.4/mods-available/solr.ini
fi

phpenmod solr

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
    sleep 5
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
