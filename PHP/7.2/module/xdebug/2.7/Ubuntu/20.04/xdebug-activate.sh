#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  Remote host

Example: ${scriptName} -r 10.0.2.2
EOF
}

trim()
{
  echo -n "$1" | xargs
}

remoteHost=

while getopts hr:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) remoteHost=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${remoteHost}" ]]; then
  echo "No remote host specified!"
  exit 1
fi

moduleFile=$(find /usr/lib/php/ -name xdebug.so)
cat <<EOF | tee /usr/local/bin/xdebug-activate > /dev/null
#!/bin/bash
cat <<EOFXA | sudo tee /etc/php/7.2/mods-available/xdebug.ini > /dev/null
zend_extension=${moduleFile}
xdebug.max_nesting_level=512
xdebug.remote_enable=1
xdebug.remote_autostart=1
xdebug.remote_connect_back=0
xdebug.remote_host=${remoteHost}
xdebug.discover_client_host=0
xdebug.client_host=${remoteHost}
xdebug.profiler_enable=0
xdebug.profiler_enable_trigger=1
xdebug.profiler_output_dir=/var/xdebug
EOFXA
sudo service apache2 reload
export PHP_IDE_CONFIG="serverName=cli"
EOF

mkdir -p /var/xdebug
chmod 0777 /var/xdebug

chmod +x /usr/local/bin/xdebug-activate

if [[ ! -f /usr/local/bin/xa ]]; then
  ln -s /usr/local/bin/xdebug-activate /usr/local/bin/xa | cat
fi
