#!/bin/bash -e

install-package build-essential
install-package php7.2-dev
install-package php-pear 1:1.10

install-pecl-package "igbinary" 2.0.5

if [[ -d "/etc/php/5.6/mods-available" ]]; then
    echo "extension=igbinary.so" > /etc/php/5.6/mods-available/igbinary.ini
fi

echo "extension=igbinary.so" > /etc/php/7.2/mods-available/igbinary.ini

phpenmod igbinary

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
    sleep 5
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
