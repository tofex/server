#!/bin/bash -e

install-package build-essential
install-package php7.2-dev
install-package php-pear 1:1.10
install-package imagemagick
install-package libmagickwand-dev

echo "\n" | install-pecl-package "imagick" 3.4.3

if [[ -d "/etc/php/5.6/mods-available" ]]; then
    echo "extension=imagick.so" > /etc/php/5.6/mods-available/imagick.ini
fi

echo "extension=imagick.so" > /etc/php/7.2/mods-available/imagick.ini

phpenmod imagick

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
    sleep 5
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
