#!/bin/bash -e

install-package build-essential
install-package php7.4-dev
install-package php-pear 1:1.10
install-package libmcrypt-dev

install-pecl-package "mcrypt" 1.0.2

if [[ -d "/etc/php/5.6/mods-available" ]]; then
    echo "extension=mcrypt.so" > /etc/php/5.6/mods-available/mcrypt.ini
fi

echo "extension=mcrypt.so" > /etc/php/7.4/mods-available/mcrypt.ini

phpenmod mcrypt

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
    sleep 5
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
