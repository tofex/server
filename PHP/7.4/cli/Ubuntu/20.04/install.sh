#!/bin/bash -e

install-package python3-software-properties
add-ppa-repository ppa:ondrej/php
install-package libc6 2.31-0ubuntu9.7
install-package php7.4-dev
install-package php7.4-cli
install-package php7.4-xml
install-package php-pear 1:1.10
install-package libxml2-dev
install-package libcurl4-openssl-dev
install-package libpcre3-dev

mkdir -p /var/log/php
chown root:www-data /var/log/php
chmod 0660 /var/log/php

replace-file-content /etc/php/7.4/cli/php.ini "max_execution_time = 14400" "max_execution_time = 30"
replace-file-content /etc/php/7.4/cli/php.ini "max_input_time = 14400" "max_input_time = 60"
replace-file-content /etc/php/7.4/cli/php.ini "memory_limit = 4096M" "memory_limit = -1"
add-file-content-after /etc/php/7.4/cli/php.ini "error_log = /var/log/php/cli.log" "error_log = syslog" 1

update-alternatives --set php "$(which php7.4)"

mkdir -p /opt/install/
crudini --set /opt/install/env.properties php version "7.4"
crudini --set /opt/install/env.properties php type "cli"
