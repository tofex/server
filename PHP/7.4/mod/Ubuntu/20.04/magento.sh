#!/bin/bash -e

install-package php7.4-bcmath
install-package php7.4-curl
install-package php7.4-gd
install-package php7.4-intl
install-package php7.4-mbstring
install-package php7.4-mysql
install-package php7.4-soap
install-package php7.4-xmlrpc
install-package php7.4-xsl
install-package php7.4-zip

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../module/mcrypt/1.0/install.sh"
"${currentPath}/../../../module/redis/5.1/install.sh"
"${currentPath}/../../../module/solr/2.5/install.sh"

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
