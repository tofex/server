#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  Remote host

Example: ${scriptName} -r 10.0.2.2
EOF
}

trim()
{
  echo -n "$1" | xargs
}

remoteHost=

while getopts hr:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) remoteHost=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${remoteHost}" ]]; then
  echo "No remote host specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" -r "${remoteHost}" -r "${remoteHost}"
