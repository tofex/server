#!/bin/bash -e

install-package php5.6-dev
install-package php-pear 1:1.10

install-pecl-package "igbinary" 2.0.5

echo "extension=igbinary.so" > /etc/php/5.6/mods-available/igbinary.ini

phpenmod igbinary

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
  sleep 5
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
