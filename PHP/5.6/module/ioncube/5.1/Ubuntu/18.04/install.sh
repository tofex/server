#!/bin/bash -e

rm -rf /tmp/ioncube
mkdir -p /tmp/ioncube
cd /tmp/ioncube
wget -nv http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64_5.1.2.tar.gz
tar xfz ioncube_loaders_lin_x86-64_5.1.2.tar.gz
cp ioncube/ioncube_loader_lin_5.6.so /usr/lib/php/20131226
chmod 0644 /usr/lib/php/20131226/ioncube_loader_lin_5.6.so
echo "; priority=01" > /etc/php/5.6/mods-available/ioncube.ini
echo "zend_extension = /usr/lib/php/20131226/ioncube_loader_lin_5.6.so" >> /etc/php/5.6/mods-available/ioncube.ini
test ! -f /etc/php/5.6/apache2/conf.d/01-ioncube.ini && ln -s /etc/php/5.6/mods-available/ioncube.ini /etc/php/5.6/apache2/conf.d/01-ioncube.ini
test ! -f /etc/php/5.6/cli/conf.d/01-ioncube.ini && ln -s /etc/php/5.6/mods-available/ioncube.ini /etc/php/5.6/cli/conf.d/01-ioncube.ini

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
