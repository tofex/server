#!/bin/bash -e

install-package php5.6-dev
install-package php-pear 1:1.10

install-pecl-package "oauth" 1.2.3

echo "extension=oauth.so" > /etc/php/5.6/mods-available/oauth.ini

phpenmod oauth

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
