#!/bin/bash -e

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  cat <<EOF | sudo tee /etc/php/5.6/apache2/conf.d/10-opcache.ini > /dev/null
; configuration for php opcache module
; priority=10
zend_extension=opcache.so
opcache.enable=0
EOF

  sudo service apache2 restart
fi
