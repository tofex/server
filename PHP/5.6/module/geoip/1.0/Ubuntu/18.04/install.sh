#!/bin/bash -e

install-package php5.6-dev
install-package php-pear 1:1.10
install-package libgeoip-dev

install-pecl-package "geoip" 1.0.8

echo "extension=geoip.so" > /etc/php/5.6/mods-available/geoip.ini

phpenmod geoip

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
  sleep 5
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
