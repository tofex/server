#!/bin/bash -e

install-package build-essential
install-package php5.6-dev
install-package php-pear 1:1.10

echo "no" | install-pecl-package "redis" 3.1.4

echo "extension=redis.so" > /etc/php/5.6/mods-available/redis.ini

phpenmod redis

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
