#!/bin/bash -e

install-package php5.6-bcmath
install-package php5.6-curl
install-package php5.6-gd
install-package php5.6-intl
install-package php5.6-mbstring
install-package php5.6-mcrypt
install-package php5.6-mysql
install-package php5.6-soap
install-package php5.6-xmlrpc
install-package php5.6-xsl
install-package php5.6-zip

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../module/redis/3.1/install.sh"

if [[ ! -f /.dockerenv ]]; then
  if [[ $(which apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(which nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
