#!/bin/bash -e

replace-file-content /etc/php/5.6/apache2/php.ini "short_open_tag = Off" "short_open_tag = On"
replace-file-content /etc/php/5.6/cli/php.ini "short_open_tag = Off" "short_open_tag = On"
