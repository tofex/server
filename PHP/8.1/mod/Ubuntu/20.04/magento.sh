#!/bin/bash -e

install-package php8.1-bcmath
install-package php8.1-curl
install-package php8.1-gd
install-package php8.1-intl
install-package php8.1-mbstring
install-package php8.1-mysql
install-package php8.1-soap
install-package php8.1-xmlrpc
install-package php8.1-xsl
install-package php8.1-zip

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${currentPath}/../../../module/mcrypt/1.0/install.sh"
"${currentPath}/../../../module/redis/5.3/install.sh"
"${currentPath}/../../../module/solr/2.6/install.sh"

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
