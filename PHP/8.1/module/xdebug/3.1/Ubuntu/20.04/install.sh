#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  Remote host

Example: ${scriptName} -r 10.0.2.2
EOF
}

trim()
{
  echo -n "$1" | xargs
}

remoteHost=

while getopts hr:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) remoteHost=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${remoteHost}" ]]; then
  echo "No remote host specified!"
  exit 1
fi

install-package build-essential
install-package php-xml
install-package php8.1-xml

install-pecl-package xdebug 3.1.5

echo ";zend_extension=$(find /usr/lib/php/ -name xdebug.so)" > /etc/php/8.1/mods-available/xdebug.ini
echo ";xdebug.max_nesting_level=512" >> /etc/php/8.1/mods-available/xdebug.ini
echo ";xdebug.remote_enable=1" >> /etc/php/8.1/mods-available/xdebug.ini
echo ";xdebug.remote_autostart=1" >> /etc/php/8.1/mods-available/xdebug.ini
if [[ -z "${remoteHost}" ]]; then
  echo ";xdebug.remote_connect_back=1" >> /etc/php/8.1/mods-available/xdebug.ini
else
  echo ";xdebug.remote_connect_back=0" >> /etc/php/8.1/mods-available/xdebug.ini
  echo ";xdebug.remote_host=${remoteHost}" >> /etc/php/8.1/mods-available/xdebug.ini
  echo ";xdebug.discover_client_host=0" >> /etc/php/8.1/mods-available/xdebug.ini
  echo ";xdebug.client_host=${remoteHost}" >> /etc/php/8.1/mods-available/xdebug.ini
fi
echo ";xdebug.profiler_enable=0" >> /etc/php/8.1/mods-available/xdebug.ini
echo ";xdebug.profiler_enable_trigger=1" >> /etc/php/8.1/mods-available/xdebug.ini
echo ";xdebug.profiler_output_dir=/var/xdebug" >> /etc/php/8.1/mods-available/xdebug.ini

mkdir -p /var/xdebug
chown www-data: /var/xdebug

phpenmod xdebug

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
