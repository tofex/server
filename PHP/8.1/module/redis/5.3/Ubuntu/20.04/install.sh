#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

install-package build-essential
install-package php8.1-dev
install-package php-pear 1:1.10

echo "no" | install-pecl-package "redis" 5.3.7

echo "Creating configuration at: /etc/php/8.1/mods-available/redis.ini"
echo "extension=redis.so" > /etc/php/8.1/mods-available/redis.ini

phpenmod redis

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
