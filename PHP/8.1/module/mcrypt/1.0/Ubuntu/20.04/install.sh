#!/bin/bash -e

install-package build-essential
install-package php8.1-dev
install-package php-pear 1:1.10
install-package libmcrypt-dev

install-pecl-package "mcrypt" 1.0.5

echo "Creating configuration at: /etc/php/8.1/mods-available/mcrypt.ini"
echo "extension=mcrypt.so" > /etc/php/8.1/mods-available/mcrypt.ini

phpenmod mcrypt

if [[ ! -f /.dockerenv ]]; then
  if [[ $(get-installed-package-version apache2 | wc -l) -gt 0 ]]; then
    service apache2 restart
    sleep 5
  fi

  if [[ $(get-installed-package-version nginx | wc -l) -gt 0 ]]; then
    service nginx restart
  fi
fi
