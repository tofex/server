#!/bin/bash -e

cd /usr/local/include
ln -s /usr/include/x86_64-linux-gnu/curl curl

install-package build-essential
install-package icu-devtools 60.2
install-package libicu-dev 60.2
install-package libxml2-dev
install-package libxslt1-dev
install-package apache2-dev
install-package libjpeg-dev
install-package libpng-dev
install-package libxpm-dev
install-package libmysqlclient-dev
install-package libpq-dev
install-package libfreetype6-dev
install-package libldb-dev
install-package libcurl3
install-package libssl1.0-dev
install-package libcurl-openssl1.0-dev
install-package libmcrypt-dev

ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so
ln -s /usr/lib/x86_64-linux-gnu/liblber.so /usr/lib/liblber.so

cd /tmp
rm -rf curl-7.55.1/
wget -nv https://curl.se/download/curl-7.55.1.tar.gz
tar xzf curl-7.55.1.tar.gz
cd curl-7.55.1/
./configure --with-ssl
make
make install

mkdir -p /usr/local/php/etc/ini

cd /tmp
rm -rf php-5.4.16
wget -nv http://museum.php.net/php5/php-5.4.16.tar.gz
tar -xzf php-5.4.16.tar.gz
cd php-5.4.16/
./configure --prefix=/usr/local/php --with-apxs2 --enable-mbstring --with-curl --with-openssl --with-xmlrpc --enable-soap --enable-zip --with-gd --with-jpeg-dir --with-png-dir --with-mysqli --with-pgsql --enable-embedded-mysqli --with-freetype-dir --with-ldap --enable-intl --with-xsl --with-zlib --with-bcmath --with-mcrypt --with-pdo_mysql --with-config-file-scan-dir=/usr/local/php/etc/ini
make
make install
cp php.ini-production /usr/local/php/lib/php.ini

replace-file-content /usr/local/php/lib/php.ini "max_execution_time = 3600" "max_execution_time = 30"
replace-file-content /usr/local/php/lib/php.ini "max_input_time = 3600" "max_input_time = 60"
replace-file-content /usr/local/php/lib/php.ini "max_input_vars = 100000" "; max_input_vars = 1000"
replace-file-content /usr/local/php/lib/php.ini "memory_limit = 4096M" "memory_limit = 128M"
add-file-content-after /usr/local/php/lib/php.ini "error_log = /var/log/php/apache.log" "error_log = syslog" 1

ln -s /usr/local/php/bin/pear /usr/local/bin/pear
ln -s /usr/local/php/bin/peardev /usr/local/bin/peardev
ln -s /usr/local/php/bin/pecl /usr/local/bin/pecl
ln -s /usr/local/php/bin/php /usr/local/bin/php
ln -s /usr/local/php/bin/php-cgi /usr/local/bin/php-cgi
ln -s /usr/local/php/bin/php-config /usr/local/bin/php-config
ln -s /usr/local/php/bin/phpize /usr/local/bin/phpize

update-alternatives --install /usr/local/bin/php php /usr/local/php/bin/php 50

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  a2dismod mpm_worker
  a2dismod mpm_event
  a2enmod mpm_prefork
  a2enmod php5

  cat <<EOF | sudo tee "/etc/apache2/mods-enabled/php5.conf" > /dev/null
<FilesMatch ".+\.ph(ar|p|tml)\$">
    SetHandler application/x-httpd-php
</FilesMatch>
<FilesMatch ".+\.phps\$">
    SetHandler application/x-httpd-php-source
    # Deny access to raw php sources by default
    # To re-enable it's recommended to enable access to the files
    # only in specific virtual host or directory
    Require all denied
</FilesMatch>
# Deny access to files without filename (e.g. '.php')
<FilesMatch "^\.ph(ar|p|ps|tml)\$">
    Require all denied
</FilesMatch>
EOF

  service apache2 restart
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties php version "5.4"
crudini --set /opt/install/env.properties php type "mod"
