#!/bin/bash -e

replace-file-content /usr/local/php/lib/php.ini "short_open_tag = Off" "short_open_tag = On"
