#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -r  Remote host

Example: ${scriptName} -r 10.0.2.2
EOF
}

trim()
{
  echo -n "$1" | xargs
}

remoteHost=

while getopts hr:? option; do
  case ${option} in
    h) usage; exit 1;;
    r) remoteHost=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${remoteHost}" ]]; then
  echo "No remote host specified!"
  exit 1
fi

install-pecl-package xdebug 2.3.3

add-file-content-before /usr/local/php/lib/php.ini ";zend_extension=$(find /usr/local/php/lib/php/ -name xdebug.so)" ";extension=php_bz2.dll" 1

mkdir -p /var/xdebug
chown www-data: /var/xdebug

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
