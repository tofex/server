#!/bin/bash -e

rm -rf /tmp/ioncube
mkdir -p /tmp/ioncube
cd /tmp/ioncube
wget -nv http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64_5.1.2.tar.gz
tar xfz ioncube_loaders_lin_x86-64_5.1.2.tar.gz
cp ioncube/ioncube_loader_lin_5.4.so /usr/local/php/lib/php/
chmod 0644 /usr/local/php/lib/php/ioncube_loader_lin_5.4.so

add-file-content-before /usr/local/php/lib/php.ini "zend_extension=/usr/local/php/lib/php/ioncube_loader_lin_5.4.so" "zend_extension=" 1

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
