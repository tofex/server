#!/bin/bash -e

install-package build-essential

echo "no" | install-pecl-package "redis" 3.1.4

add-file-content-before /usr/local/php/lib/php.ini "extension=redis.so" ";extension=php_bz2.dll" 1

if [[ $(which apache2 | wc -l) -gt 0 ]]; then
  service apache2 restart
fi

if [[ $(which nginx | wc -l) -gt 0 ]]; then
  service nginx restart
fi
