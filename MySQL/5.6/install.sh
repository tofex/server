#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF

usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host, default: localhost
  -p  Port, default: 3306
  -s  Password
  -b  Bind address, default: 0.0.0.0
  -i  Server Id, default: 1

Example: ${scriptName} -s secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootPassword=
bindAddress="0.0.0.0"
serverId="1"

while getopts hn:p:s:b:i:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    b) bindAddress=$(trim "$OPTARG");;
    i) serverId=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root port specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${bindAddress}" ]]; then
  echo "No bind address specified!"
  exit 1
fi

if [[ -z "${serverId}" ]]; then
  echo "No server id specified!"
  exit 1
fi

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"
distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" \
  -n "${databaseRootHost}" \
  -p "${databaseRootPort}" \
  -s "${databaseRootPassword}" \
  -b "${bindAddress}" \
  -i "${serverId}"
