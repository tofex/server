#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Server host name, default: localhost
  -p  Server port, default: 3306
  -s  User password
  -b  Bind address, default: 0.0.0.0
  -i  Server Id, default: 1

Example: ${scriptName} -s secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootPassword=
bindAddress="0.0.0.0"
serverId="1"

while getopts hn:p:s:b:i:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    b) bindAddress=$(trim "$OPTARG");;
    i) serverId=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root port specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root password specified!"
  exit 1
fi

if [[ -z "${bindAddress}" ]]; then
  echo "No bind address specified!"
  exit 1
fi

if [[ -z "${serverId}" ]]; then
  echo "No server id specified!"
  exit 1
fi

export DEBIAN_FRONTEND=noninteractive

echo "Downloading libraries"
install-package libaio1
install-package libnuma1
install-package-from-deb mysql-common 5.6.39-1ubuntu14.04 https://repo.mysql.com/apt/pool/mysql-5.6/m/mysql-community/mysql-common_5.6.39-1ubuntu14.04_amd64.deb
install-package-from-deb mysql-community-client 5.6.39-1ubuntu14.04 https://repo.mysql.com/apt/pool/mysql-5.6/m/mysql-community/mysql-community-client_5.6.39-1ubuntu14.04_amd64.deb
install-package-from-deb mysql-client 5.6.39-1ubuntu14.04 https://repo.mysql.com/apt/pool/mysql-5.6/m/mysql-community/mysql-client_5.6.39-1ubuntu14.04_amd64.deb
install-package-from-deb mysql-community-server 5.6.39-1ubuntu14.04 https://repo.mysql.com/apt/pool/mysql-5.6/m/mysql-community/mysql-community-server_5.6.39-1ubuntu14.04_amd64.deb
install-package-from-deb mysql-server 5.6.39-1ubuntu14.04 https://repo.mysql.com/apt/pool/mysql-5.6/m/mysql-community/mysql-server_5.6.39-1ubuntu14.04_amd64.deb

echo "Adding user: root"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e 'use mysql;' >/dev/null 2>&1 && mysqladmin -h"${databaseRootHost}" -P"${databaseRootPort}" -u root password "${databaseRootPassword}" >/dev/null 2>&1

export MYSQL_PWD="${databaseRootPassword}"

echo "Granting super rights to user: 'root'@'%'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT USAGE ON *.* TO 'root'@'%' IDENTIFIED BY '${databaseRootPassword}';";

echo "Granting all privileges to user: 'root'@'%'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';"

echo "Granting super rights to user: 'root'@'127.0.0.1'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT USAGE ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY '${databaseRootPassword}';";

echo "Granting all privileges to user: 'root'@'127.0.0.1'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1';"

echo "Flushing privileges"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e "FLUSH PRIVILEGES;"

add-file-content-before /etc/security/limits.conf "mysql  soft  nofile  65535" "# End of file" 1
add-file-content-before /etc/security/limits.conf "mysql  hard  nofile  65535" "# End of file" 1
sysctl -p

service mysql restart

mkdir -p /opt/install/
crudini --set /opt/install/env.properties mysql type "mysql"
crudini --set /opt/install/env.properties mysql version "5.6"
crudini --set /opt/install/env.properties mysql port "${databaseRootPort}"
crudini --set /opt/install/env.properties mysql rootUser "root"
crudini --set /opt/install/env.properties mysql rootPassword "${databaseRootPassword}"
