#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Root host, default: localhost
  -p  Root port, default: 3306
  -u  Root user, default: root
  -s  Root password
  -o  Project host, default: localhost
  -r  Project port, default: 3306
  -e  Project user
  -w  Project password
  -b  Project database name
  -g  Grant project user super rights, default: false

Example: ${scriptName} -u root -s secret -e project -w secret2 -b project_db -g
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootUser="root"
databaseRootPassword=
databaseProjectHost="localhost"
databaseProjectPort="3306"
databaseProjectUser=
databaseProjectPassword=
databaseProjectDatabaseName=
databaseProjectGrantSuperRights="no"

while getopts hn:p:u:s:o:r:e:w:b:g? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    u) databaseRootUser=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    o) databaseProjectHost=$(trim "$OPTARG");;
    r) databaseProjectPort=$(trim "$OPTARG");;
    e) databaseProjectUser=$(trim "$OPTARG");;
    w) databaseProjectPassword=$(trim "$OPTARG");;
    b) databaseProjectDatabaseName=$(trim "$OPTARG");;
    g) databaseProjectGrantSuperRights="yes";;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootUser}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${databaseProjectHost}" ]]; then
  echo "No database project host specified!"
  exit 1
fi

if [[ -z "${databaseProjectPort}" ]]; then
  echo "No database project port specified!"
  exit 1
fi

if [[ -z "${databaseProjectUser}" ]]; then
  echo "No database project user specified!"
  exit 1
fi

if [[ -z "${databaseProjectPassword}" ]]; then
  echo "No database project password specified!"
  exit 1
fi

if [[ -z "${databaseProjectDatabaseName}" ]]; then
  echo "No database project database name specified!"
  exit 1
fi

if [[ -z "${databaseProjectGrantSuperRights}" ]]; then
  echo "No database project grant super rights specified!"
  exit 1
fi

export MYSQL_PWD="${databaseRootPassword}"

echo "Adding user: '${databaseProjectUser}'@'localhost'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT ALL ON ${databaseProjectDatabaseName}.* TO '${databaseProjectUser}'@'localhost' identified by '${databaseProjectPassword}' WITH GRANT OPTION;"

if [[ "${databaseProjectGrantSuperRights}" == "yes" ]]; then
    echo "Granting super rights to user: '${databaseProjectUser}'@'localhost'"
    mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT SUPER ON *.* TO '${databaseProjectUser}'@'localhost';"
fi

echo "Adding user: '${databaseProjectUser}'@'%'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT ALL ON ${databaseProjectDatabaseName}.* TO '${databaseProjectUser}'@'%' identified by '${databaseProjectPassword}' WITH GRANT OPTION;"

if [[ "${databaseProjectGrantSuperRights}" == "yes" ]]; then
    echo "Granting super rights to user: '${databaseProjectUser}'@'%'"
    mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT SUPER ON *.* TO '${databaseProjectUser}'@'%';"
fi

echo "Adding user: '${databaseProjectUser}'@'${databaseProjectHost}'"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT ALL ON ${databaseProjectDatabaseName}.* TO '${databaseProjectUser}'@'${databaseProjectHost}' identified by '${databaseProjectPassword}' WITH GRANT OPTION;"

if [[ "${databaseProjectGrantSuperRights}" == "yes" ]]; then
    echo "Granting super rights to user: '${databaseProjectUser}'@'${databaseProjectHost}'"
    mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT SUPER ON *.* TO '${databaseProjectUser}'@'${databaseProjectHost}';"
fi

echo "Flushing privileges"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "FLUSH PRIVILEGES;"

export MYSQL_PWD="${databaseProjectPassword}"

echo "Dropping database: ${databaseProjectDatabaseName}"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseProjectUser}" -e "DROP DATABASE IF EXISTS ${databaseProjectDatabaseName};"

echo "Creating database: ${databaseProjectDatabaseName}"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseProjectUser}" -e "CREATE DATABASE ${databaseProjectDatabaseName} CHARACTER SET utf8 COLLATE utf8_general_ci;";
