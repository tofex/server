#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -s  Current root password
  -p  New root password

Example: ${scriptName} -s old_secret -p new_secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseCurrentRootPassword=
databaseNewRootPassword=

while getopts hs:p:? option; do
  case ${option} in
    h) usage; exit 1;;
    s) databaseCurrentRootPassword=$(trim "$OPTARG");;
    p) databaseNewRootPassword=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseCurrentRootPassword}" ]]; then
  echo "No database root password specified!"
  exit 1
fi

if [[ -z "${databaseNewRootPassword}" ]]; then
  echo "No new database root password specified!"
  exit 1
fi

export MYSQL_PWD="${databaseCurrentRootPassword}"

hosts=( $(mysql -u root "mysql" -e "select host from user where user = 'root';" --skip-column-names --batch) )

update=
for host in "${hosts[@]}"; do
  echo "Changing password of user: 'root'@'${host}'"
  update="${update} ALTER USER 'root'@'${host}' IDENTIFIED WITH mysql_native_password BY '${databaseNewRootPassword}';"
done
update="${update} FLUSH PRIVILEGES;"

mysql -u root -e "${update}"
