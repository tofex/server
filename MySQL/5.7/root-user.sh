#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Server host name, default: localhost
  -p  Server port, default: 3306
  -u  User name, default: root
  -s  User password, default: generated

Example: ${scriptName} -s secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootUser="root"
databaseRootPassword=

while getopts hn:p:u:s:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    u) databaseRootUser=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootUser}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  databaseRootPassword=$(echo "${RANDOM}" | md5sum | head -c 32)
  echo "Using generated password: ${databaseRootPassword}"
fi

if [[ -z "${databaseRootPassword}" ]]; then
  echo "No database root user specified!"
  exit 1
fi

export MYSQL_PWD="${databaseRootPassword}"

rootUserNames=( "'${databaseRootUser}'@'%'" "'${databaseRootUser}'@'127.0.0.1'" "'${databaseRootUser}'@'localhost'" )

for rootUserName in "${rootUserNames[@]}"; do
  echo "Granting super rights to user: ${rootUserName}"
  mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT USAGE ON *.* TO ${rootUserName} IDENTIFIED BY '${databaseRootPassword}';";

  echo "Granting all privileges to user: ${rootUserName}"
  mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "GRANT ALL PRIVILEGES ON *.* TO ${rootUserName} WITH GRANT OPTION;"

  echo "Allowing login with password to user: ${rootUserName}"
  mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "ALTER USER ${rootUserName} IDENTIFIED WITH mysql_native_password BY '${databaseRootPassword}';"
done

echo "Flushing privileges"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u"${databaseRootUser}" -e "FLUSH PRIVILEGES;"
