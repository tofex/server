#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF

usage: ${scriptName} options

OPTIONS:
  --help                  Show this message
  --bindAddress           Bind address, default: 127.0.0.1
  --databasePort          Port, default: 3306
  --serverId              Server Id, default: 1
  --connections           Connections, default: 100
  --innodbBufferPoolSize  InnoDB buffer pool size in GB, default: 4
  --innodbFilePerTable    Flag to use a file per table (on/off), default: off

Example: ${scriptName} --bindAddress 0.0.0.0 --databasePort 3306 --serverId 1 --connections 100 --innodbBufferPoolSize 4
EOF
}

bindAddress=
databasePort=
serverId=
connections=
innodbBufferPoolSize=

source "${currentPath}/../../prepare-parameters.sh"

if [[ -z "${bindAddress}" ]]; then
  bindAddress="127.0.0.1"
fi

if [[ -z "${databasePort}" ]]; then
  databasePort="3306"
fi

if [[ -z "${serverId}" ]]; then
  serverId="1"
fi

if [[ -z "${connections}" ]]; then
  connections="100"
fi

if [[ -z "${innodbBufferPoolSize}" ]]; then
  innodbBufferPoolSize="4"
fi

if [[ -z "${innodbFilePerTable}" ]]; then
  innodbFilePerTable="OFF"
fi

install-package lsb-release

distribution=$(lsb_release -i | awk '{print $3}')
release=$(lsb_release -r | awk '{print $2}')

distributionReleaseScript="${currentPath}/${distribution}/${release}/${scriptName}"

if [[ ! -f "${distributionReleaseScript}" ]]; then
  echo "Missing script: ${distributionReleaseScript}"
  exit 1
fi

"${distributionReleaseScript}" \
  --bindAddress "${bindAddress}" \
  --databasePort "${databasePort}" \
  --serverId "${serverId}" \
  --connections "${connections}" \
  --innodbBufferPoolSize "${innodbBufferPoolSize}" \
  --innodbFilePerTable "${innodbFilePerTable}"
