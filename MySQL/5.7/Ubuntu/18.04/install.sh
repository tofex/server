#!/bin/bash -e

currentPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
scriptName="${0##*/}"

usage()
{
cat >&2 << EOF

usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Server host name, default: localhost
  -p  Server port, default: 3306
  -s  User password, default: generated
  -b  Bind address, default: 0.0.0.0
  -i  Server Id, default: 1

Example: ${scriptName} -u mysql -s secret
EOF
}

trim()
{
  echo -n "$1" | xargs
}

databaseRootHost="localhost"
databaseRootPort="3306"
databaseRootPassword=
bindAddress="0.0.0.0"
serverId="1"

while getopts hn:p:s:b:i:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) databaseRootHost=$(trim "$OPTARG");;
    p) databaseRootPort=$(trim "$OPTARG");;
    s) databaseRootPassword=$(trim "$OPTARG");;
    b) bindAddress=$(trim "$OPTARG");;
    i) serverId=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${databaseRootHost}" ]]; then
  echo "No database root host specified!"
  exit 1
fi

if [[ -z "${databaseRootPort}" ]]; then
  echo "No database root port specified!"
  exit 1
fi

if [[ -z "${databaseRootPassword}" ]]; then
  databaseRootPassword=$(echo "${RANDOM}" | md5sum | head -c 32)
  echo "Using generated password: ${databaseRootPassword}"
fi

if [[ -z "${bindAddress}" ]]; then
  echo "No bind address specified!"
  exit 1
fi

if [[ -z "${serverId}" ]]; then
  echo "No server id specified!"
  exit 1
fi

export DEBIAN_FRONTEND=noninteractive

echo "Downloading libraries"
install-package libaio1
install-package libnuma1
install-package libmecab2
install-package-from-deb mysql-common 5.7.21-1ubuntu17.10 https://repo.mysql.com/apt/pool/mysql-5.7/m/mysql-community/mysql-common_5.7.21-1ubuntu17.10_amd64.deb
install-package-from-deb mysql-community-client 5.7.21-1ubuntu17.10 https://repo.mysql.com/apt/pool/mysql-5.7/m/mysql-community/mysql-community-client_5.7.21-1ubuntu17.10_amd64.deb
install-package-from-deb mysql-client 5.7.21-1ubuntu17.10 https://repo.mysql.com/apt/pool/mysql-5.7/m/mysql-community/mysql-client_5.7.21-1ubuntu17.10_amd64.deb
install-package-from-deb mysql-community-server 5.7.21-1ubuntu17.10 https://repo.mysql.com/apt/pool/mysql-5.7/m/mysql-community/mysql-community-server_5.7.21-1ubuntu17.10_amd64.deb
install-package-from-deb mysql-server 5.7.21-1ubuntu17.10 https://repo.mysql.com/apt/pool/mysql-5.7/m/mysql-community/mysql-server_5.7.21-1ubuntu17.10_amd64.deb

if [[ -f /.dockerenv ]]; then
  echo "Starting MySQL"
  /usr/sbin/mysqld --daemonize --user mysql --pid-file=/var/run/mysqld/mysqld.pid
fi

echo "Adding user: root"
mysql -h"${databaseRootHost}" -P"${databaseRootPort}" -u root -e 'use mysql;' >/dev/null 2>&1 && mysqladmin -h"${databaseRootHost}" -P"${databaseRootPort}" -u root password "${databaseRootPassword}" >/dev/null 2>&1

"${currentPath}/../../root-user.sh" -s "${databaseRootPassword}"

echo "Increasing file limits"
add-file-content-before /etc/security/limits.conf "mysql  soft  nofile  65535" "# End of file" 1
add-file-content-before /etc/security/limits.conf "mysql  hard  nofile  65535" "# End of file" 1
sysctl -p

if [[ -f /.dockerenv ]]; then
  echo "Stopping MySQL"
  kill "$(cat /var/run/mysqld/mysqld.pid)"

  echo "Allowing binding from: ${bindAddress}"
  sed -i "s/bind-address.*/bind-address = ${bindAddress}/g" /etc/mysql/mysql.conf.d/mysqld.cnf

  echo "Creating start script at: /usr/local/bin/mysql.sh"
  cat <<EOF > /usr/local/bin/mysql.sh
#!/bin/bash -e
/usr/sbin/mysqld --user mysql --pid-file=/var/run/mysqld/mysqld.pid
EOF
  chmod +x /usr/local/bin/mysql.sh
else
  echo "Restarting MySQL"
  service mysql restart
fi

mkdir -p /opt/install/
crudini --set /opt/install/env.properties mysql type "mysql"
crudini --set /opt/install/env.properties mysql version "5.7"
crudini --set /opt/install/env.properties mysql port "${databaseRootPort}"
crudini --set /opt/install/env.properties mysql rootUser "root"
crudini --set /opt/install/env.properties mysql rootPassword "${databaseRootPassword}"
