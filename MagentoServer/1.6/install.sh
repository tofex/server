#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -s  System name
  -j  Project id (optional)
  -v  Magento version
  -i  Magento edition
  -e  Magento mode (production or developer)
  -k  Crypt key
  -c  Composer user
  -p  Composer password
  -m  Composer project
  -x  Build Magento, default: yes
  -a  Additional composer projects
  -g  Git url
  -q  Run composer when building with Git url, default: no
  -y  Deploy history count, default: 5

Example: ${scriptName} -s project01 -v 2.3.4 -i community -e production -c 12345 -p 67890
EOF
}

trim()
{
  echo -n "$1" | xargs
}

systemName=
magentoServerPath=
projectId=
magentoVersion=
magentoEdition=
magentoMode=
cryptKey=
composerUser=
composerPassword=
composerProject=
buildMagento="yes"
additionalComposerProjects=
gitUrl=
gitComposer="no"
deployHistoryCount=5

while getopts hs:n:j:v:i:e:k:c:p:m:x:a:g:q:y:? option; do
  case "${option}" in
    h) usage; exit 1;;
    s) systemName=$(trim "$OPTARG");;
    n) magentoServerPath=$(trim "$OPTARG");;
    j) projectId=$(trim "$OPTARG");;
    v) magentoVersion=$(trim "$OPTARG");;
    i) magentoEdition=$(trim "$OPTARG");;
    e) magentoMode=$(trim "$OPTARG");;
    k) cryptKey=$(trim "$OPTARG");;
    c) composerUser=$(trim "$OPTARG");;
    p) composerPassword=$(trim "$OPTARG");;
    m) composerProject=$(trim "$OPTARG");;
    x) buildMagento=$(trim "$OPTARG");;
    a) additionalComposerProjects=$(trim "$OPTARG");;
    g) gitUrl=$(trim "$OPTARG");;
    q) gitComposer=$(trim "$OPTARG");;
    y) deployHistoryCount=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${systemName}" ]] || [[ "${systemName}" == "-" ]]; then
  echo "No system name specified!"
  exit 1
fi

if [[ -z "${magentoServerPath}" ]] || [[ "${magentoServerPath}" == "-" ]]; then
  magentoServerPath="magento-server"
fi

if [[ -z "${composerUser}" ]] || [[ "${composerUser}" == "-" ]]; then
  echo "No composer user specified!"
  exit 1
fi

if [[ -z "${composerPassword}" ]] || [[ "${composerPassword}" == "-" ]]; then
  echo "No composer password specified!"
  exit 1
fi

if [[ -z "${projectId}" ]]; then
  projectId="-"
fi

if [[ -z "${cryptKey}" ]]; then
  cryptKey="-"
fi

if [[ -z "${additionalComposerProjects}" ]]; then
  additionalComposerProjects="-"
fi

mkdir -p "/var/www/${systemName}"
chown www-data:www-data "/var/www/${systemName}"

sudo -H -u www-data bash -c "mkdir -p /var/www/${systemName}/${magentoServerPath}"
cd "/var/www/${systemName}/${magentoServerPath}"

#sudo -H -u www-data bash -c "echo \"{}\" > composer.json"
#sudo -H -u www-data bash -c "composer config --no-interaction allow-plugins.tofex/magento-server-installer true"
#sudo -H -u www-data bash -c "composer config --no-interaction preferred-install dist"
#sudo -H -u www-data bash -c "composer config --no-interaction repositories.tofex composer https://composer.tofex.de"
#sudo -H -u www-data bash -c "composer config --no-interaction http-basic.composer.tofex.de ${composerUser} ${composerPassword}"
#phpVersion=$(php -v | head -n 1 | cut -d " " -f 2 | cut -f1-2 -d".")

#if [[ "${phpVersion}" == "5.4" ]]; then
#  install-package jq
#  install-package moreutils
#  jq ".repositories.tofex += {\"options\": {\"ssl\": {\"verify_peer\": false, \"allow_self_signed\": true}}}" composer.json | sponge composer.json
#  chown www-data:www-data composer.json
#fi

#sudo -H -u www-data bash -c "composer require tofex/magento-server:~1.6.0"

sudo -H -u www-data bash -c "composer config --global --no-interaction http-basic.composer.tofex.de ${composerUser} ${composerPassword}"
sudo -H -u www-data bash -c "composer create-project --repository-url=https://composer.tofex.de/ \"tofex/magento-server\" --no-interaction --prefer-dist ."

sudo -H -u www-data bash -c ". ./init.sh"

sudo -H -u www-data bash -c "rm -rf env.properties"

sudo -H -u www-data bash -c "./env/init-system.sh -n ${systemName} -p ${projectId}"
sudo -H -u www-data bash -c "./env/init-server.sh -p /var/www/${systemName}/htdocs"

if [[ -n "${magentoVersion}" ]] && [[ "${magentoVersion}" != "-" ]] && [[ -n "${magentoEdition}" ]] && [[ "${magentoEdition}" != "-" ]] && [[ -n "${magentoMode}" ]] && [[ "${magentoMode}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-install.sh -v ${magentoVersion} -e ${magentoEdition} -m ${magentoMode} -c ${cryptKey}"
fi

if [[ -n "${composerProject}" ]] && [[ "${composerProject}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-build.sh -b /var/www/${systemName}/builds -t composer -c ${composerProject} -u ${composerUser} -p ${composerPassword} -m ${buildMagento} -a ${additionalComposerProjects}"
  sudo -H -u www-data bash -c "./env/init-deploy.sh -d /var/www/${systemName}/releases -c ${deployHistoryCount}"
fi

if [[ -n "${gitUrl}" ]] && [[ "${gitUrl}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-build.sh -b /var/www/${systemName}/builds -t git -g ${gitUrl} -u ${gitComposer}"
  sudo -H -u www-data bash -c "./env/init-deploy.sh -d /var/www/${systemName}/releases -c ${deployHistoryCount}"
fi
