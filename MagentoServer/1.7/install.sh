#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -s  System name
  -t  Init system (yes/no), default: yes
  -j  Project id (optional)
  -v  Magento version
  -i  Magento edition
  -e  Magento mode (production or developer)
  -k  Crypt key
  -c  Composer user
  -p  Composer password
  -m  Composer project
  -x  Build Magento, default: yes
  -a  Additional composer projects
  -g  Git url
  -q  Run composer when building with Git url, default: no
  -y  Deploy history count, default: 5

Example: ${scriptName} -s project01 -v 2.3.4 -i community -e production -c 12345 -p 67890
EOF
}

trim()
{
  echo -n "$1" | xargs
}

systemName=
initSystem=
magentoServerPath=
projectId=
magentoVersion=
magentoEdition=
magentoMode=
cryptKey=
composerUser=
composerPassword=
composerProject=
buildMagento="yes"
additionalComposerProjects=
gitUrl=
gitComposer="no"
deployHistoryCount=5

while getopts hs:t:n:j:v:i:e:k:c:p:m:x:a:g:q:y:? option; do
  case "${option}" in
    h) usage; exit 1;;
    s) systemName=$(trim "$OPTARG");;
    t) initSystem=$(trim "$OPTARG");;
    n) magentoServerPath=$(trim "$OPTARG");;
    j) projectId=$(trim "$OPTARG");;
    v) magentoVersion=$(trim "$OPTARG");;
    i) magentoEdition=$(trim "$OPTARG");;
    e) magentoMode=$(trim "$OPTARG");;
    k) cryptKey=$(trim "$OPTARG");;
    c) composerUser=$(trim "$OPTARG");;
    p) composerPassword=$(trim "$OPTARG");;
    m) composerProject=$(trim "$OPTARG");;
    x) buildMagento=$(trim "$OPTARG");;
    a) additionalComposerProjects=$(trim "$OPTARG");;
    g) gitUrl=$(trim "$OPTARG");;
    q) gitComposer=$(trim "$OPTARG");;
    y) deployHistoryCount=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${systemName}" ]] || [[ "${systemName}" == "-" ]]; then
  echo "No system name specified!"
  exit 1
fi

if [[ -z "${initSystem}" ]] || [[ "${initSystem}" == "-" ]]; then
  initSystem="yes"
fi

if [[ -z "${magentoServerPath}" ]] || [[ "${magentoServerPath}" == "-" ]]; then
  magentoServerPath="magento-server"
fi

if [[ -z "${composerUser}" ]] || [[ "${composerUser}" == "-" ]]; then
  echo "No composer user specified!"
  exit 1
fi

if [[ -z "${composerPassword}" ]] || [[ "${composerPassword}" == "-" ]]; then
  echo "No composer password specified!"
  exit 1
fi

if [[ -z "${projectId}" ]]; then
  projectId="-"
fi

if [[ -z "${cryptKey}" ]]; then
  cryptKey="-"
fi

if [[ -z "${additionalComposerProjects}" ]]; then
  additionalComposerProjects="-"
fi

if [[ "${initSystem}" == "yes" ]]; then
  mkdir -p "/var/www/${systemName}"
  chown www-data:www-data "/var/www/${systemName}"
  cd "/var/www/${systemName}"
else
  cd "/var/www"
fi

sudo -H -u www-data bash -c "composer config --global --no-interaction http-basic.composer.tofex.de ${composerUser} ${composerPassword}"
sudo -H -u www-data bash -c "composer create-project --repository-url=https://composer.tofex.de/ \"tofex/magento-server-project\" --no-interaction --prefer-dist ${magentoServerPath}"

cd "${magentoServerPath}"

sudo -H -u www-data bash -c ". ./core/init.sh"

sudo -H -u www-data bash -c "rm -rf env.properties"

sudo -H -u www-data bash -c "./env/init-system.sh -n ${systemName} -p ${projectId}"

if [[ "${initSystem}" == "yes" ]]; then
  sudo -H -u www-data bash -c "./env/init-server.sh -n server -p /var/www/${systemName}/htdocs"
fi

if [[ -n "${magentoVersion}" ]] && [[ "${magentoVersion}" != "-" ]] && [[ -n "${magentoEdition}" ]] && [[ "${magentoEdition}" != "-" ]] && [[ -n "${magentoMode}" ]] && [[ "${magentoMode}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-install.sh -v ${magentoVersion} -e ${magentoEdition} -m ${magentoMode} -c ${cryptKey}"
fi

if [[ -n "${composerProject}" ]] && [[ "${composerProject}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-build.sh -b /var/www/${systemName}/builds -t composer -c ${composerProject} -u ${composerUser} -p ${composerPassword} -m ${buildMagento} -a ${additionalComposerProjects}"
  sudo -H -u www-data bash -c "./env/init-deploy.sh -d /var/www/${systemName}/releases -c ${deployHistoryCount}"
fi

if [[ -n "${gitUrl}" ]] && [[ "${gitUrl}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-build.sh -b /var/www/${systemName}/builds -t git -g ${gitUrl} -u ${gitComposer}"
  sudo -H -u www-data bash -c "./env/init-deploy.sh -d /var/www/${systemName}/releases -c ${deployHistoryCount}"
fi

if [[ -f /.dockerenv ]]; then
  echo "Creating start script at: /usr/local/bin/magento-server.sh"
  cat <<EOF > /usr/local/bin/magento-server.sh
#!/bin/bash -e
/usr/local/bin/php.sh
EOF
  chmod +x /usr/local/bin/magento-server.sh
fi
