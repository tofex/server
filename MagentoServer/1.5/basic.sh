#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host name
  -c  Composer user
  -p  Composer password

Example: ${scriptName} -n project01 -c 12345 -p 67890
EOF
}

trim()
{
  echo -n "$1" | xargs
}

hostName=
composerUser=
composerPassword=

while getopts hn:c:p:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) hostName=$(trim "$OPTARG");;
    c) composerUser=$(trim "$OPTARG");;
    p) composerPassword=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${hostName}" ]] || [[ "${hostName}" == "-" ]]; then
  echo "No host name specified!"
  exit 1
fi

if [[ -z "${composerUser}" ]] || [[ "${composerUser}" == "-" ]]; then
  echo "No composer user specified!"
  exit 1
fi

if [[ -z "${composerPassword}" ]] || [[ "${composerPassword}" == "-" ]]; then
  echo "No composer password specified!"
  exit 1
fi

mkdir -p "/var/www/${hostName}"
chown www-data:www-data "/var/www/${hostName}"

sudo -H -u www-data bash -c "mkdir -p /var/www/${hostName}/magento-server"
cd "/var/www/${hostName}/magento-server"

sudo -H -u www-data bash -c "echo \"{}\" > composer.json"
sudo -H -u www-data bash -c "composer config --no-interaction preferred-install dist"
sudo -H -u www-data bash -c "composer config --no-interaction repositories.tofex composer https://composer.tofex.de"
sudo -H -u www-data bash -c "composer config --no-interaction http-basic.composer.tofex.de ${composerUser} ${composerPassword}"

sudo -H -u www-data bash -c "composer require tofex/magento-server:~1.5.0"

sudo -H -u www-data bash -c ". ./init.sh"

cat <<EOF | sudo -u www-data tee "env.properties" > /dev/null
[project]
servers = server1
hostName = ${hostName}

[install]
repositories[] = "composer|https://repo.magento.com|d661c529da2e737d5b514bf1ff2a2576|b969ec145c55b8a8248ca8541160fe89"

[build]
type = composer
server = server1
repositories[] = "composer|https://composer.tofex.de|${composerUser}|${composerPassword}"

[server1]
type = local
webPath = /var/www/${hostName}/htdocs
buildPath = /var/www/${hostName}/builds
deployPath = /var/www/${hostName}/releases
webUser = www-data
webGroup = www-data
deployHistoryCount = 5
upgrade = yes
EOF
