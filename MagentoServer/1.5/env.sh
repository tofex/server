#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host name

Example: ${scriptName} -n project01
EOF
}

trim()
{
  echo -n "$1" | xargs
}

hostName=

while getopts hn:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) hostName=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${hostName}" ]] || [[ "${hostName}" == "-" ]]; then
  echo "No host name specified!"
  exit 1
fi

if [[ -f "/opt/install/env.properties" ]]; then
  cd "/var/www/${hostName}/magento-server"

  databaseType=$(ini-parse "/opt/install/env.properties" "no" "mysql" "type")
  databaseVersion=$(ini-parse "/opt/install/env.properties" "no" "mysql" "version")
  databasePort=$(ini-parse "/opt/install/env.properties" "no" "mysql" "port")

  if [[ -n "${databaseType}" ]] && [[ "${databaseType}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project databaseType \"${databaseType}\""
  fi

  if [[ -n "${databaseVersion}" ]] && [[ "${databaseVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project databaseVersion \"${databaseVersion}\""
    sudo -H -u www-data bash -c "ini-set env.properties yes project databaseHost localhost"
  fi

  if [[ -n "${databasePort}" ]] && [[ "${databasePort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project databasePort ${databasePort}"
  fi

  apacheVersion=$(ini-parse "/opt/install/env.properties" "no" "apache" "version")
  apacheHttpPort=$(ini-parse "/opt/install/env.properties" "no" "apache" "httpPort")
  apacheSslPort=$(ini-parse "/opt/install/env.properties" "no" "apache" "sslPort")

  if [[ -n "${apacheVersion}" ]] && [[ "${apacheVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project apacheVersion ${apacheVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project apacheHost localhost"
  fi

  if [[ -n "${apacheHttpPort}" ]] && [[ "${apacheHttpPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project apacheHttpPort ${apacheHttpPort}"
  fi

  if [[ -n "${apacheSslPort}" ]] && [[ "${apacheSslPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project apacheSslPort ${apacheSslPort}"
  fi

  nginxVersion=$(ini-parse "/opt/install/env.properties" "no" "nginx" "version")
  nginxHttpPort=$(ini-parse "/opt/install/env.properties" "no" "nginx" "httpPort")
  nginxSslPort=$(ini-parse "/opt/install/env.properties" "no" "nginx" "sslPort")

  if [[ -n "${nginxVersion}" ]] && [[ "${nginxVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project nginxVersion ${nginxVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project nginxHost localhost"
  fi

  if [[ -n "${nginxHttpPort}" ]] && [[ "${nginxHttpPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project nginxHttpPort ${nginxHttpPort}"
  fi

  if [[ -n "${nginxSslPort}" ]] && [[ "${nginxSslPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project nginxSslPort ${nginxSslPort}"
  fi

  varnishVersion=$(ini-parse "/opt/install/env.properties" "no" "varnish" "version")
  varnishPort=$(ini-parse "/opt/install/env.properties" "no" "varnish" "port")
  varnishAdminPort=$(ini-parse "/opt/install/env.properties" "no" "varnish" "adminPort")
  varnishSiteName=$(ini-parse "/opt/install/env.properties" "no" "varnish" "siteName")

  if [[ -n "${varnishVersion}" ]] && [[ "${varnishVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project varnishVersion ${varnishVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project varnishHost localhost"
  fi

  if [[ -n "${varnishPort}" ]] && [[ "${varnishPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project varnishPort ${varnishPort}"
  fi

  if [[ -n "${varnishAdminPort}" ]] && [[ "${varnishAdminPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project varnishAdminPort ${varnishAdminPort}"
  fi

  if [[ -n "${varnishSiteName}" ]] && [[ "${varnishSiteName}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project varnishSiteName \"${varnishSiteName}\""
  fi

  redisCacheVersion=$(ini-parse "/opt/install/env.properties" "no" "redis" "cacheVersion")
  redisCachePort=$(ini-parse "/opt/install/env.properties" "no" "redis" "cachePort")
  redisCacheDatabase=$(ini-parse "/opt/install/env.properties" "no" "redis" "cacheDatabase")

  if [[ -n "${redisCacheVersion}" ]] && [[ "${redisCacheVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisCacheVersion ${redisCacheVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisCacheHost localhost"
  fi

  if [[ -n "${redisCachePort}" ]] && [[ "${redisCachePort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisCachePort ${redisCachePort}"
  fi

  if [[ -n "${redisCacheDatabase}" ]] && [[ "${redisCacheDatabase}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisCacheDatabase ${redisCacheDatabase}"
  fi

  redisSessionVersion=$(ini-parse "/opt/install/env.properties" "no" "redis" "sessionVersion")
  redisSessionPort=$(ini-parse "/opt/install/env.properties" "no" "redis" "sessionPort")
  redisSessionDatabase=$(ini-parse "/opt/install/env.properties" "no" "redis" "sessionDatabase")

  if [[ -n "${redisSessionVersion}" ]] && [[ "${redisSessionVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisSessionVersion ${redisSessionVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisSessionHost localhost"
  fi

  if [[ -n "${redisSessionPort}" ]] && [[ "${redisSessionPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisSessionPort ${redisSessionPort}"
  fi

  if [[ -n "${redisSessionDatabase}" ]] && [[ "${redisSessionDatabase}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisSessionDatabase ${redisSessionDatabase}"
  fi

  redisFullPageCacheVersion=$(ini-parse "/opt/install/env.properties" "no" "redis" "fullPageCacheVersion")
  redisFullPageCachePort=$(ini-parse "/opt/install/env.properties" "no" "redis" "fullPageCachePort")
  redisFullPageCacheDatabase=$(ini-parse "/opt/install/env.properties" "no" "redis" "fullPageCacheDatabase")

  if [[ -n "${redisFullPageCacheVersion}" ]] && [[ "${redisFullPageCacheVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisFullPageCacheVersion ${redisFullPageCacheVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisFullPageCacheHost localhost"
  fi

  if [[ -n "${redisFullPageCachePort}" ]] && [[ "${redisFullPageCachePort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisFullPageCachePort ${redisFullPageCachePort}"
  fi

  if [[ -n "${redisFullPageCacheDatabase}" ]] && [[ "${redisFullPageCacheDatabase}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project redisFullPageCacheDatabase ${redisFullPageCacheDatabase}"
  fi

  solrVersion=$(ini-parse "/opt/install/env.properties" "no" "solr" "version")
  solrPort=$(ini-parse "/opt/install/env.properties" "no" "solr" "port")
  solrServiceName=$(ini-parse "/opt/install/env.properties" "no" "solr" "serviceName")

  if [[ -n "${solrVersion}" ]] && [[ "${solrVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project solrVersion ${solrVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project solrHost localhost"
  fi

  if [[ -n "${solrPort}" ]] && [[ "${solrPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project solrPort ${solrPort}"
  fi

  if [[ -n "${solrServiceName}" ]] && [[ "${solrServiceName}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project solrServiceName \"${solrServiceName}\""
  fi

  rabbitMQVersion=$(ini-parse "/opt/install/env.properties" "no" "rabbitmq" "version")
  rabbitMQPort=$(ini-parse "/opt/install/env.properties" "no" "rabbitmq" "port")

  if [[ -n "${rabbitMQVersion}" ]] && [[ "${rabbitMQVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project rabbitMQVersion ${rabbitMQVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project rabbitMQHost localhost"
  fi

  if [[ -n "${rabbitMQPort}" ]] && [[ "${rabbitMQPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project rabbitMQPort ${rabbitMQPort}"
  fi

  influxDBVersion=$(ini-parse "/opt/install/env.properties" "no" "influxdb" "version")
  influxDBPort=$(ini-parse "/opt/install/env.properties" "no" "influxdb" "port")

  if [[ -n "${influxDBVersion}" ]] && [[ "${influxDBVersion}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project influxDBVersion ${influxDBVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project influxDBHost localhost"
  fi

  if [[ -n "${influxDBPort}" ]] && [[ "${influxDBPort}" != "-" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project influxDBPort ${influxDBPort}"
  fi

  telegrafVersion=$(ini-parse "/opt/install/env.properties" "no" "telegraf" "version")

  if [[ -n "${telegrafVersion}" ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes project telegrafVersion ${telegrafVersion}"
    sudo -H -u www-data bash -c "ini-set env.properties yes project telegrafHost localhost"
  fi
fi
