#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host name
  -s  Server name
  -v  Magento version
  -i  Magento edition
  -e  Magento mode, (production or development)
  -j  Project id
  -c  Composer user
  -p  Composer password
  -m  Composer project
  -g  Git url
  -q  Run composer when building with Git url, default: no
  -a  Additional composer projects
  -x  Build Magento, default: yes
  -k  Crypt key
  -l  Link
  -b  Script before
  -f  Script after
  -y  Deploy history count, default: 5

Example: ${scriptName} -n project01 -s project01.tofex.net -v 2.3.4 -i community -e production -c 12345 -p 67890
EOF
}

trim()
{
  echo -n "$1" | xargs
}

versionCompare() {
  if [[ "$1" == "$2" ]]; then
    echo "0"
  elif [[ "$1" = $(echo -e "$1\n$2" | sort -V | head -n1) ]]; then
    echo "1"
  else
    echo "2"
  fi
}

hostName=
serverName=
magentoVersion=
magentoEdition=
magentoMode=
projectId=
composerUser=
composerPassword=
composerProject=
gitUrl=
gitComposer="no"
additionalComposerProjects=
buildMagento="yes"
cryptKey=
link=
scriptBefore=
scriptAfter=
deployHistoryCount=5

while getopts hn:s:v:i:e:j:c:p:m:g:q:a:x:k:l:b:f:y:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) hostName=$(trim "$OPTARG");;
    s) serverName=$(trim "$OPTARG");;
    v) magentoVersion=$(trim "$OPTARG");;
    i) magentoEdition=$(trim "$OPTARG");;
    e) magentoMode=$(trim "$OPTARG");;
    j) projectId=$(trim "$OPTARG");;
    c) composerUser=$(trim "$OPTARG");;
    p) composerPassword=$(trim "$OPTARG");;
    m) composerProject=$(trim "$OPTARG");;
    g) gitUrl=$(trim "$OPTARG");;
    q) gitComposer=$(trim "$OPTARG");;
    a) additionalComposerProjects=$(trim "$OPTARG");;
    x) buildMagento=$(trim "$OPTARG");;
    k) cryptKey=$(trim "$OPTARG");;
    l) link=$(trim "$OPTARG");;
    b) scriptBefore=$(trim "$OPTARG");;
    f) scriptAfter=$(trim "$OPTARG");;
    y) deployHistoryCount=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${hostName}" ]] || [[ "${hostName}" == "-" ]]; then
  echo "No host name specified!"
  exit 1
fi

if [[ -z "${magentoVersion}" ]] || [[ "${magentoVersion}" == "-" ]]; then
  echo "No magento version specified!"
  exit 1
fi

if [[ -z "${magentoEdition}" ]] || [[ "${magentoEdition}" == "-" ]]; then
  echo "No magento edition specified!"
  exit 1
fi

if [[ -z "${magentoMode}" ]] || [[ "${magentoMode}" == "-" ]]; then
  echo "No Magento mode specified!"
  exit 1
fi

if [[ "${magentoMode}" != "developer" ]] && [[ "${magentoMode}" != "production" ]]; then
  echo "Invalid Magento mode specified!"
  exit 1
fi

if [[ -z "${composerUser}" ]] || [[ "${composerUser}" == "-" ]]; then
  echo "No composer user specified!"
  exit 1
fi

if [[ -z "${composerPassword}" ]] || [[ "${composerPassword}" == "-" ]]; then
  echo "No composer password specified!"
  exit 1
fi

mkdir -p "/var/www/${hostName}"
chown www-data:www-data "/var/www/${hostName}"

sudo -H -u www-data bash -c "mkdir -p /var/www/${hostName}/magento-server"
cd "/var/www/${hostName}/magento-server"

#sudo -H -u www-data bash -c "echo \"{}\" > composer.json"
#sudo -H -u www-data bash -c "composer config --no-interaction preferred-install dist"
#sudo -H -u www-data bash -c "composer config --no-interaction repositories.tofex composer https://composer.tofex.de"
#sudo -H -u www-data bash -c "composer config --no-interaction http-basic.composer.tofex.de ${composerUser} ${composerPassword}"

#phpVersion=$(php -v | head -n 1 | cut -d " " -f 2 | cut -f1-2 -d".")
#if [[ "${phpVersion}" == "5.4" ]]; then
#  install-package jq
#  install-package moreutils
#  jq ".repositories.tofex += {\"options\": {\"ssl\": {\"verify_peer\": false, \"allow_self_signed\": true}}}" composer.json | sponge composer.json
#  chown www-data:www-data composer.json
#fi

#sudo -H -u www-data bash -c "composer require tofex/magento-server:~1.5.0"

sudo -H -u www-data bash -c "composer config --global --no-interaction http-basic.composer.tofex.de ${composerUser} ${composerPassword}"
sudo -H -u www-data bash -c "composer create-project --repository-url=https://composer.tofex.de/ \"tofex/magento-server=~1.5.0\" --no-interaction --prefer-dist ."

sudo -H -u www-data bash -c ". ./init.sh"

sudo -H -u www-data bash -c "rm -rf env.properties"

sudo -H -u www-data bash -c "./env/init-system.sh -n ${systemName} -p ${projectId}"
sudo -H -u www-data bash -c "./env/init-server.sh -p /var/www/${systemName}/htdocs"

if [[ -n "${magentoVersion}" ]] && [[ "${magentoVersion}" != "-" ]] && [[ -n "${magentoEdition}" ]] && [[ "${magentoEdition}" != "-" ]] && [[ -n "${magentoMode}" ]] && [[ "${magentoMode}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-install.sh -v ${magentoVersion} -e ${magentoEdition} -m ${magentoMode} -c ${cryptKey}"
fi

if [[ -n "${composerProject}" ]] && [[ "${composerProject}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-build.sh -b /var/www/${systemName}/builds -t composer -c ${composerProject} -u ${composerUser} -p ${composerPassword} -m ${buildMagento} -a ${additionalComposerProjects}"
  sudo -H -u www-data bash -c "./env/init-deploy.sh -d /var/www/${systemName}/releases -c ${deployHistoryCount}"
fi

if [[ -n "${gitUrl}" ]] && [[ "${gitUrl}" != "-" ]]; then
  sudo -H -u www-data bash -c "./env/init-build.sh -b /var/www/${systemName}/builds -t git -g ${gitUrl} -u ${gitComposer}"
  sudo -H -u www-data bash -c "./env/init-deploy.sh -d /var/www/${systemName}/releases -c ${deployHistoryCount}"
fi







sudo -H -u www-data bash -c "touch env.properties"
sudo -H -u www-data bash -c "ini-set env.properties yes project servers server1"
sudo -H -u www-data bash -c "ini-set env.properties yes project hostName \"${hostName}\""
if [[ $(versionCompare "${magentoVersion}" "1.9.4.5") == 0 ]] || [[ $(versionCompare "${magentoVersion}" "1.9.4.5") == 1 ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes install repositories \"composer|https://composer.tofex.de|397680c997623334a6da103dbfd2d3c3|a9ccf6ec2e552892f8a510b4b0e1edd5\""
else
  sudo -H -u www-data bash -c "ini-set env.properties yes install repositories \"composer|https://repo.magento.com|d661c529da2e737d5b514bf1ff2a2576|b969ec145c55b8a8248ca8541160fe89\""
fi
sudo -H -u www-data bash -c "ini-set env.properties yes install magentoVersion \"${magentoVersion}\""
sudo -H -u www-data bash -c "ini-set env.properties yes install magentoEdition \"${magentoEdition}\""
sudo -H -u www-data bash -c "ini-set env.properties yes install magentoMode \"${magentoMode}\""
sudo -H -u www-data bash -c "ini-set env.properties yes build server server1"
sudo -H -u www-data bash -c "ini-set env.properties yes server1 type local"
sudo -H -u www-data bash -c "ini-set env.properties yes server1 webUser www-data"
sudo -H -u www-data bash -c "ini-set env.properties yes server1 webGroup www-data"
sudo -H -u www-data bash -c "ini-set env.properties yes server1 webPath \"/var/www/${hostName}/htdocs\""
sudo -H -u www-data bash -c "ini-set env.properties yes server1 buildPath \"/var/www/${hostName}/builds\""
sudo -H -u www-data bash -c "ini-set env.properties yes server1 deployPath \"/var/www/${hostName}/releases\""
sudo -H -u www-data bash -c "ini-set env.properties yes server1 deployHistoryCount ${deployHistoryCount}"
sudo -H -u www-data bash -c "ini-set env.properties yes server1 upgrade yes"

if [[ -n "${projectId}" ]] && [[ "${projectId}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes project projectId ${projectId}"
fi

if [[ -n "${composerProject}" ]] && [[ "${composerProject}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes build type composer"
  sudo -H -u www-data bash -c "ini-set env.properties yes build repositories \"composer|https://composer.tofex.de|${composerUser}|${composerPassword}\""
  sudo -H -u www-data bash -c "ini-set env.properties yes build composerProject \"${composerProject}\""
fi

if [[ -n "${gitUrl}" ]] && [[ "${gitUrl}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes build type git"
  sudo -H -u www-data bash -c "ini-set env.properties yes build gitUrl \"${gitUrl}\""
  if [[ "${gitComposer}" == "yes" ]] || [[ "${gitComposer}" == 1 ]]; then
    sudo -H -u www-data bash -c "ini-set env.properties yes build composer yes"
  else
    sudo -H -u www-data bash -c "ini-set env.properties yes build composer no"
  fi
fi

if [[ "${buildMagento}" == "yes" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes build magento yes"
else
  sudo -H -u www-data bash -c "ini-set env.properties yes build magento no"
fi

if [[ -n "${additionalComposerProjects}" ]] && [[ "${additionalComposerProjects}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes build additionalComposerProjects ${additionalComposerProjects}"
fi

if [[ -n "${cryptKey}" ]] && [[ "${cryptKey}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes install cryptKey \"${cryptKey}\""
fi

if [[ -n "${serverName}" ]] && [[ "${serverName}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes project serverName \"${serverName}\""
fi

if [[ -n "${link}" ]] && [[ "${link}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes server1 link \"${link}\""
fi

if [[ -n "${scriptBefore}" ]] && [[ "${scriptBefore}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes server1 scriptBefore \"${scriptBefore}\""
fi

if [[ -n "${scriptAfter}" ]] && [[ "${scriptAfter}" != "-" ]]; then
  sudo -H -u www-data bash -c "ini-set env.properties yes server1 scriptAfter \"${scriptAfter}\""
fi
