#!/bin/bash -e

scriptName="${0##*/}"

usage()
{
cat >&2 << EOF
usage: ${scriptName} options

OPTIONS:
  -h  Show this message
  -n  Host name
  -o  Hosts
  -j  Project id
  -m  Composer project name
  -c  Composer user
  -p  Composer password
  -v  Magento version
  -i  Magento edition (community / enterprise)
  -e  Magento mode (developer / production)
  -k  Crypt key
  -d  Database host
  -r  Database port
  -u  Database user
  -w  Database password
  -t  Database name

Example: ${scriptName} -n m1-block-customer -u 12345 -p 67890 -v 2.3.4 -i community -e no@one.com -k 12345
EOF
}

trim()
{
  echo -n "$1" | xargs
}

hostName=
hosts=
projectId=
composerProjectName=
composerUser=
composerPassword=
magentoVersion=
magentoEdition=
magentoMode=
cryptKey=
databaseHost=
databasePort=
databaseUser=
databasePassword=
databaseName=

while getopts hn:o:j:m:c:p:v:i:e:k:d:r:u:w:t:? option; do
  case ${option} in
    h) usage; exit 1;;
    n) hostName=$(trim "$OPTARG");;
    o) hosts=$(trim "$OPTARG");;
    j) projectId=$(trim "$OPTARG");;
    m) composerProjectName=$(trim "$OPTARG");;
    c) composerUser=$(trim "$OPTARG");;
    p) composerPassword=$(trim "$OPTARG");;
    v) magentoVersion=$(trim "$OPTARG");;
    i) magentoEdition=$(trim "$OPTARG");;
    e) magentoMode=$(trim "$OPTARG");;
    k) cryptKey=$(trim "$OPTARG");;
    d) databaseHost=$(trim "$OPTARG");;
    r) databasePort=$(trim "$OPTARG");;
    u) databaseUser=$(trim "$OPTARG");;
    w) databasePassword=$(trim "$OPTARG");;
    t) databaseName=$(trim "$OPTARG");;
    ?) usage; exit 1;;
  esac
done

if [[ -z "${hostName}" ]]; then
  read -r -p "Host name: " hostName
fi

if [[ -z "${hosts}" ]]; then
  read -r -p "Hosts (server:scope:code): " hosts
fi

if [[ -z "${projectId}" ]]; then
  read -r -p "Project Id: " projectId
fi

if [[ -z "${composerProjectName}" ]]; then
  read -r -p "Composer project name: " composerProjectName
fi

if [[ -z "${composerUser}" ]]; then
  read -r -p "Composer user: " composerUser
fi

if [[ -z "${composerPassword}" ]]; then
  read -r -p "Composer password: " composerPassword
fi

if [[ -z "${magentoVersion}" ]]; then
  read -r -p "Magento version: " magentoVersion
fi

if [[ -z "${magentoEdition}" ]]; then
  read -r -p "Magento edition: " magentoEdition
fi

if [[ -z "${magentoMode}" ]]; then
  read -r -p "Magento mode (developer/production): " magentoMode
fi

if [[ -z "${cryptKey}" ]]; then
  read -r -p "Crypt Key (empty generate new key): " cryptKey
  if [[ -z "${cryptKey}" ]]; then
    cryptKey=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 32 | head -n 1)
  fi
fi

if [[ -z "${databaseHost}" ]]; then
  read -r -p "Database host: " databaseHost
fi

if [[ -z "${databasePort}" ]]; then
  read -r -p "Database port: " databasePort
fi

if [[ -z "${databaseUser}" ]]; then
  read -r -p "Database user: " databaseUser
fi

if [[ -z "${databasePassword}" ]]; then
  read -r -p "Database password: " databasePassword
fi

if [[ -z "${databaseName}" ]]; then
  read -r -p "Database name: " databaseName
fi

if [[ -z "${hostName}" ]]; then
  echo "Missing host name"
  exit 1
fi

if [[ -z "${hosts}" ]]; then
  echo "Missing hosts"
  exit 1
fi

if [[ -z "${projectId}" ]]; then
  echo "Missing project id"
  exit 1
fi

if [[ -z "${composerProjectName}" ]]; then
  echo "Missing composer package name"
  exit 1
fi

if [[ -z "${composerUser}" ]]; then
  echo "Missing composer user"
  exit 1
fi

if [[ -z "${composerPassword}" ]]; then
  echo "Missing composer password"
  exit 1
fi

if [[ -z "${magentoVersion}" ]]; then
  echo "Missing Magento version"
  exit 1
fi

if [[ -z "${magentoEdition}" ]]; then
  echo "Missing Magento edition"
  exit 1
fi

if [[ -z "${magentoMode}" ]]; then
  echo "Missing Magento mode"
  exit 1
fi

if [[ -z "${cryptKey}" ]]; then
  echo "Missing crypt key"
  exit 1
fi

if [[ -z "${databaseHost}" ]]; then
  echo "Missing database host"
  exit 1
fi

if [[ -z "${databasePort}" ]]; then
  echo "Missing database port"
  exit 1
fi

if [[ -z "${databaseUser}" ]]; then
  echo "Missing database user"
  exit 1
fi

if [[ -z "${databasePassword}" ]]; then
  echo "Missing database password"
  exit 1
fi

if [[ -z "${databaseName}" ]]; then
  echo "Missing database name"
  exit 1
fi

configDir=${PWD}

hostList=( $(echo "${hosts}" | tr "," "\n") )
mainHostName=$(echo "${hostList[0]}" | cut -d: -f1)

update-apt

wget --no-cache -q -O - https://bitbucket.org/tofex/server/raw/master/init.sh | bash

cd /opt/install

./server/MagentoServer/1.5/install.sh \
  -n "${hostName}" \
  -o "${hosts}" \
  -s "${mainHostName}" \
  -j "${projectId}" \
  -c "${composerUser}" \
  -p "${composerPassword}" \
  -m "${composerProjectName}" \
  -v "${magentoVersion}" \
  -i "${magentoEdition}" \
  -e "${magentoMode}" \
  -k "${cryptKey}" \
  -d "${databaseHost}" \
  -r "${databasePort}" \
  -u "${databaseUser}" \
  -w "${databasePassword}" \
  -t "${databaseName}"

./server/MagentoServer/1.5/env.sh -n "${hostName}"
